package com.adfinbureau.wms.screen.login;

import com.adfinbureau.wms.R;

import flow.HasParent;
import flow.Layout;
import mortar.Blueprint;
import com.adfinbureau.wms.screen.main.MainScreen;
import com.adfinbureau.wms.screen.splash.SplashScreen;

@Layout(R.layout.view_login)
public class LoginScreen implements Blueprint, HasParent<SplashScreen> {
	@Override
	public String getMortarScopeName() {
		return getClass().getName();
	}

	@Override
	public Object getDaggerModule() {
		return new Module();
	}

	@Override
	public SplashScreen getParent() {
		return new SplashScreen();
	}

	@dagger.Module(
			injects = LoginView.class,
			addsTo = MainScreen.Module.class,
			library = true
	)
	class Module {}
}
