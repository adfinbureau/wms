package com.adfinbureau.wms.screen.survey;

import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.actionbar.ActionBarConfig;
import com.adfinbureau.wms.actionbar.ActionBarOwner;
import com.adfinbureau.wms.activity.ActivityResultOwner;
import com.adfinbureau.wms.activity.ActivityResultRegistrar;
import com.adfinbureau.wms.model.Survey;
import com.adfinbureau.wms.screen.asbuild.AsBuildScreen;
import com.adfinbureau.wms.screen.map.MapScreen;
import com.adfinbureau.wms.screen.map.OfflineMapScreen;
import com.adfinbureau.wms.screen.picture.PictureScreen;
import com.adfinbureau.wms.screen.text.TextScreen;
import com.adfinbureau.wms.screen.video.VideoScreen;
import com.adfinbureau.wms.service.LocationService;
import com.adfinbureau.wms.service.UserService;
import com.adfinbureau.wms.util.adapter.SurveyListAdapter;
import com.adfinbureau.wms.util.camera.AlbumStorageDirFactory;
import com.adfinbureau.wms.util.camera.BaseAlbumDirFactory;
import com.adfinbureau.wms.util.camera.FroyoAlbumDirFactory;
import com.adfinbureau.wms.util.mortar.BaseViewPresenter;
import com.adfinbureau.wms.util.widgets.DividerItemDecoration;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.sqlbrite.BriteDatabase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Validator;

import flow.Flow;
import mortar.MortarScope;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;


public class SurveyPresenter extends BaseViewPresenter<SurveyView> implements SurveyListAdapter.OnItemClickListener, ActivityResultOwner.ActivityResultListener{
	private ActionBarOwner actionBarOwner;
	private final ActivityResultRegistrar activityResultRegistrar;

	private final Validator validator;
	private final UserService userService;
	private final LocationService locationService;
	private final Flow flow;
	private final BriteDatabase db;
	private final Location location;

	private static List<Survey> items = new ArrayList<Survey>();

//	static {
//		for (int i = 1; i <= 10; i++) {
//			items.add(new ViewModel("Item " + i, R.drawable.arrow));
//		}
//	}

	@Inject
	SurveyPresenter(ActionBarOwner actionBarOwner, Validator validator, UserService userService, LocationService locationService, Flow flow, ActivityResultRegistrar activityResultRegistrar, BriteDatabase db, Location location) {
		this.actionBarOwner = actionBarOwner;
		this.validator = validator;
		this.userService = userService;
		this.locationService = locationService;
		this.flow = flow;
		this.activityResultRegistrar = activityResultRegistrar;
		this.db = db;
		this.location = location;
	}

	private AlbumStorageDirFactory mAlbumStorageDirFactory = null;

	@Override
	protected void onLoad(Bundle savedInstanceState) {
		super.onLoad(savedInstanceState);

		final SurveyView view = getView();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
			mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
		} else {
			mAlbumStorageDirFactory = new BaseAlbumDirFactory();
		}
		if (view != null) {
			configureActionBar();

			RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(view.getContext());
			view.recyclerView.setLayoutManager(mLayoutManager);
			view.recyclerView.setItemAnimator(new DefaultItemAnimator());
			//view.recyclerView.setAdapter(adapter);

			//view.recyclerView.setLayoutManager(new GridLayoutManager(view.getContext(), 2));
			view.recyclerView.addItemDecoration(new DividerItemDecoration(getView().getContext(), LinearLayoutManager.VERTICAL));

			setRecyclerAdapter(view.recyclerView);
			//view.recyclerView.scheduleLayoutAnimation();

			view.fab.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					long id = db.insert(Survey.TABLE, new Survey.Builder().data("A").build());
					flow.goTo(new MapScreen(location, id));
				}
			});

			String x = "SELECT * from "+Survey.TABLE+" ORDER BY "+Survey.CREATED_DATE +" DESC";
			subscription = db.createQuery(Survey.TABLE,
//					LocationLists.QUERY)
					x)
					.mapToList(Survey.MAPPER)
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe(adapter);
		}
	}

	private Subscription subscription;
	private SurveyListAdapter adapter;

	private void setRecyclerAdapter(RecyclerView recyclerView) {
		adapter = new SurveyListAdapter(getView().getContext(), items);
		adapter.setOnItemClickListener(this);
		recyclerView.setAdapter(adapter);
	}

	private void configureActionBar() {
		ActionBarConfig config = new ActionBarConfig.Builder()
				.title("Survey")
				.build();
		actionBarOwner.setConfig(config);
	}

	@Override
	public void onItemClick(View view, final Survey viewModel) {
		//long id = db.insert(Survey.TABLE, new Survey.Builder().data("A").build());
		flow.goTo(new MapScreen(location, viewModel.id()));
		/*
		new MaterialDialog.Builder(getView().getContext())
				.title(R.string.menu)
				.items(R.array.map_menu)
				.itemsCallbackSingleChoice(2, new MaterialDialog.ListCallbackSingleChoice() {
					@Override
					public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {

						if (text.toString().contentEquals("Google Map")) {
							flow.goTo(new MapScreen(location, viewModel.id()));
						} else if (text.toString().contentEquals("Map Quest")) {
							flow.goTo(new OfflineMapScreen(location, viewModel.id()));
						}
						return true; // allow selection
					}
				})
				.positiveText(R.string.md_choose_label)
				.show();
				*/
	}

	@Override
	protected void onEnterScope(MortarScope scope) {
		super.onEnterScope(scope);
		activityResultRegistrar.register(scope, this);
	}

	@Override
	protected void onExitScope() {
		super.onExitScope();

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	}
}
