package com.adfinbureau.wms.screen.register;

import static org.apache.commons.lang3.StringUtils.trimToEmpty;

import java.util.Set;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import com.adfinbureau.wms.SharedPreferencesKeys;
import com.adfinbureau.wms.actionbar.ActionBarConfig;
import com.adfinbureau.wms.actionbar.ActionBarOwner;
import com.adfinbureau.wms.model.User;
import com.adfinbureau.wms.model.UserWithPassword;
import com.adfinbureau.wms.repository.JsonSharedPreferencesRepository;
import com.adfinbureau.wms.screen.home.HomeScreen;
import com.adfinbureau.wms.service.ApiService;
import com.adfinbureau.wms.service.TestService;
import com.adfinbureau.wms.util.mortar.BaseViewPresenter;

import android.os.Bundle;

import flow.Flow;
import rx.Subscription;
import rx.functions.Action1;

class RegisterPresenter extends BaseViewPresenter<RegisterView> {

  private final Flow flow;
  private final ActionBarOwner actionBarOwner;
  private final Validator validator;
  private final ApiService apiService;
  private final TestService testService;
  private final JsonSharedPreferencesRepository prefsRepository;

  private Subscription registerSubscription;

  @Inject
  RegisterPresenter(Flow flow, ActionBarOwner actionBarOwner, Validator validator, ApiService apiService, TestService testService, JsonSharedPreferencesRepository prefsRepository) {
    this.flow = flow;
    this.actionBarOwner = actionBarOwner;
    this.validator = validator;
    this.apiService = apiService;
    this.testService = testService;
    this.prefsRepository = prefsRepository;
  }

  @Override
  protected void onLoad(Bundle savedInstanceState) {
    super.onLoad(savedInstanceState);

    RegisterView view = getView();
    if (view != null) {
      configureActionBar();
    }
  }

  @Override
  public void dropView(RegisterView view) {
    if (registerSubscription != null && !registerSubscription.isUnsubscribed()) {
      registerSubscription.unsubscribe();
    }

    super.dropView(view);
  }

  private void configureActionBar() {
    ActionBarConfig config = new ActionBarConfig.Builder()
      .title("Register")
      .build();
    actionBarOwner.setConfig(config);
  }

  public void register(String name, String email, String password) {
    RegisterView view = getView();

    UserWithPassword user = new UserWithPassword(trimToEmpty(name), trimToEmpty(email), trimToEmpty(password));
    Set<ConstraintViolation<UserWithPassword>> errors = validator.validate(user);

    //User u = new User(name, password);
   // storeUser(u);

    apiService.register(user);
    flow.goTo(new HomeScreen());
    /*
    if (errors.isEmpty()) {
      registerWithApi(user);
    } else {
      view.onValidationError(errors);
    }
    */

    /*
    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    StrictMode.setThreadPolicy(policy);
    Call<Result> x =  testService.customers();
    try {
      Result b = x.execute().body();
      for(Customer c : b.getRecords()){
        Log.d("DATA", c.getCity());
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    */

    /*
    Customer x = new Customer("A","B","C");
    x.save();

    List<Customer> cust = x.find(Customer.class, "Name = ?", new String[]{"A"});
    Log.d("TOTAL", "NILAI : "+cust.size());
    */
  }

  private void registerWithApi(UserWithPassword user) {
    registerSubscription = apiService.register(user)
      .subscribe(new Action1<User>() {
        @Override
        public void call(User user) {
          storeUser(user);
          flow.resetTo(new HomeScreen());
        }
      }, new Action1<Throwable>() {
        @Override
        public void call(Throwable thrown) {
          getView().onRegistrationError(thrown);
        }
      });
  }

  private void storeUser(User user) {
    prefsRepository.putObject(SharedPreferencesKeys.USER_ACCOUNT, user);
  }
}
