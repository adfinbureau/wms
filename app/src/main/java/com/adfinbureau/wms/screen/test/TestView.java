package com.adfinbureau.wms.screen.test;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.Button;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.util.mortar.BaseView;

import javax.inject.Inject;

import butterknife.InjectView;
import mortar.Presenter;

public class TestView extends BaseView {

	@Inject
	TestPresenter presenter;

	@InjectView(R.id.btnUpload)
	Button upload;

	public TestView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected Presenter getPresenter() {
		return presenter;
	}


}
