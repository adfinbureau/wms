package com.adfinbureau.wms.screen.setting;


import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.SharedPreferencesKeys;
import com.adfinbureau.wms.actionbar.ActionBarConfig;
import com.adfinbureau.wms.actionbar.ActionBarOwner;
import com.adfinbureau.wms.model.LocationData;
import com.adfinbureau.wms.repository.JsonSharedPreferencesRepository;
import com.adfinbureau.wms.screen.text.AddTextScreen;
import com.adfinbureau.wms.service.LocationService;
import com.adfinbureau.wms.service.UserService;
import com.adfinbureau.wms.util.SimpleFileDialog;
import com.adfinbureau.wms.util.adapter.TextListAdapter;
import com.adfinbureau.wms.util.mortar.BaseViewPresenter;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.folderselector.FolderChooserDialog;
import com.squareup.sqlbrite.BriteDatabase;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.validation.Validator;

import flow.Flow;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

class SettingPresenter extends BaseViewPresenter<SettingView> implements FolderChooserDialog.FolderCallback {

	private ActionBarOwner actionBarOwner;

	private final Validator validator;
	private final UserService userService;
	private final LocationService locationService;
	private final Flow flow;
	private final BriteDatabase db;
	private final JsonSharedPreferencesRepository sharedPreferences;

	private static final String TAG = "SettingPresenter";
	private BluetoothAdapter btAdapter = null;

	private static List<LocationData> items = new ArrayList<>();

	static {
//		for (int i = 1; i <= 10; i++) {
//			items.add(new ViewModel("Item " + i, R.drawable.arrow));
//		}
	}

	@Inject
	SettingPresenter(ActionBarOwner actionBarOwner, Validator validator, UserService userService, LocationService locationService, Flow flow, BriteDatabase db, JsonSharedPreferencesRepository sharedPreferences) {
		this.actionBarOwner = actionBarOwner;
		this.validator = validator;
		this.userService = userService;
		this.locationService = locationService;
		this.flow = flow;
		this.db = db;
		this.sharedPreferences = sharedPreferences;
	}

	@Override
	protected void onLoad(Bundle savedInstanceState) {
		super.onLoad(savedInstanceState);

		final SettingView view = getView();
		if (view != null) {
			configureActionBar();

			//Image(Picture) Quality
			String currentSelection = sharedPreferences.getObject(SharedPreferencesKeys.SETTING_PICTURE_QUALITY, String.class);
			if(currentSelection != null){
				if(currentSelection.contentEquals("Low")){
					getView().quality.setSelection(0);
				}
				else if(currentSelection.contentEquals("Medium")){
					getView().quality.setSelection(1);
				}
				else if(currentSelection.contentEquals("Normal")){
					getView().quality.setSelection(2);
				}
				else if(currentSelection.contentEquals("Original")){
					getView().quality.setSelection(3);
				}
			}else{
				getView().quality.setSelection(1);
			}

			view.quality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					String currentSelection = getView().quality.getSelectedItem().toString();
					if(currentSelection.contentEquals("Low")){
						currentSelection = "Low";
					}else if(currentSelection.contentEquals("Medium")){
						currentSelection = "Medium";
					}
					else if(currentSelection.contentEquals("Normal")){
						currentSelection = "Normal";
					}
					else if(currentSelection.contentEquals("Original")){
						currentSelection = "Original";
					}
					else{
						currentSelection = "Medium";
					}
					Log.d(TAG, "Current selection is not null, current selection is : " + currentSelection);
					sharedPreferences.putObject(SharedPreferencesKeys.SETTING_PICTURE_QUALITY, currentSelection);
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {

				}
			});

			String address = sharedPreferences.getObject(SharedPreferencesKeys.BLUETOOTH_GPS, String.class);
			String name = sharedPreferences.getObject(SharedPreferencesKeys.BLUETOOTH_GPS_NAME, String.class);
			if(address != null && name != null) {
				view.bluetooth.setText(name+" : "+address);
			}
				//Setup Bluetooth
			view.bluetooth.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
				setupBluetooth(new OnBluetoothPairedListener() {
					@Override
					public void onPaired(String name, String address) {
						sharedPreferences.putObject(SharedPreferencesKeys.BLUETOOTH_GPS_NAME, name);
						sharedPreferences.putObject(SharedPreferencesKeys.BLUETOOTH_GPS, address);
						view.bluetooth.setText(name+" : "+address);
					}
				});
				}
			});

			String imageLocation = sharedPreferences.getObject(SharedPreferencesKeys.SETTING_IMAGE_LOCATION, String.class);
			if(imageLocation != null){
				view.image.setText(imageLocation);
			}
			String asbuildLocation = sharedPreferences.getObject(SharedPreferencesKeys.SETTING_ASBUILD_LOCATION, String.class);
			if(asbuildLocation != null){
				view.asbuild.setText(asbuildLocation);
			}
			String videoLocation = sharedPreferences.getObject(SharedPreferencesKeys.SETTING_VIDEO_LOCATION, String.class);
			if(videoLocation != null){
				view.video.setText(videoLocation);
			}

			view.image.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
//					Toast.makeText(getView().getContext(), "BUTTON CLICKED", Toast.LENGTH_LONG).show();
//					Intent intent = new Intent("SETTING");
//					intent.putExtra("folder", "picture");
//					LocalBroadcastManager.getInstance(getView().getContext()).sendBroadcast(intent);

					SimpleFileDialog FolderChooseDialog = new SimpleFileDialog(getView().getContext(), "FolderChoose", new SimpleFileDialog.SimpleFileDialogListener() {
						@Override
						public void onChosenDir(String chosenDir) {
							// The code in this function will be executed when the dialog OK button is pushed
							Toast.makeText(getView().getContext(), chosenDir, Toast.LENGTH_LONG).show();
							sharedPreferences.putObject(SharedPreferencesKeys.SETTING_IMAGE_LOCATION, chosenDir);
							view.image.setText(chosenDir);

						}
					});
					FolderChooseDialog.chooseFile_or_Dir();
				}
			});

			view.asbuild.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
//					Toast.makeText(getView().getContext(), "BUTTON CLICKED", Toast.LENGTH_LONG).show();
//					Intent intent = new Intent("SETTING");
//					intent.putExtra("folder", "picture");
//					LocalBroadcastManager.getInstance(getView().getContext()).sendBroadcast(intent);

					SimpleFileDialog FolderChooseDialog = new SimpleFileDialog(getView().getContext(), "FolderChoose", new SimpleFileDialog.SimpleFileDialogListener() {
						@Override
						public void onChosenDir(String chosenDir) {
							// The code in this function will be executed when the dialog OK button is pushed
							Toast.makeText(getView().getContext(), chosenDir, Toast.LENGTH_LONG).show();
							sharedPreferences.putObject(SharedPreferencesKeys.SETTING_ASBUILD_LOCATION, chosenDir);
							view.asbuild.setText(chosenDir);
						}
					});
					FolderChooseDialog.chooseFile_or_Dir();
				}
			});

			view.video.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
//					Toast.makeText(getView().getContext(), "BUTTON CLICKED", Toast.LENGTH_LONG).show();
//					Intent intent = new Intent("SETTING");
//					intent.putExtra("folder", "picture");
//					LocalBroadcastManager.getInstance(getView().getContext()).sendBroadcast(intent);

					SimpleFileDialog FolderChooseDialog = new SimpleFileDialog(getView().getContext(), "FolderChoose", new SimpleFileDialog.SimpleFileDialogListener() {
						@Override
						public void onChosenDir(String chosenDir) {
							// The code in this function will be executed when the dialog OK button is pushed
							Toast.makeText(getView().getContext(), chosenDir, Toast.LENGTH_LONG).show();
							sharedPreferences.putObject(SharedPreferencesKeys.SETTING_VIDEO_LOCATION, chosenDir);

							view.video.setText(chosenDir);
						}
					});
					FolderChooseDialog.chooseFile_or_Dir();
				}
			});
//			FolderChooserDialog.Builder(getView().getContext()).
//			new FolderChooserDialog().Builder(getView().getContext())
//					.chooseButton(R.string.md_choose_label)  // changes label of the choose button
//					.initialPath("/sdcard/Download")  // changes initial path, defaults to external storage directory
//					.tag("optional-identifier")
//					.goUpLabel("Up") // custom go up label, default label is "..."
//					.show();
		}
	}

	private void configureActionBar() {
		ActionBarConfig config = new ActionBarConfig.Builder()
				.title("Setting")
				.build();
		actionBarOwner.setConfig(config);
	}


	private void setupBluetooth(final OnBluetoothPairedListener onBluetoothPairedListener){
		btAdapter = BluetoothAdapter.getDefaultAdapter();
		Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
		if (pairedDevices.size() > 0) {
			final BluetoothDevice blueDev[] = new BluetoothDevice[pairedDevices.size()];
			String[] items = new String[blueDev.length];
			int i =0;
			for (BluetoothDevice devicel : pairedDevices) {
				blueDev[i] = devicel;
				items[i] = blueDev[i].getName() + ": " + blueDev[i].getAddress();

				Log.i(TAG, "INFO: PAIRING "+items[i]+"\n");
				//mArrayAdapter.add(device.getName() + "\n" + device.getAddress());
				i++;
			}

			final AlertDialog.Builder builder = new AlertDialog.Builder(getView().getContext());
			builder.setTitle("Choose Bluetooth :");
			builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int item) {
					dialog.dismiss();
					if (item >= 0 && item < blueDev.length) {
						Log.d(TAG, "ADDRESS " + blueDev[item].getAddress());

						onBluetoothPairedListener.onPaired(blueDev[item].getName(), blueDev[item].getAddress());
//						sharedPreferencesRepository.putObject(SharedPreferencesKeys.BLUETOOTH_GPS, blueDev[item].getAddress());
//						turnOnBluetoothReceiver(blueDev[item].getAddress());
					}
				}
			});
			AlertDialog alert = builder.create();
			alert.show();
		}
		else{
			Log.i("ADFINERROR", "ERROR: Check bluetooth connetion");
			Toast.makeText(getView().getContext(), "INFO: Check bluetooth connetion", Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void onFolderSelection(FolderChooserDialog dialog, File folder) {

	}

	private interface OnBluetoothPairedListener{
		public void onPaired(String name, String address);
	}

}
