package com.adfinbureau.wms.screen.picture;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.util.mortar.BaseView;

import javax.inject.Inject;

import butterknife.InjectView;
import mortar.Presenter;

public class PictureView extends BaseView {

	@Inject
	PicturePresenter presenter;

	@InjectView(R.id.recycler)
	RecyclerView recyclerView;

	@InjectView(R.id.fab)
	FloatingActionButton fab;

	public PictureView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected Presenter getPresenter() {
		return presenter;
	}


}
