package com.adfinbureau.wms.screen.todo;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListView;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.util.mortar.BaseView;

import javax.inject.Inject;

import butterknife.InjectView;
import mortar.Presenter;

public class TodoView extends BaseView {

	@Inject
	TodoPresenter presenter;

	@InjectView(android.R.id.list)
	ListView listView;

	@InjectView(android.R.id.empty)
	View emptyView;

	public TodoView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected Presenter getPresenter() {
		return presenter;
	}

	public ListView getListView() {
		return listView;
	}

	public View getEmptyView() {
		return emptyView;
	}
}
