package com.adfinbureau.wms.screen.map;

import android.location.Location;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.screen.home.HomeScreen;
import com.adfinbureau.wms.screen.main.MainScreen;
import com.adfinbureau.wms.screen.splash.SplashScreen;
import com.adfinbureau.wms.screen.survey.SurveyScreen;

import dagger.Provides;
import flow.HasParent;
import flow.Layout;
import mortar.Blueprint;

@Layout(R.layout.view_map)
public class MapScreen implements Blueprint, HasParent<SurveyScreen> {
	private long survey_id;
	private Location location;

	public MapScreen(Location location, long survey_id) {
		this.location = location;
		this.survey_id = survey_id;
	}

	@Override
	public String getMortarScopeName() {
		return getClass().getName();
	}

	@Override
	public Object getDaggerModule() {
		return new Module();
	}

	@Override
	public SurveyScreen getParent() {
		return new SurveyScreen(location);
	}

	@dagger.Module(
			injects = MapView.class,
			addsTo = MainScreen.Module.class,
			library = true
	)
	class Module {
		@Provides
		Location provideLocation() {
			return location;
		}
		@Provides
		String survey() { return String.valueOf(survey_id); }
	}
}
