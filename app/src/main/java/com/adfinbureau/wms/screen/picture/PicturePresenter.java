package com.adfinbureau.wms.screen.picture;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.View;

import com.adfinbureau.wms.SharedPreferencesKeys;
import com.adfinbureau.wms.actionbar.ActionBarConfig;
import com.adfinbureau.wms.actionbar.ActionBarOwner;
import com.adfinbureau.wms.activity.ActivityResultOwner;
import com.adfinbureau.wms.activity.ActivityResultRegistrar;
import com.adfinbureau.wms.model.LocationData;
//import com.adfinbureau.wms.screen.todo.ListsItem;
import com.adfinbureau.wms.repository.JsonSharedPreferencesRepository;
import com.adfinbureau.wms.service.LocationService;
import com.adfinbureau.wms.service.UserService;
import com.adfinbureau.wms.util.adapter.PictureListAdapter;
import com.adfinbureau.wms.util.camera.AlbumStorageDirFactory;
import com.adfinbureau.wms.util.camera.BaseAlbumDirFactory;
import com.adfinbureau.wms.util.camera.FroyoAlbumDirFactory;
import com.adfinbureau.wms.util.helper.FileSplitter;
import com.adfinbureau.wms.util.mortar.BaseViewPresenter;
import com.afollestad.materialdialogs.MaterialDialog;
import com.iceteck.silicompressorr.SiliCompressor;
import com.squareup.sqlbrite.BriteDatabase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Validator;

import flow.Flow;
import id.zelory.compressor.Compressor;
import mortar.MortarScope;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

public class PicturePresenter extends BaseViewPresenter<PictureView> implements PictureListAdapter.OnItemClickListener, ActivityResultOwner.ActivityResultListener{
	private ActionBarOwner actionBarOwner;
	private final ActivityResultRegistrar activityResultRegistrar;

	private final Validator validator;
	private final UserService userService;
	private final LocationService locationService;
	private final Flow flow;
	private final BriteDatabase db;
	private final Location location;
	private final String survey;
	private final JsonSharedPreferencesRepository sharedPreferences;

	private static List<LocationData> items = new ArrayList<>();

	private static final String TAG = "PicturePresenter";
//	static {
//		for (int i = 1; i <= 10; i++) {
//			items.add(new ViewModel("Item " + i, R.drawable.arrow));
//		}
//	}

	@Inject
	PicturePresenter(ActionBarOwner actionBarOwner, Validator validator, UserService userService, LocationService locationService, Flow flow, ActivityResultRegistrar activityResultRegistrar, BriteDatabase db, Location location, String survey, JsonSharedPreferencesRepository sharedPreferences) {
		this.actionBarOwner = actionBarOwner;
		this.validator = validator;
		this.userService = userService;
		this.locationService = locationService;
		this.flow = flow;
		this.activityResultRegistrar = activityResultRegistrar;
		this.db = db;
		this.location = location;
		this.survey = survey;
		this.sharedPreferences = sharedPreferences;
	}

	private AlbumStorageDirFactory mAlbumStorageDirFactory = null;

	@Override
	protected void onLoad(Bundle savedInstanceState) {
		super.onLoad(savedInstanceState);

		final PictureView view = getView();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
			mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
		} else {
			mAlbumStorageDirFactory = new BaseAlbumDirFactory();
		}
		if (view != null) {
			configureActionBar();
			view.recyclerView.setLayoutManager(new GridLayoutManager(view.getContext(), 2));
			setRecyclerAdapter(view.recyclerView);
			view.recyclerView.scheduleLayoutAnimation();

			view.fab.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dispatchTakePictureIntent(ACTION_TAKE_PHOTO_B);
				}
			});

			String x = "SELECT * from "+LocationData.TABLE+" where survey_id = '"+survey+"' and picture IS NOT NULL " +
					"AND longitude = '"+location.getLongitude()+"' "+
					"AND latitude = '"+location.getLatitude()+"'";
			Log.d(TAG, "Query Get Picture : "+x);
			subscription = db.createQuery(LocationData.TABLE,
//					LocationLists.QUERY)
					x)
					.mapToList(LocationData.MAPPER)
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe(adapter);
		}
	}

	private static final int ACTION_TAKE_PHOTO_B = 1;
	private String mCurrentPhotoPath;

	private static final String JPEG_FILE_PREFIX = "IMG_";
	private static final String JPEG_FILE_SUFFIX = ".jpg";

	private File setUpPhotoFile() throws IOException {

		File f = createImageFile();
		mCurrentPhotoPath = f.getAbsolutePath();

		return f;
	}

	/* Photo album for this application */
	private String getAlbumName() {
		return "WMS-Picture";
	}

	private File getAlbumDir() {
		File storageDir = null;

		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
			String imageLocation = sharedPreferences.getObject(SharedPreferencesKeys.SETTING_IMAGE_LOCATION, String.class);
			if(imageLocation != null){
				storageDir = new File(imageLocation);
			}else{
				storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(getAlbumName());

				if (storageDir != null) {
					if (! storageDir.mkdirs()) {
						if (! storageDir.exists()){
							Log.d(TAG, "failed to create directory");
							return null;
						}
					}
				}
			}
		} else {
			Log.v(TAG, "External storage is not mounted READ/WRITE.");
		}

		return storageDir;
	}

	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";
		File albumF = getAlbumDir();
		File imageF = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
		return imageF;
	}

	private void dispatchTakePictureIntent(int actionCode) {

		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		switch(actionCode) {
			case ACTION_TAKE_PHOTO_B:
				File f = null;

				try {
					f = setUpPhotoFile();
					mCurrentPhotoPath = f.getAbsolutePath();
					takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
				} catch (IOException e) {
					e.printStackTrace();
					f = null;
					mCurrentPhotoPath = null;
				}
				break;

			default:
				break;
		} // switch

		activityResultRegistrar.startActivityForResult(actionCode, takePictureIntent);
	}


	private Subscription subscription;
	private PictureListAdapter adapter;

	private void setRecyclerAdapter(RecyclerView recyclerView) {
		adapter = new PictureListAdapter(getView().getContext(), items);
		adapter.setOnItemClickListener(this);
		recyclerView.setAdapter(adapter);
	}

	private void configureActionBar() {
		ActionBarConfig config = new ActionBarConfig.Builder()
				.title("Picture")
				.build();
		actionBarOwner.setConfig(config);
	}

	@Override
	public void onItemClick(View view, final LocationData viewModel) {
		new MaterialDialog.Builder(getView().getContext())
				.title("Picture")
				.content("Description")
				.inputType(InputType.TYPE_CLASS_TEXT |
						InputType.TYPE_TEXT_VARIATION_PERSON_NAME |
						InputType.TYPE_TEXT_FLAG_CAP_WORDS)
				//.inputRange(2, 16)
				.positiveText("Update")
				.input("Edit description", viewModel.description(), false, new MaterialDialog.InputCallback() {
					@Override
					public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
						//showToast("Hello, " + input.toString() + "!");
						//getView().showToast(input.toString(), Toast.LENGTH_LONG);
						if (!viewModel.status().contentEquals("COMPLETE")) {
							db.update(LocationData.TABLE,
									new LocationData.Builder()
											.description(input.toString())
											.build(), LocationData.ID + " = ?", new String[]{String.valueOf(viewModel.id())});
						} else if (viewModel.status().contentEquals("COMPLETE")) {
							db.update(LocationData.TABLE,
									new LocationData.Builder()
											.status("UPDATE")
											.description(input.toString())
											.build(), LocationData.ID + " = ?", new String[]{String.valueOf(viewModel.id())});
						}
						Snackbar.make(getView(), "Description successful updated " + input.toString() + " " + LocationData.ID + " : " + viewModel.id(), Snackbar.LENGTH_LONG).show();
					}
				}).show();
	}

	@Override
	protected void onEnterScope(MortarScope scope) {
		super.onEnterScope(scope);
		activityResultRegistrar.register(scope, this);
	}

	@Override
	protected void onExitScope() {
		super.onExitScope();

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == 0){
			Snackbar.make(getView(), "Cancel capture image", Snackbar.LENGTH_LONG).show();
		}else{
			Snackbar.make(getView(), "Image captured "+mCurrentPhotoPath, Snackbar.LENGTH_LONG).show();
			Log.d(TAG, "Location longitude : " + location.getLongitude() + " | latitude : " + location.getLatitude());

			String currentSelection = sharedPreferences.getObject(SharedPreferencesKeys.SETTING_PICTURE_QUALITY, String.class);
			if(currentSelection != null){
				if(currentSelection.contentEquals("Low")){
					FileSplitter.resizeImage(mCurrentPhotoPath);
				}else if(currentSelection.contentEquals("Medium")){
					String filePath = SiliCompressor.with(getView().getContext()).compress(mCurrentPhotoPath, true);
					try {
						FileSplitter.copyFile(new File(filePath), new File(mCurrentPhotoPath));
						new File(filePath).delete();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				else if(currentSelection.contentEquals("Normal")){
					File photo = new File(mCurrentPhotoPath);
					File compress = Compressor.getDefault(getView().getContext()).compressToFile(photo);
					photo.delete();

					try {
						FileSplitter.copyFile(compress, new File(mCurrentPhotoPath));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				else if(currentSelection.contentEquals("Original")){

				}
			}else{
				String filePath = SiliCompressor.with(getView().getContext()).compress(mCurrentPhotoPath, true);
				try {
					FileSplitter.copyFile(new File(filePath), new File(mCurrentPhotoPath));
					new File(filePath).delete();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			String x = "SELECT * from "+ LocationData.TABLE+" where survey_id = "+survey+" AND "+LocationData.TYPE+" = 'NODE' AND "+LocationData.LONGITUDE+" = '"+location.getLongitude()+"' AND "+LocationData.LATITUDE+" = '"+location.getLatitude()+"'";
			Cursor cursor = db.query(x);
			Log.d(TAG, "CEK NODE  : " + cursor.getCount());
			if(cursor.getCount() == 0) {
				db.insert(LocationData.TABLE,
						new LocationData.Builder()
								.longitude(location.getLongitude() + "")
								.latitude(location.getLatitude() + "")
								.altitude(location.getAltitude() + "")
								.survey(survey)
								.type("NODE")
								.status("PENDING")
								.build());
			}

			Log.d(TAG, "onActivityResult: PICTURE FILE PATH : "+mCurrentPhotoPath);

			db.insert(LocationData.TABLE,
					new LocationData.Builder()
							.longitude(location.getLongitude() + "")
							.latitude(location.getLatitude() + "")
							.altitude(location.getAltitude()+"")
							.type("PICTURE")
							.survey(survey)
							.picture(mCurrentPhotoPath)
							.status("PENDING")
							.build());
		}
	}
}
