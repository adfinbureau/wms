package com.adfinbureau.wms.screen.home;


import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.SharedPreferencesKeys;
import com.adfinbureau.wms.actionbar.ActionBarConfig;
import com.adfinbureau.wms.actionbar.ActionBarOwner;
import com.adfinbureau.wms.activity.ActivityResultOwner;
import com.adfinbureau.wms.activity.ActivityResultRegistrar;
import com.adfinbureau.wms.model.LocationData;
import com.adfinbureau.wms.model.Survey;
import com.adfinbureau.wms.model.User;
import com.adfinbureau.wms.receiver.ConnectionMonitor;
import com.adfinbureau.wms.repository.JsonSharedPreferencesRepository;
import com.adfinbureau.wms.screen.asbuild.AsBuildScreen;
import com.adfinbureau.wms.screen.laporan.LaporanScreen;
import com.adfinbureau.wms.screen.login.LoginScreen;
import com.adfinbureau.wms.screen.picture.PictureScreen;
import com.adfinbureau.wms.screen.setting.SettingScreen;
import com.adfinbureau.wms.screen.survey.SurveyScreen;
import com.adfinbureau.wms.screen.test.TestScreen;
import com.adfinbureau.wms.screen.text.TextScreen;
import com.adfinbureau.wms.screen.video.VideoScreen;
import com.adfinbureau.wms.service.BluetoothLocationService;
import com.adfinbureau.wms.service.GPSLocationService;
import com.adfinbureau.wms.service.GoogleLocationService;
import com.adfinbureau.wms.service.LocationService;
import com.adfinbureau.wms.service.UploadAsbuildServiceListener;
import com.adfinbureau.wms.service.UploadDataService;
import com.adfinbureau.wms.service.UploadPictureServiceListener;
import com.adfinbureau.wms.service.UploadVideoServiceListener;
import com.adfinbureau.wms.service.UserService;
import com.adfinbureau.wms.util.adapter.RecyclerViewAdapter;
import com.adfinbureau.wms.util.adapter.ViewModel;
import com.adfinbureau.wms.util.bluetooth.BlueTooth;
import com.adfinbureau.wms.util.bluetooth.OnLocationReceived;
import com.adfinbureau.wms.util.mortar.BaseViewPresenter;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.squareup.sqlbrite.BriteDatabase;

import org.acra.ACRA;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.validation.Validator;

import flow.Flow;
import mortar.MortarScope;

class HomePresenter extends BaseViewPresenter<HomeView> implements RecyclerViewAdapter.OnItemClickListener, ActivityResultOwner.ActivityResultListener{

	private ActionBarOwner actionBarOwner;
	private final ActivityResultRegistrar activityResultRegistrar;
	private BriteDatabase db;

	private final JsonSharedPreferencesRepository sharedPreferences;
	private final Validator validator;
	private final UserService userService;
	private final LocationService locationService;
	private final Flow flow;
	private static List<ViewModel> items = new ArrayList<>();

	LocationRequest mLocationRequest;
	Location mCurrentLocation;

	@Inject
	JsonSharedPreferencesRepository sharedPreferencesRepository;

	static {
		items.add(new ViewModel("Photo", R.drawable.ic_photo));
		items.add(new ViewModel("Video", R.drawable.ic_video));
		items.add(new ViewModel("As Build", R.drawable.ic_report_new));
		items.add(new ViewModel("Text", R.drawable.ic_text));
		items.add(new ViewModel("Map", R.drawable.ic_location_new));
		items.add(new ViewModel("About", R.drawable.ic_about_new));
		items.add(new ViewModel("Laporan", R.drawable.ic_laporan));
		items.add(new ViewModel("Sign Out", R.drawable.signout));
		items.add(new ViewModel("Settings", R.drawable.setting));
//		items.add(new ViewModel("Dummy", R.drawable.signout));
		items.add(new ViewModel("Exit", R.drawable.signout));

//		for (int i = 1; i <= 10; i++) {
//			items.add(new ViewModel("Item " + i, R.drawable.arrow));
//		}
	}

	@Inject
	HomePresenter(ActionBarOwner actionBarOwner, Validator validator, UserService userService, LocationService locationService, Flow flow, ActivityResultRegistrar activityResultRegistrar, BriteDatabase db, JsonSharedPreferencesRepository sharedPreferences) {
		this.actionBarOwner = actionBarOwner;
		this.validator = validator;
		this.userService = userService;
		this.locationService = locationService;
		this.flow = flow;
		this.activityResultRegistrar = activityResultRegistrar;
		this.db = db;
		this.sharedPreferences = sharedPreferences;
	}

	//GPSLocationService mBoundGPSService;
	//GPSBluetoothService mBoundBluetoothService;

	boolean mServiceBound = false;

	private void updateView(Intent intent){
		double longitude = intent.getDoubleExtra("longitude", 0);
		double latitude = intent.getDoubleExtra("latitude", 0);
		double altitude = intent.getDoubleExtra("altitude", 0);

		mCurrentLocation = new Location("GoogleLocation");
		mCurrentLocation.setLongitude(longitude);
		mCurrentLocation.setLatitude(latitude);
		mCurrentLocation.setAltitude(altitude);

		if (getView() != null) {
			getView().lng.setText("Longitude : " + longitude + "");
			getView().lat.setText("Latitude : " + latitude + "");
			getView().alt.setText("Elevasi : " + altitude + "");
		}

		Log.d(TAG, "Update Location Detected : Longitude : " + longitude + " | " + "Latitude : " + latitude + " | " + "Elevasi : " + altitude);
	}

	// Define the callback for what to do when data is received
	private BroadcastReceiver testReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			updateView(intent);
		}
	};

	@Override
	protected void onExitScope() {
		super.onExitScope();

		Log.d(TAG, "unregister receiver");
		LocalBroadcastManager.getInstance(getView().getContext()).unregisterReceiver(testReceiver);
	}

	//GPS, Mobile, Bluetooth
	private String currentGpsOption = "GPS";
	Intent intent;
	Intent intentGPS;
	Intent intentBluetooth;

	@Override
	protected void onLoad(Bundle savedInstanceState) {
		super.onLoad(savedInstanceState);

		if(userAccountExists()) {
			HomeView view = getView();

			LocalBroadcastManager.getInstance(getView().getContext()).registerReceiver(testReceiver, filter);


			//Turn off all service first
			intent = new Intent(getView().getContext(), GoogleLocationService.class);
			intentGPS = new Intent(getView().getContext(), GPSLocationService.class);
			intentBluetooth = new Intent(getView().getContext(), BluetoothLocationService.class);

			String currentSelection = sharedPreferencesRepository.getObject(SharedPreferencesKeys.LOCATION_SOURCE, String.class);
			if(currentSelection != null){
				if(currentSelection.contentEquals("None")){
					getView().spinnerLocationSource.setSelection(0);
				}else if(currentSelection.contentEquals("Mobile")){
					getView().spinnerLocationSource.setSelection(1);
				}else if(currentSelection.contentEquals("GPS")){
					getView().spinnerLocationSource.setSelection(2);
				}else if(currentSelection.contentEquals("Bluetooth")){
					getView().spinnerLocationSource.setSelection(3);
				}
			}
			//set default selection
//			String currentSelection = getView().spinnerLocationSource.getSelectedItem().toString();
//			if(currentSelection.contentEquals("None")){
//				getView().spinnerLocationSource.setSelection(0);
//			}else if(currentSelection.contentEquals("Mobile")){
//				getView().spinnerLocationSource.setSelection(1);
//			}else if(currentSelection.contentEquals("GPS Altea")){
//				getView().spinnerLocationSource.setSelection(2);
//			}else if(currentSelection.contentEquals("Bluetooth")){
//				getView().spinnerLocationSource.setSelection(3);
//			}
			//end set default selection

			view.spinnerLocationSource.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					String currentSelection = getView().spinnerLocationSource.getSelectedItem().toString();
					if(currentSelection.contentEquals("GPS Mobile")){
						currentSelection = "Mobile";
					}else if(currentSelection.contentEquals("GPS Altea")){
						currentSelection = "GPS";
					}else if(currentSelection.contentEquals("Bluetooth")){
						currentSelection = "Bluetooth";
					}else{
						currentSelection = "None";
					}
					Log.d(TAG, "Current selection is not null, current selection is : "+currentSelection);
					sharedPreferences.putObject(SharedPreferencesKeys.LOCATION_SOURCE, currentSelection);

					LocalBroadcastManager.getInstance(getView().getContext()).unregisterReceiver(testReceiver);

					//getView().getContext().stopService(intent);
					getView().getContext().stopService(intentGPS);
//					getView().getContext().stopService(intentBluetooth);

					setupLocationSource();
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {

				}
			});

			//auto upload
			if((sharedPreferencesRepository.getObject(SharedPreferencesKeys.AUTOUPLOAD, String.class) != null) &&
					!sharedPreferencesRepository.getObject(SharedPreferencesKeys.AUTOUPLOAD, String.class).contentEquals("")	){
				view.aSwitch.setChecked(true);
			}
			else{
				view.aSwitch.setChecked(false);
			}

			view.aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					Context context = getView().getContext();

					if (isChecked) {
						sharedPreferencesRepository.putObject(SharedPreferencesKeys.AUTOUPLOAD, "true");

						Intent msgIntent = new Intent(context, UploadDataService.class);
						context.startService(msgIntent);

						msgIntent = new Intent(context, UploadPictureServiceListener.class);
						context.startService(msgIntent);

						msgIntent = new Intent(context, UploadVideoServiceListener.class);
						context.startService(msgIntent);

						msgIntent = new Intent(context, UploadAsbuildServiceListener.class);
						context.startService(msgIntent);
					} else {
						sharedPreferencesRepository.putObject(SharedPreferencesKeys.AUTOUPLOAD, "");

						Intent msgIntent = new Intent(context, UploadDataService.class);
						context.stopService(msgIntent);
						msgIntent = new Intent(context, UploadPictureServiceListener.class);
						context.stopService(msgIntent);
						msgIntent = new Intent(context, UploadVideoServiceListener.class);
						context.stopService(msgIntent);
						msgIntent = new Intent(context, UploadAsbuildServiceListener.class);
						context.stopService(msgIntent);
					}
				}
			});


			if (view != null) {
				configureActionBar();
				view.recyclerView.setHasFixedSize(true);
				view.recyclerView.setLayoutManager(new GridLayoutManager(view.getContext(), 2));
				setRecyclerAdapter(view.recyclerView);
			}
		}else{
			Snackbar.make(getView(), "User not exists", Snackbar.LENGTH_LONG).show();
		}
	}

	IntentFilter filterBluetooth = new IntentFilter("BluetoothLocationService");
	IntentFilter filter = new IntentFilter("GoogleLocationService");
	IntentFilter filterGPS = new IntentFilter("GPSLocationService");

	private void setupLocationSource() {
		currentGpsOption = sharedPreferencesRepository.getObject(SharedPreferencesKeys.LOCATION_SOURCE, String.class);

		//Should turn on
		if(!ConnectionMonitor.isMyServiceRunning(getView().getContext(), GoogleLocationService.class)){
			Log.d(TAG, "GoogleLocationService starting...");
			getView().getContext().startService(intent);
		}
		else{
			Log.d(TAG, "GoogleLocationService already running");
		}

		//GPS MOBILE
		if(currentGpsOption.contentEquals("Mobile")) {
			Log.d(TAG, "Register receiver");
			LocalBroadcastManager.getInstance(getView().getContext()).registerReceiver(testReceiver, filter);
		}
		//END GPS MOBILE

		//GPS ALTEA
		else if(currentGpsOption.contentEquals("GPS")) {
			if (!ConnectionMonitor.isMyServiceRunning(getView().getContext(), GPSLocationService.class)) {
				Log.d(TAG, "GPSLocationService starting...");
				getView().getContext().startService(intentGPS);
			} else {
				Log.d(TAG, "GPSLocationService already running");
			}

			Log.d(TAG, "Register receiver");
			LocalBroadcastManager.getInstance(getView().getContext()).registerReceiver(testReceiver, filterGPS);
		}
		//END GPS ALTEA

		//BLUETOOTH
		else if(currentGpsOption.contentEquals("Bluetooth")) {
			if (!ConnectionMonitor.isMyServiceRunning(getView().getContext(), BluetoothLocationService.class)) {
				getView().getContext().startService(intentBluetooth);
			} else {
				Log.d(TAG, "BluetoothLocationService already running");
			}
			Log.d(TAG, "Register bluetooth receiver");
			LocalBroadcastManager.getInstance(getView().getContext()).registerReceiver(testReceiver, filterBluetooth);


//			getView().getContext().stopService(intentBluetooth);
//			setupBluetooth(new OnBluetoothPairedListener() {
//				@Override
//				public void onPaired(String address) {
//				if (!ConnectionMonitor.isMyServiceRunning(getView().getContext(), BluetoothLocationService.class)) {
//					Log.d(TAG, "BluetoothLocationService starting...");
//					intentBluetooth.putExtra("address", address);
//					getView().getContext().startService(intentBluetooth);
//				} else {
//					Log.d(TAG, "BluetoothLocationService already running");
//				}
//
//				Log.d(TAG, "Register receiver");
//				LocalBroadcastManager.getInstance(getView().getContext()).registerReceiver(testReceiver, filterBluetooth);
//				}
//			});

		}
		else{

		}
		//END BLUETOOTH
	}

	private void dummyData(){
		String x = "SELECT * from "+ LocationData.TABLE;
		if(db.query(x).getCount() == 0) {
			getView().showToast("DUMMY DATA", Toast.LENGTH_LONG);
			double longitude = 106.7305655;
			double latitude = -6.4216066;
			for (int i = 0; i < 10; i++) {
				long id = db.insert(Survey.TABLE, new Survey.Builder().data("HOME").build());
				for(int j=0;j<50;j++){
					db.insert(LocationData.TABLE,
							new LocationData.Builder()
									.longitude(longitude + "")
									.latitude(latitude + "")
									.survey(id+"")
									.type("NODE")
									.status("PENDING")
									.build());

//					db.insert(LocationData.TABLE,
//							new LocationData.Builder()
//									.longitude(longitude + "")
//									.latitude(latitude + "")
//									.type("PICTURE")
//									.survey(id+"")
//									.picture("/sdcard/Pictures/WMS-Picture/IMG_20160618_000347_-1182709610.jpg")
//									.status("PENDING")
//									.build());

					longitude+=0.001;
				}
				latitude+=0.001;
			}
		}
		else{
			getView().showToast("DATA EXIST REMOVE", Toast.LENGTH_LONG);
			db.execute("DELETE FROM "+Survey.TABLE);
			db.execute("DELETE FROM "+LocationData.TABLE);
		}
	}

	private void setRecyclerAdapter(RecyclerView recyclerView) {
		RecyclerViewAdapter adapter = new RecyclerViewAdapter(items);
		adapter.setOnItemClickListener(this);
		recyclerView.setAdapter(adapter);
	}

	private void configureActionBar() {
		ActionBarConfig config = new ActionBarConfig.Builder()
				.visible(false)
				.build();
		actionBarOwner.setConfig(config);
	}

	protected boolean userAccountExists() {
		return sharedPreferences.getObject(SharedPreferencesKeys.USER_ACCOUNT, User.class) != null;
	}

	@Override
	public void onItemClick(View view, ViewModel viewModel) {
		Log.d(TAG, viewModel.getText());
		if(viewModel.getText().contentEquals("Test")){
			flow.goTo(new TestScreen());
		}
		if(viewModel.getText().contentEquals("Settings")){
			flow.goTo(new SettingScreen());
		}

		if(viewModel.getText().contentEquals("Laporan")){
			flow.goTo(new LaporanScreen());
		}
		if(viewModel.getText().contentEquals("Sign Out")){
			new MaterialDialog.Builder(getView().getContext())
					.content("Anda yakin ingin sign out?")
					.positiveText("Ya")
					.negativeText("Tidak")
					.onPositive(new MaterialDialog.SingleButtonCallback() {
						@Override
						public void onClick(MaterialDialog dialog, DialogAction which) {
							sharedPreferences.putObject(SharedPreferencesKeys.USER_ACCOUNT, null);
							flow.goTo(new LoginScreen());
						}
					})
					.onNegative(new MaterialDialog.SingleButtonCallback() {
						@Override
						public void onClick(MaterialDialog dialog, DialogAction which) {

						}
					})
					.show();
		}
		if(viewModel.getText().contentEquals("Exit")) {
			new MaterialDialog.Builder(getView().getContext())
				.content("Anda yakin ingin keluar dari aplikasi ini?")
				.positiveText("Ya")
				.negativeText("Tidak")
				.onPositive(new MaterialDialog.SingleButtonCallback() {
					@Override
					public void onClick(MaterialDialog dialog, DialogAction which) {
						System.exit(0);  // die die die!
//						flow.goBack();
					}
				})
				.onNegative(new MaterialDialog.SingleButtonCallback() {
					@Override
					public void onClick(MaterialDialog dialog, DialogAction which) {

					}
				})
				.show();
		}
		if(viewModel.getText().contentEquals("Dummy")) {
			dummyData();
		}
		else {
			if (mCurrentLocation != null) {
				if (viewModel.getText().contentEquals("Photo")) {
					long id = db.insert(Survey.TABLE, new Survey.Builder().data("HOME").build());
					flow.goTo(new PictureScreen(mCurrentLocation, id));
				} else if (viewModel.getText().contentEquals("Video")) {
					long id = db.insert(Survey.TABLE, new Survey.Builder().data("HOME").build());
					flow.goTo(new VideoScreen(mCurrentLocation, id));
				} else if (viewModel.getText().contentEquals("As Build")) {
					long id = db.insert(Survey.TABLE, new Survey.Builder().data("HOME").build());
					flow.goTo(new AsBuildScreen(mCurrentLocation, id));
				} else if (viewModel.getText().contentEquals("Text")) {
					long id = db.insert(Survey.TABLE, new Survey.Builder().data("HOME").build());
					flow.goTo(new TextScreen(mCurrentLocation, id));
				} else if (viewModel.getText().contentEquals("Map")) {
					flow.goTo(new SurveyScreen(mCurrentLocation));
				}
			} else {
				Toast.makeText(getView().getContext(), "Location not found, please wait ...", Toast.LENGTH_LONG).show();
			}
		}
	}

	private BluetoothAdapter btAdapter = null;
	private static final String TAG = "HomePresenter";

	private interface OnBluetoothPairedListener{
		public void onPaired(String address);
	}

	private void setupBluetooth(final OnBluetoothPairedListener onBluetoothPairedListener){
		btAdapter = BluetoothAdapter.getDefaultAdapter();
		Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
		if (pairedDevices.size() > 0) {
			final BluetoothDevice blueDev[] = new BluetoothDevice[pairedDevices.size()];
			String[] items = new String[blueDev.length];
			int i =0;
			for (BluetoothDevice devicel : pairedDevices) {
				blueDev[i] = devicel;
				items[i] = blueDev[i].getName() + ": " + blueDev[i].getAddress();

				Log.i(TAG, "INFO: PAIRING "+items[i]+"\n");
				//mArrayAdapter.add(device.getName() + "\n" + device.getAddress());
				i++;
			}

			final AlertDialog.Builder builder = new AlertDialog.Builder(getView().getContext());
			builder.setTitle("Choose Bluetooth:");
			builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int item) {
					dialog.dismiss();
					if (item >= 0 && item < blueDev.length) {
						Log.d(TAG, "ADDRESS " + blueDev[item].getAddress());

						onBluetoothPairedListener.onPaired(blueDev[item].getAddress());
//						sharedPreferencesRepository.putObject(SharedPreferencesKeys.BLUETOOTH_GPS, blueDev[item].getAddress());
//						turnOnBluetoothReceiver(blueDev[item].getAddress());
					}
				}
			});
			AlertDialog alert = builder.create();
			alert.show();
		}
		else{
			Log.i("ADFINERROR", "ERROR: Check bluetooth connetion");
			Toast.makeText(getView().getContext(), "INFO: Check bluetooth connetion", Toast.LENGTH_LONG).show();
		}
	}

	@Override
	protected void onEnterScope(MortarScope scope) {
		super.onEnterScope(scope);
		activityResultRegistrar.register(scope, this);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Snackbar.make(getView(), "TETS", Snackbar.LENGTH_LONG).show();
	}
}