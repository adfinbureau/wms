package com.adfinbureau.wms.screen.text;


import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.adfinbureau.wms.actionbar.ActionBarConfig;
import com.adfinbureau.wms.actionbar.ActionBarOwner;
import com.adfinbureau.wms.model.LocationData;
import com.adfinbureau.wms.model.LocationText;
import com.adfinbureau.wms.model.UserWMS;
import com.adfinbureau.wms.service.LocationService;
import com.adfinbureau.wms.service.UserService;
import com.adfinbureau.wms.util.adapter.RecyclerViewAdapter;
import com.adfinbureau.wms.util.adapter.ViewModel;
import com.adfinbureau.wms.util.mortar.BaseViewPresenter;
import com.squareup.sqlbrite.BriteDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import flow.Flow;

class AddTextPresenter extends BaseViewPresenter<AddTextView>{

	private ActionBarOwner actionBarOwner;

	private final Validator validator;
	private final UserService userService;
	private final LocationService locationService;
	private final Flow flow;
	private final String survey;
	private final Location location;
	private final BriteDatabase db;

	private LocationData locationData;

	private static final String TAG = "AddTextPresenter";

	@Inject
	AddTextPresenter(ActionBarOwner actionBarOwner, Validator validator, UserService userService, LocationService locationService, Flow flow, String survey, Location location, BriteDatabase db, LocationData locationData) {
		this.actionBarOwner = actionBarOwner;
		this.validator = validator;
		this.userService = userService;
		this.locationService = locationService;
		this.flow = flow;
		this.survey = survey;
		this.location = location;
		this.db = db;

		this.locationData = locationData;
	}

	@Override
	protected void onLoad(Bundle savedInstanceState) {
		super.onLoad(savedInstanceState);

		AddTextView view = getView();
		if (view != null) {
			configureActionBar();

			if(locationData!=null) {
				view.masalahEditText.setText(locationData.masalah());
				view.usulanEditText.setText(locationData.usulan());
			}
		}
	}


	private void configureActionBar() {
		ActionBarConfig config = new ActionBarConfig.Builder()
				.title("Add / Update Text")
				.build();
		actionBarOwner.setConfig(config);
	}

	public void save(String masalah, String usulan) {
		final LocationText u = new LocationText(masalah, usulan);

		Set<ConstraintViolation<LocationText>> errors = validator.validate(u);

		if(errors.size() > 0) {
			getView().onValidationError(errors);
		}else{
//			db.update(LocationData.TABLE,
//					new LocationData.Builder()
//							.status("UPDATE")
//							.description(input.toString())
//							.build(), LocationData.ID + " = ?", new String[]{String.valueOf(viewModel.id())});

			String x = "SELECT * from "+ LocationData.TABLE+" where survey_id = "+survey+" AND "+LocationData.TYPE+" = 'NODE' AND "+LocationData.LONGITUDE+" = '"+location.getLongitude()+"' AND "+LocationData.LATITUDE+" = '"+location.getLatitude()+"'";
			Cursor cursor = db.query(x);
			Log.d(TAG, "CEK NODE  : " + cursor.getCount());
			if(cursor.getCount() == 0) {
				db.insert(LocationData.TABLE,
						new LocationData.Builder()
								.longitude(location.getLongitude() + "")
								.latitude(location.getLatitude() + "")
								.altitude(location.getAltitude() + "")
								.survey(survey)
								.type("NODE")
								.status("PENDING")
								.build());
			}

			if(locationData == null) {
				db.insert(LocationData.TABLE, new LocationData.Builder()
						.survey(survey)
						.longitude(location.getLongitude() + "")
						.latitude(location.getLatitude() + "")
						.altitude(location.getAltitude() + "")
						.type("MASALAH")
						.masalah(masalah)
						.usulan(usulan)
						.status("PENDING").build());
			}else{
				db.update(LocationData.TABLE, new LocationData.Builder()
					.masalah(masalah)
					.usulan(usulan)
					.status("UPDATE").build(), LocationData.ID + " = ?", new String[]{String.valueOf(locationData.id())}
				);
			}
			flow.goBack();
		}
	}
}
