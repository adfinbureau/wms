package com.adfinbureau.wms.screen.laporan;

import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.actionbar.ActionBarConfig;
import com.adfinbureau.wms.actionbar.ActionBarOwner;
import com.adfinbureau.wms.activity.ActivityResultOwner;
import com.adfinbureau.wms.activity.ActivityResultRegistrar;
import com.adfinbureau.wms.db.Db;
import com.adfinbureau.wms.model.LocationData;
import com.adfinbureau.wms.model.LocationProgress;
import com.adfinbureau.wms.model.Survey;
import com.adfinbureau.wms.screen.asbuild.AsBuildScreen;
import com.adfinbureau.wms.screen.map.MapScreen;
import com.adfinbureau.wms.screen.picture.PictureScreen;
import com.adfinbureau.wms.screen.text.TextScreen;
import com.adfinbureau.wms.screen.video.VideoScreen;
import com.adfinbureau.wms.service.LocationService;
import com.adfinbureau.wms.service.UserService;
import com.adfinbureau.wms.util.adapter.LocationListAdapter;
import com.adfinbureau.wms.util.adapter.SurveyListAdapter;
import com.adfinbureau.wms.util.camera.AlbumStorageDirFactory;
import com.adfinbureau.wms.util.camera.BaseAlbumDirFactory;
import com.adfinbureau.wms.util.camera.FroyoAlbumDirFactory;
import com.adfinbureau.wms.util.helper.FileSplitter;
import com.adfinbureau.wms.util.mortar.BaseViewPresenter;
import com.adfinbureau.wms.util.widgets.DividerItemDecoration;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bignerdranch.android.multiselector.MultiSelector;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.sqlbrite.BriteDatabase;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Validator;

import flow.Flow;
import mortar.MortarScope;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

//import com.adfinbureau.wms.screen.todo.ListsItem;

public class LaporanPresenter extends BaseViewPresenter<LaporanView> implements ActivityResultOwner.ActivityResultListener{
	private ActionBarOwner actionBarOwner;
	private final ActivityResultRegistrar activityResultRegistrar;
	private final Flow flow;
	private final BriteDatabase db;

	private static List<LocationData> items = new ArrayList<LocationData>();

	@Inject
	LaporanPresenter(ActionBarOwner actionBarOwner, Flow flow, ActivityResultRegistrar activityResultRegistrar, BriteDatabase db) {
		this.actionBarOwner = actionBarOwner;
		this.flow = flow;
		this.activityResultRegistrar = activityResultRegistrar;
		this.db = db;
	}

	String[] TYPELIST = {"NODE", "PICTURE", "VIDEO", "ASBUILD", "MASALAH"};
	String[] STATUSLIST = {"PENDING", "PROGRESS", "COMPLETE", "UPDATE"};

	@Override
	protected void onLoad(Bundle savedInstanceState) {
		super.onLoad(savedInstanceState);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
			mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
		} else {
			mAlbumStorageDirFactory = new BaseAlbumDirFactory();
		}

		final LaporanView view = getView();
		view.type.setAdapter(new ArrayAdapter<String>(getView().getContext(), android.R.layout.simple_dropdown_item_1line, TYPELIST));
		view.status.setAdapter(new ArrayAdapter<String>(getView().getContext(), android.R.layout.simple_dropdown_item_1line, STATUSLIST));

		view.type.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				Snackbar.make(getView(), "Type " + TYPELIST[position], Snackbar.LENGTH_LONG).show();

				filterList(view.type.getText().toString(), view.status.getText().toString());
			}
		});

		view.status.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				Snackbar.make(getView(), "Status "+STATUSLIST[position], Snackbar.LENGTH_LONG).show();
				filterList(view.type.getText().toString(), view.status.getText().toString());
			}
		});

		FloatingActionButton fab = view.fab;
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				final MultiSelector multiSelector = adapter.getmMultiSelector();
				if(multiSelector.getSelectedPositions().size() > 0){
					new MaterialDialog.Builder(getView().getContext())
						.title(R.string.delete_confirmation_selection)
						.onNegative(new MaterialDialog.SingleButtonCallback() {
							@Override
							public void onClick(MaterialDialog dialog, DialogAction which) {

							}
						})
						.onPositive(new MaterialDialog.SingleButtonCallback() {
							@Override
							public void onClick(MaterialDialog dialog, DialogAction which) {
								int jumlahData = multiSelector.getSelectedPositions().size();
								for(int selected : multiSelector.getSelectedPositions()){
									LocationData locData = adapter.getItem(selected);
									String x = "SELECT * from " + LocationData.TABLE + " WHERE "+LocationData.ID+" = '"+locData.id()+"'";
//									String x = "SELECT * from " + LocationData.TABLE + " WHERE " + LocationData.STATUS + " = 'COMPLETE' AND "+LocationData.ID+" = '"+locData.id()+"'";
									Cursor cursor = db.query(x);
									Log.d("LaporanPresenter", "JUMLAH DATA NODE YANG AKAN DIHAPUS : " + cursor.getCount());
									if (cursor.getCount() > 0) {
										List<LocationData> locations = new ArrayList<LocationData>();
										try {
											while (cursor.moveToNext()) {
												final String type = Db.getString(cursor, LocationData.TYPE);

												//delete file node
												if (type.contentEquals("PICTURE")) {
													final String picture = Db.getString(cursor, LocationData.PICTURE);
													FileSplitter.deleteParts(picture);
													File file = new File(picture);
													file.delete();
												} else if (type.contentEquals("ASBUILD")) {
													final String asbuild = Db.getString(cursor, LocationData.AS_BUILD);
													FileSplitter.deleteParts(asbuild);
													File file = new File(asbuild);
													file.delete();
												} else if (type.contentEquals("VIDEO")) {
													final String video = Db.getString(cursor, LocationData.VIDEO);
													FileSplitter.deleteParts(video);
													File file = new File(video);
													file.delete();
												}

												//delete node
												String queryDelete = "DELETE FROM " + LocationData.TABLE + " WHERE "+LocationData.ID+" = '"+locData.id()+"'";
//												String queryDelete = "DELETE FROM " + LocationData.TABLE + " WHERE " + LocationData.STATUS + " = 'COMPLETE' AND "+LocationData.ID+" = '"+locData.id()+"'";
												db.executeAndTrigger(LocationData.TABLE, queryDelete);

												//delete progress
												String progressDelete = "DELETE FROM "+ LocationProgress.TABLE + "WHERE "+LocationProgress.SERVER_ID+" = '"+locData.serverId()+"'";
												db.executeAndTrigger(LocationProgress.TABLE, progressDelete);

												//check survey
												final String survey = Db.getString(cursor, LocationData.SURVEY_ID);
												String nodeInSurvey = "SELECT * from "+LocationData.TABLE+" where "+LocationData.SURVEY_ID+" = '"+survey+"'";
												Cursor cursorNodeInSurvey = db.query(nodeInSurvey);
												Log.d("LaporanPresenter", "JUMLAH DATA NODE YANG AKAN DIHAPUS : " + cursor.getCount());

												if (cursorNodeInSurvey.getCount() == 0) {
													Log.d("LaporanPresenter", "Tidak ada node didalam survey ini");

													//delete survey
													db.executeAndTrigger(Survey.TABLE, "DELETE FROM "+Survey.TABLE+" where "+Survey.ID+" = '"+survey+"'");
												}
											}
										} catch (Exception e) {

										}
									}
									adapter.getmMultiSelector().clearSelections();
									Snackbar.make(getView(), jumlahData+" data berhasil dihapus", Snackbar.LENGTH_LONG)
											.setAction("Action", null).show();

								}
							}
						})
						.positiveText(R.string.yes)
						.negativeText(R.string.no)
						.show();
				}else{
					new MaterialDialog.Builder(getView().getContext())
						.title(R.string.delete_confirmation)
						.onNegative(new MaterialDialog.SingleButtonCallback() {
							@Override
							public void onClick(MaterialDialog dialog, DialogAction which) {

							}
						})
						.onPositive(new MaterialDialog.SingleButtonCallback() {
							@Override
							public void onClick(MaterialDialog dialog, DialogAction which) {
								try {
									FileUtils.cleanDirectory(getAlbumDir("WMS-Picture"));
									FileUtils.cleanDirectory(getAlbumDir("WMS-As-Build"));
									FileUtils.cleanDirectory(getAlbumDir("WMS-Video"));
								} catch (IOException e) {
									e.printStackTrace();
								} finally{
									//delete all node
									String queryDelete = "DELETE FROM " + LocationData.TABLE;
									db.executeAndTrigger(LocationData.TABLE, queryDelete);

									//delete all survey
									String surveyDelete = "DELETE FROM " + Survey.TABLE;
									db.executeAndTrigger(Survey.TABLE, surveyDelete);

									//delete all progress
									String progressDelete = "DELETE FROM "+ LocationProgress.TABLE;
									db.executeAndTrigger(LocationProgress.TABLE, progressDelete);
								}

							}
						})
						.positiveText(R.string.yes)
						.negativeText(R.string.no)
						.show();
				}
			}
		});

		if (view != null) {
			configureActionBar();

			RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(view.getContext());
			view.recyclerView.setLayoutManager(mLayoutManager);
			view.recyclerView.setItemAnimator(new DefaultItemAnimator());
			//view.recyclerView.setAdapter(adapter);
			//view.recyclerView.setLayoutManager(new GridLayoutManager(view.getContext(), 2));
			view.recyclerView.addItemDecoration(new DividerItemDecoration(getView().getContext(), LinearLayoutManager.VERTICAL));
			setRecyclerAdapter(view.recyclerView);
			//view.recyclerView.scheduleLayoutAnimation();

			String x = "SELECT * from "+ LocationData.TABLE;
			subscription = db.createQuery(LocationData.TABLE,
//					LocationLists.QUERY)
					x)
					.mapToList(LocationData.MAPPER)
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe(adapter);
		}
	}

	private AlbumStorageDirFactory mAlbumStorageDirFactory = null;

	private static final String TAG = "LaporanPresenter";

	private File getAlbumDir(String albumName) {
		File storageDir = null;
		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
			storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(albumName);
			if (storageDir != null) {
				if (!storageDir.mkdirs()) {
					if (!storageDir.exists()){
						Log.d(TAG, "failed to create directory");
						return null;
					}
				}
			}
		} else {
			Log.v(TAG, "External storage is not mounted READ/WRITE.");
		}
		return storageDir;
	}

	private void filterList(String type, String status){
		String x = "SELECT * from "+ LocationData.TABLE+" WHERE "+LocationData.TYPE+" = '"+type+"' AND "+LocationData.STATUS+" = '"+status+"'";
		subscription = db.createQuery(LocationData.TABLE,
				x)
				.mapToList(LocationData.MAPPER)
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(adapter);
	}
	private Subscription subscription;
	private LocationListAdapter adapter;

	private void setRecyclerAdapter(RecyclerView recyclerView) {
		adapter = new LocationListAdapter(getView().getContext(), items, db);
//		adapter.setOnItemClickListener(this);
		recyclerView.setAdapter(adapter);
	}

	private void configureActionBar() {
		ActionBarConfig config = new ActionBarConfig.Builder()
				.title("Laporan / History")
				.build();
		actionBarOwner.setConfig(config);
	}

	@Override
	protected void onEnterScope(MortarScope scope) {
		super.onEnterScope(scope);
		activityResultRegistrar.register(scope, this);
	}

	@Override
	protected void onExitScope() {
		super.onExitScope();

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	}
}
