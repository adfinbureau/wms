package com.adfinbureau.wms.screen.main;

import static android.content.Intent.*;
import static com.atomicleopard.expressive.Expressive.map;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import com.adfinbureau.wms.activity.ActivityResultOwner;
import com.adfinbureau.wms.receiver.ConnectionMonitor;

import com.adfinbureau.wms.repository.JsonSharedPreferencesRepository;
//import com.adfinbureau.wms.service.RegistrationIntentService;
import com.adfinbureau.wms.util.bluetooth.BlueTooth;
import com.adfinbureau.wms.util.bluetooth.OnLocationReceived;
import com.afollestad.materialdialogs.folderselector.FolderChooserDialog;
import com.cocosw.bottomsheet.BottomSheet;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.adfinbureau.wms.R;
import com.adfinbureau.wms.SharedPreferencesKeys;
import com.adfinbureau.wms.actionbar.ActionBarConfig;
import com.adfinbureau.wms.actionbar.ActionBarOwner;
import com.adfinbureau.wms.actionbar.MenuItemSelectionHandler;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.TypedArray;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.InjectView;
import flow.Flow;
import mortar.Mortar;
import mortar.MortarActivityScope;
import mortar.MortarScope;

public class MainActivity extends AppCompatActivity implements ActionBarOwner.View,
        ActivityResultOwner.View, FolderChooserDialog.FolderCallback{

  @Inject MixpanelAPI mixpanel;
  @Inject ActionBarOwner actionBarOwner;
  @Inject
  ActivityResultOwner activityResultOwner;
  @Inject SharedPreferences sharedPreferences;
  @Inject
  JsonSharedPreferencesRepository sharedPreferencesRepository;

  @InjectView(R.id.app_toolbar) Toolbar actionBarToolbar;
  @InjectView(R.id.container) MainView mainView;

  private MortarActivityScope activityScope;
  private Flow mainFlow;
  private Map<Integer, MenuItemSelectionHandler> menuItemSelectionHandlers;
  private ConnectionMonitor mConnectionReceiver;

  private synchronized void startMonitoringConnection() {
    IntentFilter aFilter = new IntentFilter(
            ConnectivityManager.CONNECTIVITY_ACTION);
    registerReceiver(mConnectionReceiver, aFilter);
  }

  private synchronized void stopMonitoringConnection() {
    unregisterReceiver(mConnectionReceiver);
  }

  @Override
  public Object getSystemService(String name) {
    if (Mortar.isScopeSystemService(name)) {
      return activityScope;
    }
    return super.getSystemService(name);
  }

  /**
   * Inform the view about back events.
   */
  @Override
  public void onBackPressed() {
    // Give the view a chance to handle going back. If it declines the honor, let super do its thing.
    if (!mainFlow.goBack()) {
      super.onBackPressed();
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
//    registerReceiver();
  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu items for use in the action bar
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu_main, menu);
    return super.onCreateOptionsMenu(menu);
  }

  /**
   * Inform the view about up events.
   */
  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    MenuItemSelectionHandler action = menuItemSelectionHandlers.get(item.getItemId());
    if (action != null) {
      return action.execute();
    }

    return super.onOptionsItemSelected(item);
  }

  @Override
  public void setActionBarConfig(ActionBarConfig config) {
    final ActionBar actionBar = getSupportActionBar();
    if (actionBar == null) {
      return;
    }

    String title = config.getTitle();
    actionBar.setTitle(title != null ? title : getString(R.string.app_name));

    if (config.isVisible() && !actionBar.isShowing()) {
      actionBar.show();

      // since actionbar is in overlay mode, set the container padding to compensate
      mainView.setPadding(0, getActionBarHeight(), 0, 0);
    } else if (!config.isVisible() && actionBar.isShowing()) {
      actionBar.hide();

      // remove padding so we get full bleed when action bar is hidden
      mainView.setPadding(0, 0, 0, 0);
    }

    actionBar.setDisplayHomeAsUpEnabled(config.isHomeAsUpEnabled());
  }

  @Override
  public Context getMortarContext() {
    return this;
  }

//  private BroadcastReceiver mRegistrationBroadcastReceiver;
  private boolean isReceiverRegistered;

  private static final long INTERVAL = 1000 * 10;
  private static final long FASTEST_INTERVAL = 1000 * 5;

  BroadcastReceiver testReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      Toast.makeText(MainActivity.this, intent.getStringExtra("message"), Toast.LENGTH_LONG).show();
    }
  };

  BroadcastReceiver settingReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
//      FolderChooserDialog.Builder(context)
//      Toast.makeText(MainActivity.this, "RECEIVE SETTING RECEIVER", Toast.LENGTH_LONG).show();
//      String folder = intent.getStringExtra("folder");
//
//      new FolderChooserDialog.Builder(MainActivity.this)
//					.chooseButton(R.string.md_choose_label)  // changes label of the choose button
//					.initialPath("/sdcard")  // changes initial path, defaults to external storage directory
//					.tag("optional-identifier")
////					.goUpLabel("Up") // custom go up label, default label is "..."
//					.show();
    }
  };

  private void registerReceiver(){
//    if(!isReceiverRegistered) {
//      LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
//              new IntentFilter(SharedPreferencesKeys.REGISTRATION_COMPLETE));
//      isReceiverRegistered = true;
//    }
  }

  @Override
  protected void onPause() {
//    LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
//    isReceiverRegistered = false;
    super.onPause();
    LocalBroadcastManager.getInstance(this).unregisterReceiver(testReceiver);
//    LocalBroadcastManager.getInstance(this).unregisterReceiver(settingReceiver);
  }

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Log.d(TAG, "ONCREATE");
    if (isWrongInstance()) {
      finish();
      return;
    }

    initActivityScope(savedInstanceState);
    startApp(savedInstanceState);

    IntentFilter filter = new IntentFilter("ERROR");
    IntentFilter filterSetting = new IntentFilter("SETTING");

    // Define the callback for what to do when data is received
    LocalBroadcastManager.getInstance(this).registerReceiver(testReceiver, filter);
//    LocalBroadcastManager.getInstance(this).registerReceiver(settingReceiver, filterSetting);

    /*
    final ProgressDialog dialog = new ProgressDialog(this);
    dialog.setMessage("Mohon menunggu sebentar");
    dialog.show();

    mRegistrationBroadcastReceiver = new BroadcastReceiver() {
      @Override
      public void onReceive(Context context, Intent intent) {
        boolean sentToken = sharedPreferencesRepository
                .getObject(SharedPreferencesKeys.SENT_TOKEN_TO_SERVER, boolean.class);
        if (sentToken) {
          Log.d(TAG, "Token already sended");
          dialog.dismiss();
          startApp(savedInstanceState);
        } else {
          dialog.dismiss();
          Log.d(TAG, "Token fail to sended");
          finish();
//          restartApp();
        }
      }
    };

    // Registering BroadcastReceiver
    registerReceiver();

    if (checkPlayServices()) {
      Log.i(TAG, "This device is supported.");
      // Start IntentService to register this application with GCM.
      Intent intent = new Intent(this, RegistrationIntentService.class);
      startService(intent);
    }
    else{
      Log.i(TAG, "This device is supported.");
    }
    */
  }

  public void startApp(Bundle savedInstanceState){

    mConnectionReceiver = new ConnectionMonitor();
    startMonitoringConnection();

    inflateViewContainerLayout();
    configureActionBar();
    configureActivityResult();

  }
  /**
   * Check the device to make sure it has the Google Play Services APK. If
   * it doesn't, display a dialog that allows users to download the APK from
   * the Google Play Store or enable it in the device's system settings.
   */

  private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
  private static final String TAG = "MainActivity";

  private void configureActivityResult() {
    activityResultOwner.takeView(this);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();

    Log.d(TAG, "ONDESTROY");
    // activityScope may be null in case isWrongInstance() returned true in onCreate()
    if (isFinishing() && activityScope != null) {
      mixpanel.flush();
      actionBarOwner.dropView(this);
      activityResultOwner.dropView(this);
      MortarScope parentScope = Mortar.getScope(getApplication());
      parentScope.destroyChild(activityScope);
      activityScope = null;

      stopMonitoringConnection();
    }
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);

    Log.d(TAG, "onSaveInstanceState");
    activityScope.onSaveInstanceState(outState);
  }

  /**
   * Dev tools and the play store (and others?) launch with a different intent, and so
   * lead to a redundant instance of this activity being spawned. <a
   * href="http://stackoverflow.com/questions/17702202/find-out-whether-the-current-activity-will-be-task-root-eventually-after-pendin"
   * >Details</a>.
   */
  private boolean isWrongInstance() {
    if (!isTaskRoot()) {
      Intent intent = getIntent();
      boolean isMainAction = intent.getAction() != null && intent.getAction().equals(ACTION_MAIN);
      return intent.hasCategory(CATEGORY_LAUNCHER) && isMainAction;
    }
    return false;
  }

  /**
   * Configure the app activity bar
   */
  private void configureActionBar() {
    setSupportActionBar(actionBarToolbar);

    menuItemSelectionHandlers = map(
            android.R.id.home, new UpSelectionHandler(),
            R.id.action_environment, new SwitchEnvironmentSelectionHandler()
    );

    actionBarOwner.takeView(this);
  }

  /**
   * Inflate the view container layout and inject our view components
   */
  private void inflateViewContainerLayout() {
    setContentView(R.layout.activity_main);
    ButterKnife.inject(this);
    mainFlow = mainView.getFlow();
  }

  /**
   * Initalise the root activity Mortar scope
   */
  private void initActivityScope(Bundle savedInstanceState) {
    MortarScope parentScope = Mortar.getScope(getApplication());
    activityScope = Mortar.requireActivityScope(parentScope, new MainScreen());
    Mortar.inject(this, this);
    activityScope.onCreate(savedInstanceState);
  }

  private int getActionBarHeight() {
    TypedArray styledAttributes = getTheme().obtainStyledAttributes(new int[]{android.R.attr.actionBarSize});
    int height = (int) styledAttributes.getDimension(0, 0);
    styledAttributes.recycle();
    return height;
  }

  @Override
  public MortarScope getMortarScope() {
    return activityScope;
  }

  @Override
  public void startActivityForResult(int requestCode, Intent intent) {
    if (canHandleIntent(intent)) {
      startActivityForResult(intent, requestCode);
    } else {
      // Timber.e("Could not handle intent %s... ignoring", intent);
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    activityResultOwner.onActivityResultReceived(requestCode, resultCode, data);
  }


  private boolean canHandleIntent(Intent intent) {
    PackageManager manager = getPackageManager();
    List<ResolveInfo> info = manager.queryIntentActivities(intent, 0);
    return info.size() > 0;
  }

  @Override
  public void onFolderSelection(FolderChooserDialog dialog, File folder) {

  }

  private class UpSelectionHandler implements MenuItemSelectionHandler {
    @Override
    public boolean execute() {
      return mainFlow.goUp();
    }
  }

  private class SwitchEnvironmentSelectionHandler implements MenuItemSelectionHandler {
    @Override
    public boolean execute() {
      new BottomSheet.Builder(MainActivity.this)
              .title("Choose Environment")
              .sheet(R.menu.menu_environment)
              .listener(new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                  sharedPreferences.edit().putInt(SharedPreferencesKeys.ENVIRONMENT, which).apply();
                  restartApp();  // required to reinitialise object graph
                }
              })
              .build()
              .show();
      return true;
    }

    private void restartApp() {
      Context context = MainActivity.this;
      Intent mStartActivity = new Intent(context, MainActivity.class);

      int mPendingIntentId = 123456;
      PendingIntent mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);

      AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
      mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);

      System.exit(0);  // die die die!
    }
  }

  public void restartApp() {
    Context context = MainActivity.this;
    Intent mStartActivity = new Intent(context, MainActivity.class);

    int mPendingIntentId = 123456;
    PendingIntent mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);

    AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);

    System.exit(0);  // die die die!
  }
}