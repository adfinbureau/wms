package com.adfinbureau.wms.screen.map;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;

import android.util.Log;
import android.view.View;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.SharedPreferencesKeys;
import com.adfinbureau.wms.actionbar.ActionBarConfig;
import com.adfinbureau.wms.actionbar.ActionBarOwner;
import com.adfinbureau.wms.model.LocationData;
import com.adfinbureau.wms.repository.JsonSharedPreferencesRepository;
import com.adfinbureau.wms.screen.asbuild.AsBuildScreen;
import com.adfinbureau.wms.screen.picture.PictureScreen;
import com.adfinbureau.wms.screen.text.TextScreen;
import com.adfinbureau.wms.screen.video.VideoScreen;
import com.adfinbureau.wms.util.mortar.BaseViewPresenter;
import com.afollestad.materialdialogs.MaterialDialog;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationServices;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.squareup.sqlbrite.BriteDatabase;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.OverlayItem;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;

import flow.Flow;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

class OfflineMapPresenter extends BaseViewPresenter<OfflineMapView> implements OnMapReadyCallback, MapboxMap.OnMarkerClickListener{

	private final Flow flow;
	private final ActionBarOwner actionBarOwner;
	private final BriteDatabase db;
	private final JsonSharedPreferencesRepository sharedPreferencesRepository;

	private String survey;
	private Location location, currentLocation;

	MapboxMap mapboxMap;

	private static final String TAG = "MapPresenter";

	/**
	 * Keeps track of the last selected marker (though it may no longer be selected).  This is
	 * useful for refreshing the info window.
	 */
	private Marker mLastSelectedMarker;
	private LatLng mLastLocation;

	@Inject
	OfflineMapPresenter(Flow flow, ActionBarOwner actionBarOwner, String survey, BriteDatabase db, Location location, JsonSharedPreferencesRepository sharedPreferencesRepository) {
		this.flow = flow;
		this.actionBarOwner = actionBarOwner;
		this.survey = survey;
		this.db = db;
		this.location = location;
		this.sharedPreferencesRepository = sharedPreferencesRepository;
	}

	@Override
	protected void onExitScope() {
		super.onExitScope();

		Log.d(TAG, "Unregister receiver");
//		LocalBroadcastManager.getInstance(getView().getContext()).unregisterReceiver(googleLocationSource.testReceiver);
//		LocalBroadcastManager.getInstance(getView().getContext()).unregisterReceiver(gpsLocationSource.testReceiver);
//		LocalBroadcastManager.getInstance(getView().getContext()).unregisterReceiver(bluetoothLocationSource.testReceiver);
//		if(sharedPreferencesRepository.getObject(SharedPreferencesKeys.LOCATION_SOURCE, String.class).contentEquals("BLUETOOTH")){
//			if (bluetoothLocationSource != null) {
//				bluetoothLocationSource.getLocationBluetooth().terminate();
//			}
//		}
	}

	@Override
	protected void onLoad(Bundle savedInstanceState) {
		super.onLoad(savedInstanceState);

		Log.d(TAG, "ONLOAD MAP PRESENTER");

		OfflineMapView view = getView();


		if (view != null) {
			view.maps.onCreate(savedInstanceState);
			view.maps.getMapAsync(this);
//		view.maps.setMyLocationTrackingMode(MyLocationTracking.TRACKING_FOLLOW);
			currentGpsOption = sharedPreferencesRepository.getObject(SharedPreferencesKeys.LOCATION_SOURCE, String.class);

			//Setup location source
//		googleLocationSource = new GoogleLocationSource();
//		gpsLocationSource = new GPSLocationSource();
//		bluetoothLocationSource = new BluetoothLocationSource();

			FloatingActionButton fab = view.fab;
			fab.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
				}
			});

			hideActionBar();
		}
	}

	private void hideActionBar() {
		ActionBarConfig config = new ActionBarConfig.Builder()
				.title("Map")
				.build();
		actionBarOwner.setConfig(config);
	}

//	BluetoothLocationSource bluetoothLocationSource;
//	GoogleLocationSource googleLocationSource;
//	GPSLocationSource gpsLocationSource;

	//GPS, Mobile, Bluetooth
	private String currentGpsOption = "GPS";

	@Override
	public void onMapReady(final MapboxMap mapboxMap) {
		Log.d(TAG, "onMapReady");

		this.mapboxMap = mapboxMap;


		LocationServices locationServices = LocationServices.getLocationServices(getView().getContext());
		locationServices.addLocationListener(new com.mapbox.mapboxsdk.location.LocationListener() {
			@Override
			public void onLocationChanged(Location location) {

			}
		});
		locationServices.addLocationListener(new com.mapbox.mapboxsdk.location.LocationListener() {
			@Override
			public void onLocationChanged(Location location) {
				if (location != null) {
					// Move the map camera to where the user location is
					mapboxMap.setCameraPosition(new CameraPosition.Builder()
							.target(new LatLng(location))
							.zoom(16)
							.build());
					Log.d(TAG, "LOCATION OFFLINE CHANGE : "+location.getLatitude()+" | "+location.getLongitude()+" | "+location.getAltitude());
				}
			}
		});
		mapboxMap.setMyLocationEnabled(true);


		addMarkerToMap();

		LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
		mapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));

		mapboxMap.setOnMarkerClickListener(this);
		/*
		if(currentGpsOption.contentEquals("Mobile")){
			this.mapboxMap.setLocationSource(googleLocationSource);
		}else if(currentGpsOption.contentEquals("GPS")){
			this.mapboxMap.setLocationSource(gpsLocationSource);
		}else if(currentGpsOption.contentEquals("Bluetooth")){
			this.mapboxMap.setLocationSource(bluetoothLocationSource);
		}
		*/
	}

	private void addMarkerToMap() {
		// Uses a colored icon.

		String x = "SELECT * from "+ LocationData.TABLE+" where survey_id = "+survey+" AND "+LocationData.TYPE+" = 'NODE'";
		Subscription subscription = db.createQuery(LocationData.TABLE,
//					LocationData.QUERY)
				x)
				.mapToList(LocationData.MAPPER)
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(new Action1<List<LocationData>>() {
					@Override
					public void call(List<LocationData> locationListses) {
						Log.d(TAG, "SIZE NODE : "+locationListses.size());

						IconFactory iconFactory = IconFactory.getInstance(getView().getContext());
						Drawable iconPending = ContextCompat.getDrawable(getView().getContext(), R.drawable.loc_red);
						Drawable iconSuccess = ContextCompat.getDrawable(getView().getContext(), R.drawable.loc_black);
						Icon iconP = iconFactory.fromDrawable(iconPending);
						Icon iconS = iconFactory.fromDrawable(iconSuccess);

						for(LocationData loc : locationListses){
							LatLng current = new LatLng(Double.valueOf(loc.latitude()), Double.valueOf(loc.longitude()));
							MarkerOptions m = new MarkerOptions().position(current)
									.icon(iconP);

							String x = "SELECT * from "+ LocationData.TABLE+" where survey_id = "+survey+" AND "+LocationData.LONGITUDE+" = '"+loc.longitude()+"' AND "+LocationData.LATITUDE+" = '"+loc.latitude()+"' AND "+LocationData.STATUS+" != 'COMPLETE'";
							Cursor cursor = db.query(x);
							Log.d(TAG, "CEK BELUM KE UPLOAD : " + cursor.getCount());

							if(cursor.getCount() == 0){
								m.icon(iconS);
							}
							Log.d(TAG, loc.type() + " : " + current);
							m.snippet(loc.altitude());
							mapboxMap.addMarker(m);
						}
					}
				});
	}

	@Override
	public boolean onMarkerClick(Marker marker) {
		mLastSelectedMarker = marker;
		LatLng loc = mLastSelectedMarker.getPosition();
		Snackbar.make(getView(), "Longitude : "+loc.getLongitude()+" | Latitude : "+loc.getLatitude()+" | Altitude : "+mLastSelectedMarker.getSnippet(), Snackbar.LENGTH_LONG).show();

		new MaterialDialog.Builder(getView().getContext())
				.title(R.string.menu)
				.items(R.array.menu)
				.itemsCallbackSingleChoice(2, new MaterialDialog.ListCallbackSingleChoice() {
					@Override
					public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
						LatLng loc = mLastSelectedMarker.getPosition();
						Log.d(TAG, "Selected marker "+loc.toString());
						Location mCurrentLocation = new Location("");
						mCurrentLocation.setLongitude(loc.getLongitude());
						mCurrentLocation.setLatitude(loc.getLatitude());
						mCurrentLocation.setAltitude(Double.valueOf(mLastSelectedMarker.getSnippet()));
						Log.d(TAG, "Selected marker Location longitude : "+mCurrentLocation.getLongitude()+" | latitude : "+mCurrentLocation.getLatitude());

						if (text.toString().contentEquals("Picture")) {
							flow.goTo(new PictureScreen(mCurrentLocation, Long.valueOf(survey)));
						} else if (text.toString().contentEquals("Video")) {
							flow.goTo(new VideoScreen(mCurrentLocation, Long.valueOf(survey)));
						} else if (text.toString().contentEquals("As Build")) {
							flow.goTo(new AsBuildScreen(mCurrentLocation, Long.valueOf(survey)));
						} else if (text.toString().contentEquals("Text")) {
							flow.goTo(new TextScreen(mCurrentLocation, Long.valueOf(survey)));
						}
						return true; // allow selection
					}
				})
				.positiveText(R.string.md_choose_label)
				.show();

		return false;
	}

	/*
	private class GoogleLocationSource implements LocationSource{
		private OnLocationChangedListener mListener;

		@Override
		public void activate(OnLocationChangedListener onLocationChangedListener) {
			this.mListener = onLocationChangedListener;

			Log.d(TAG, "Activate Google Location Source Service, Start Listen to gps");

			//Cek gps is turn on
			IntentFilter filter = new IntentFilter("GoogleLocationService");
			LocalBroadcastManager.getInstance(getView().getContext()).registerReceiver(testReceiver, filter);
		}

		@Override
		public void deactivate() {
			Log.d(TAG, "Deactivate Google Location Source Service, Unlisten location");
			LocalBroadcastManager.getInstance(getView().getContext()).unregisterReceiver(testReceiver);
			mListener = null;
		}

		// Define the callback for what to do when data is received
		private BroadcastReceiver testReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				double longitude = intent.getDoubleExtra("longitude", 0);
				double latitude = intent.getDoubleExtra("latitude", 0);
				double altitude = intent.getDoubleExtra("altitude", 0);

				Location location = new Location("GoogleLocation");
				location.setLongitude(longitude);
				location.setLatitude(latitude);
				location.setAltitude(altitude);

				mListener.onLocationChanged(location);
			}
		};
	}

	private class GPSLocationSource implements LocationSource{
		private OnLocationChangedListener mListener;

		@Override
		public void activate(OnLocationChangedListener onLocationChangedListener) {
			this.mListener = onLocationChangedListener;

			Log.d(TAG, "Activate GPS Location Source Service, Start Listen to gps");

			//Cek gps is turn on
			IntentFilter filter = new IntentFilter("GPSLocationService");
			LocalBroadcastManager.getInstance(getView().getContext()).registerReceiver(testReceiver, filter);
		}

		@Override
		public void deactivate() {
			Log.d(TAG, "Deactivate GPS Location Source Service, Unlisten location");
			LocalBroadcastManager.getInstance(getView().getContext()).unregisterReceiver(testReceiver);
			mListener = null;
		}

		// Define the callback for what to do when data is received
		private BroadcastReceiver testReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				double longitude = intent.getDoubleExtra("longitude", 0);
				double latitude = intent.getDoubleExtra("latitude", 0);
				double altitude = intent.getDoubleExtra("altitude", 0);

				Location location = new Location("GPSLocation");
				location.setLongitude(longitude);
				location.setLatitude(latitude);
				location.setAltitude(altitude);

				mListener.onLocationChanged(location);
			}
		};
	}

	private class BluetoothLocationSource implements LocationSource{
		private OnLocationChangedListener mListener;

		@Override
		public void activate(OnLocationChangedListener onLocationChangedListener) {
			this.mListener = onLocationChangedListener;

			Log.d(TAG, "Activate Bluetooth Location Source Service, Start Listen to gps");

			//Cek gps is turn on
			IntentFilter filter = new IntentFilter("BluetoothLocationService");
			LocalBroadcastManager.getInstance(getView().getContext()).registerReceiver(testReceiver, filter);
		}

		@Override
		public void deactivate() {
			Log.d(TAG, "Deactivate Bluetooth Location Source Service, Unlisten location");
			LocalBroadcastManager.getInstance(getView().getContext()).unregisterReceiver(testReceiver);
			mListener = null;
		}

		// Define the callback for what to do when data is received
		private BroadcastReceiver testReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				double longitude = intent.getDoubleExtra("longitude", 0);
				double latitude = intent.getDoubleExtra("latitude", 0);
				double altitude = intent.getDoubleExtra("altitude", 0);

				Location location = new Location("BluetoothLocation");
				location.setLongitude(longitude);
				location.setLatitude(latitude);
				location.setAltitude(altitude);

				mListener.onLocationChanged(location);
			}
		};
	}
	*/
}
