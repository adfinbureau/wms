package com.adfinbureau.wms.screen.text;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.model.LocationText;
import com.adfinbureau.wms.model.UserWMS;
import com.adfinbureau.wms.util.mortar.BaseView;

import java.util.Set;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;

import butterknife.InjectView;
import butterknife.OnClick;
import mortar.Presenter;

public class AddTextView extends BaseView {

	@Inject
	AddTextPresenter presenter;

	@InjectView(R.id.masalah)
	EditText masalahEditText;
	@InjectView(R.id.usulan)
	EditText usulanEditText;
	@InjectView(R.id.save)
	Button saveButton;

	public AddTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@OnClick(R.id.save)
	public void onSaveClicked(){
		String masalah = masalahEditText.getText().toString();
		String usulan = usulanEditText.getText().toString();

		presenter.save(masalah, usulan);
	}

	@Override
	protected Presenter getPresenter() {
		return presenter;
	}


	public void onValidationError(Set<ConstraintViolation<LocationText>> errors) {
		for (ConstraintViolation<LocationText> error : errors) {
			String property = error.getPropertyPath().toString();

			if (property.equals("masalah")) {
				masalahEditText.setError(error.getMessage());
			} else if (property.equals("usulan")) {
				usulanEditText.setError(error.getMessage());
			}
		}
	}
}
