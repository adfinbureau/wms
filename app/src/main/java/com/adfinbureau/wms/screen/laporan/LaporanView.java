package com.adfinbureau.wms.screen.laporan;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.util.mortar.BaseView;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import javax.inject.Inject;

import butterknife.InjectView;
import mortar.Presenter;

public class LaporanView extends BaseView {

	@Inject
	LaporanPresenter presenter;

	@InjectView(R.id.recycler_view)
	RecyclerView recyclerView;

	@InjectView(R.id.type)
	MaterialBetterSpinner type;

	@InjectView(R.id.status)
	MaterialBetterSpinner status;

	@InjectView(R.id.fab)
	FloatingActionButton fab;

	public LaporanView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected Presenter getPresenter() {
		return presenter;
	}


}
