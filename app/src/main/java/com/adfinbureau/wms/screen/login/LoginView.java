package com.adfinbureau.wms.screen.login;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;

import butterknife.InjectView;
import butterknife.OnClick;
import mortar.Presenter;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.model.UserWMS;
import com.adfinbureau.wms.model.UserWithPassword;
import com.adfinbureau.wms.util.mortar.BaseView;

import java.util.Set;

public class LoginView extends BaseView {

	@Inject LoginPresenter presenter;

	@InjectView(R.id.email_field)
	EditText emailEditText;
	@InjectView(R.id.password_field)
	EditText passwordEditText;
	@InjectView(R.id.login_button)
	Button loginButton;
	@InjectView(R.id.mapview)
	com.google.android.gms.maps.MapView maps;

	public LoginView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected Presenter getPresenter() {
		return presenter;
	}


	@OnClick(R.id.login_button)
	public void onLoginClicked(){
		String email = emailEditText.getText().toString();
		String password = passwordEditText.getText().toString();

		presenter.login(email, password);
	}

	public void onValidationError(Set<ConstraintViolation<UserWMS>> errors) {

//		registerButton.setEnabled(true);

		for (ConstraintViolation<UserWMS> error : errors) {
			String property = error.getPropertyPath().toString();
//			if (property.equals("name")) {
//				nameEditText.setError(error.getMessage());
//			}
			if (property.equals("username")) {
				emailEditText.setError(error.getMessage());
			} else if (property.equals("password")) {
				passwordEditText.setError(error.getMessage());
			}
		}

	}
}
