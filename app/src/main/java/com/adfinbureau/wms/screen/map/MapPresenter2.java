package com.adfinbureau.wms.screen.map;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.SharedPreferencesKeys;
import com.adfinbureau.wms.actionbar.ActionBarConfig;
import com.adfinbureau.wms.actionbar.ActionBarOwner;
import com.adfinbureau.wms.model.LocationData;
import com.adfinbureau.wms.repository.JsonSharedPreferencesRepository;
import com.adfinbureau.wms.screen.asbuild.AsBuildScreen;
import com.adfinbureau.wms.screen.picture.PictureScreen;
import com.adfinbureau.wms.screen.text.TextScreen;
import com.adfinbureau.wms.screen.video.VideoScreen;
import com.adfinbureau.wms.util.bluetooth.BlueTooth;
import com.adfinbureau.wms.util.bluetooth.OnLocationReceived;
import com.adfinbureau.wms.util.mortar.BaseViewPresenter;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.sqlbrite.BriteDatabase;

import java.util.List;

import javax.inject.Inject;

import flow.Flow;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

class MapPresenter2 extends BaseViewPresenter<MapView> implements OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMarkerClickListener{

	private final Flow flow;
	private final ActionBarOwner actionBarOwner;
	private final BriteDatabase db;

	private final JsonSharedPreferencesRepository sharedPreferencesRepository;

	GoogleMap googleMap;

	private static final String TAG = "MapPresenter";
	/**
	 * Keeps track of the last selected marker (though it may no longer be selected).  This is
	 * useful for refreshing the info window.
	 */
	private Marker mLastSelectedMarker;
	private LatLng mLastLocation;
	private String survey;
	private Location location;

	@Inject
	MapPresenter2(Flow flow, ActionBarOwner actionBarOwner, String survey, BriteDatabase db, Location location, JsonSharedPreferencesRepository sharedPreferencesRepository) {
		this.flow = flow;
		this.actionBarOwner = actionBarOwner;
		this.survey = survey;
		this.db = db;
		this.location = location;
		this.sharedPreferencesRepository = sharedPreferencesRepository;
	}

	@Override
	protected void onExitScope() {
		super.onExitScope();

		if(sharedPreferencesRepository.getObject(SharedPreferencesKeys.LOCATION_SOURCE, String.class).contentEquals("BLUETOOTH")){
			if (bluetoothLocationSource != null) {
				bluetoothLocationSource.getLocationBluetooth().terminate();
			}
		}

	}

	@Override
	protected void onLoad(Bundle savedInstanceState) {
		super.onLoad(savedInstanceState);

		Log.d(TAG, "ONLOAD MAP PRESENTER");

		MapView view = getView();
		MapsInitializer.initialize(view.getContext());
		view.maps.onCreate(savedInstanceState);
		view.maps.onResume();
		view.maps.getMapAsync(this);

		FloatingActionButton fab = view.fab;
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (mLastLocation != null) {
					MarkerOptions m = new MarkerOptions().position(mLastLocation).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
					googleMap.addMarker(m);

					Snackbar.make(view, "Location Captured", Snackbar.LENGTH_LONG)
							.setAction("Action", null).show();

					Log.d(TAG, "FAB Clicked : Longitude : "+mLastLocation.longitude+" | Latitude : "+mLastLocation.latitude);
					db.insert(LocationData.TABLE,
							new LocationData.Builder()
									.longitude(mLastLocation.longitude + "")
									.latitude(mLastLocation.latitude + "")
									.survey(survey)
									.type("NODE")
									.status("PENDING")
									.build());
				}
				else{
					Snackbar.make(view, "Location Not Found", Snackbar.LENGTH_LONG)
							.setAction("Action", null).show();
				}

			}
		});

		if (view != null) {
			hideActionBar();
		}
	}

	private void hideActionBar() {
		ActionBarConfig config = new ActionBarConfig.Builder()
				.title("Map")
				.build();
		actionBarOwner.setConfig(config);
	}

	BluetoothLocationSource bluetoothLocationSource;

	@Override
	public void onMapReady(final GoogleMap googleMap) {
		Log.d(TAG, "onMapReady");

//		FollowMeLocationSource followMeLocationSource = new FollowMeLocationSource();
//		followMeLocationSource.getBestAvailableProvider();
		this.googleMap = googleMap;

		if(sharedPreferencesRepository.getObject(SharedPreferencesKeys.LOCATION_SOURCE, String.class).contentEquals("GPS")){
		}
		else if(sharedPreferencesRepository.getObject(SharedPreferencesKeys.LOCATION_SOURCE, String.class).contentEquals("BLUETOOTH")){
			if ((sharedPreferencesRepository.getObject(SharedPreferencesKeys.BLUETOOTH_GPS, String.class)) != null &&
					!sharedPreferencesRepository.getObject(SharedPreferencesKeys.BLUETOOTH_GPS, String.class).contentEquals("")) {
				bluetoothLocationSource = new BluetoothLocationSource();
				this.googleMap.setLocationSource(bluetoothLocationSource);
			}
		}

		enableMyLocation();
		addMarkerToMap();

		ConnectivityManager cm = (ConnectivityManager) getView().getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo x = cm.getActiveNetworkInfo();
		if(x != null && x.isConnectedOrConnecting()){
			Log.d(TAG, "Map type normal");
			this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		}else {
			Log.d(TAG, "Map type none");
			this.googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
		}

		LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
		googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));

		googleMap.setOnMarkerClickListener(this);
		//googleMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
		googleMap.setOnMyLocationButtonClickListener(this);
		GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
			@Override
			public void onMyLocationChange(Location location) {
				Log.d(TAG, "onMyLocationChange "+location.getLongitude()+" | "+location.getLatitude());
				LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
				mLastLocation = loc;

				if(googleMap != null){
					googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
				}
				/*
				mLastSelectedMarker = googleMap.addMarker(new MarkerOptions().position(loc).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
				if(googleMap != null){
					googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
				}
				*/
			}
		};
		googleMap.setOnMyLocationChangeListener(myLocationChangeListener);


		//googleMap.setInfoWindowAdapter(new CustomInfoWindowAdapter());
	}


	private static final LatLng BRISBANE = new LatLng(-27.47093, 153.0235);

	private Subscription subscription;

	private void addMarkerToMap() {
		// Uses a colored icon.

		String x = "SELECT * from "+ LocationData.TABLE+" where survey_id = "+survey;
		subscription = db.createQuery(LocationData.TABLE,
//					LocationData.QUERY)
				x)
				.mapToList(LocationData.MAPPER)
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(new Action1<List<LocationData>>() {
					@Override
					public void call(List<LocationData> locationListses) {
						for(LocationData loc : locationListses){
							LatLng current = new LatLng(Double.valueOf(loc.latitude()), Double.valueOf(loc.longitude()));
							MarkerOptions m = new MarkerOptions().position(current).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));

							Log.d(TAG, loc.type()+" : "+current);
							googleMap.addMarker(m);
						}
					}
				});
		/*
		googleMap.addMarker(new MarkerOptions()
				.position(BRISBANE)
				.title("Brisbane")
				.snippet("Population: 2,074,200")
				.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
		*/


	}

	@Override
	public boolean onMyLocationButtonClick() {
		return false;
	}

	/**
	 * Enables the My Location layer if the fine location permission has been granted.
	 */
	private void enableMyLocation() {
		Log.d(TAG, "Enable my location");

		if (ContextCompat.checkSelfPermission(getView().getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
				!= PackageManager.PERMISSION_GRANTED) {
			// Permission to access the location is missing.
			Log.d(TAG, "Map permission to access the location is missing.");
		} else if (googleMap != null) {
			// Access to the location has been granted to the app.
			Log.d(TAG, "Access to the location has been granted to the app.");
			googleMap.setMyLocationEnabled(true);
		}
	}

	@Override
	public boolean onMarkerClick(Marker marker) {
		mLastSelectedMarker = marker;

		LatLng loc = mLastSelectedMarker.getPosition();
		Snackbar.make(getView(), "Longitude : "+loc.longitude+" | Latitude : "+loc.latitude, Snackbar.LENGTH_LONG).show();


		new MaterialDialog.Builder(getView().getContext())
				.title(R.string.menu)
				.items(R.array.menu)
				.itemsCallbackSingleChoice(2, new MaterialDialog.ListCallbackSingleChoice() {
					@Override
					public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
						LatLng loc = mLastSelectedMarker.getPosition();
						Log.d(TAG, "Selected marker "+loc.toString());
						Location mCurrentLocation = new Location("");
						mCurrentLocation.setLongitude(loc.longitude);
						mCurrentLocation.setLatitude(loc.latitude);

						Log.d(TAG, "Selected marker Location longitude : "+mCurrentLocation.getLongitude()+" | latitude : "+mCurrentLocation.getLatitude());
						if (bluetoothLocationSource != null) {
							bluetoothLocationSource.getLocationBluetooth().terminate();
						}

						if (text.toString().contentEquals("Picture")) {
							flow.goTo(new PictureScreen(mCurrentLocation, Long.valueOf(survey)));
						} else if (text.toString().contentEquals("Video")) {
							flow.goTo(new VideoScreen(mCurrentLocation, Long.valueOf(survey)));
						} else if (text.toString().contentEquals("As Build")) {
							flow.goTo(new AsBuildScreen(mCurrentLocation, Long.valueOf(survey)));
						} else if (text.toString().contentEquals("Text")) {
							flow.goTo(new TextScreen(mCurrentLocation, Long.valueOf(survey)));
						}

						//showToast(which + ": " + text);
						return true; // allow selection
					}
				})
				.positiveText(R.string.md_choose_label)
				.show();
		return false;
	}

	private class BluetoothLocationSource implements LocationSource{
		private OnLocationChangedListener mListener;
		private BlueTooth locationBluetooth;

		public BlueTooth getLocationBluetooth() {
			return locationBluetooth;
		}

		@Override
		public void activate(OnLocationChangedListener onLocationChangedListener) {
			this.mListener = onLocationChangedListener;
			turnOnBluetoothReceiver(sharedPreferencesRepository.getObject(SharedPreferencesKeys.BLUETOOTH_GPS, String.class));
		}

		@Override
		public void deactivate() {
			mListener = null;
		}

		private void turnOnBluetoothReceiver(String address) {
			final Handler handler = new Handler();
			locationBluetooth = new BlueTooth(address, getView().getContext(), new OnLocationReceived() {
				@Override
				public void sendLocation(final Location gps) {
					handler.post(new Runnable() {
						@Override
						public void run() {
							mListener.onLocationChanged(gps);
						}
					});
				}
			}, new BlueTooth.BlueToothEvent() {
				@Override
				public void onBluetoothDisconnect() {

				}
			});
			if(locationBluetooth.isConnected) {
				Log.d(TAG, "Bluetooth started");
				locationBluetooth.start();
			}else{
				Toast.makeText(getView().getContext(), "INFO: Check bluetooth connection", Toast.LENGTH_LONG).show();
			}
		}
	}
	/* Our custom LocationSource.
     * We register this class to receive location updates from the Location Manager
     * and for that reason we need to also implement the LocationListener interface. */
	private class FollowMeLocationSource implements LocationSource, LocationListener {

		private OnLocationChangedListener mListener;
		private LocationManager locationManager;
		private final Criteria criteria = new Criteria();
		private String bestAvailableProvider;
		/* Updates are restricted to one every 10 seconds, and only when
         * movement of more than 10 meters has been detected.*/
		private final int minTime = 10000;     // minimum time interval between location updates, in milliseconds
		private final int minDistance = 10;    // minimum distance between location updates, in meters

		private FollowMeLocationSource() {
			// Get reference to Location Manager
			locationManager = (LocationManager) getView().getContext().getSystemService(Context.LOCATION_SERVICE);

			// Specify Location Provider criteria
			criteria.setAccuracy(Criteria.ACCURACY_FINE);
			criteria.setPowerRequirement(Criteria.POWER_LOW);
			criteria.setAltitudeRequired(true);
			criteria.setBearingRequired(true);
			criteria.setSpeedRequired(true);
			criteria.setCostAllowed(true);
		}

		private void getBestAvailableProvider() {
            /* The preffered way of specifying the location provider (e.g. GPS, NETWORK) to use
             * is to ask the Location Manager for the one that best satisfies our criteria.
             * By passing the 'true' boolean we ask for the best available (enabled) provider. */
			bestAvailableProvider = locationManager.getBestProvider(criteria, true);
		}

		/* Activates this provider. This provider will notify the supplied listener
         * periodically, until you call deactivate().
         * This method is automatically invoked by enabling my-location layer. */
		@Override
		public void activate(OnLocationChangedListener listener) {
			// We need to keep a reference to my-location layer's listener so we can push forward
			// location updates to it when we receive them from Location Manager.
			mListener = listener;

			// Request location updates from Location Manager
			if (bestAvailableProvider != null) {
				locationManager.requestLocationUpdates(bestAvailableProvider, minTime, minDistance, this);
			} else {
				// (Display a message/dialog) No Location Providers currently available.
			}
		}

		/* Deactivates this provider.
         * This method is automatically invoked by disabling my-location layer. */
		@Override
		public void deactivate() {
			// Remove location updates from Location Manager
			locationManager.removeUpdates(this);

			mListener = null;
		}

		@Override
		public void onLocationChanged(Location location) {
            /* Push location updates to the registered listener..
             * (this ensures that my-location layer will set the blue dot at the new/received location) */
			if (mListener != null) {
				mListener.onLocationChanged(location);
			}

            /* ..and Animate camera to center on that location !
             * (the reason for we created this custom Location Source !) */
			googleMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
		}

		@Override
		public void onStatusChanged(String s, int i, Bundle bundle) {

		}

		@Override
		public void onProviderEnabled(String s) {

		}

		@Override
		public void onProviderDisabled(String s) {

		}
	}

	/** Demonstrates customizing the info window and/or its contents. */
	class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

		// These a both viewgroups containing an ImageView with id "badge" and two TextViews with id
		// "title" and "snippet".
		private final View mWindow;

		private final View mContents;

		CustomInfoWindowAdapter() {
			mWindow = LayoutInflater.from(getView().getContext()).inflate(R.layout.custom_info_window, null);
			mContents = LayoutInflater.from(getView().getContext()).inflate(R.layout.custom_info_contents, null);
		}

		@Override
		public View getInfoWindow(Marker marker) {
			render(marker, mWindow);
			return mWindow;
		}

		@Override
		public View getInfoContents(Marker marker) {
			render(marker, mContents);
			return mContents;
		}

		private void render(Marker marker, View view) {

			ImageView iv1 = ((ImageView) view.findViewById(R.id.picture));
			iv1.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Snackbar.make(v, "Picture", Snackbar.LENGTH_SHORT).show();
				}
			});
			ImageView iv2 = ((ImageView) view.findViewById(R.id.video));
			iv2.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Snackbar.make(v, "Video", Snackbar.LENGTH_SHORT).show();
				}
			});
			ImageView iv3 = ((ImageView) view.findViewById(R.id.asbuild));
			iv3.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Snackbar.make(v, "As Build", Snackbar.LENGTH_SHORT).show();
				}
			});
			ImageView iv4 = ((ImageView) view.findViewById(R.id.text));
			iv4.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Snackbar.make(v, "Text", Snackbar.LENGTH_SHORT).show();
				}
			});

			String title = marker.getTitle();
			TextView titleUi = ((TextView) view.findViewById(R.id.title));
			if (title != null) {
				// Spannable string allows us to edit the formatting of the text.
				SpannableString titleText = new SpannableString(title);
				titleText.setSpan(new ForegroundColorSpan(Color.RED), 0, titleText.length(), 0);
				titleUi.setText(titleText);
			} else {
				titleUi.setText("");
			}

			String snippet = marker.getSnippet();
			TextView snippetUi = ((TextView) view.findViewById(R.id.snippet));
			if (snippet != null && snippet.length() > 12) {
				SpannableString snippetText = new SpannableString(snippet);
				snippetText.setSpan(new ForegroundColorSpan(Color.MAGENTA), 0, 10, 0);
				snippetText.setSpan(new ForegroundColorSpan(Color.BLUE), 12, snippet.length(), 0);
				snippetUi.setText(snippetText);
			} else {
				snippetUi.setText("");
			}
		}
	}
}
