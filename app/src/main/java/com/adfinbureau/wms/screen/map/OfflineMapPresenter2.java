package com.adfinbureau.wms.screen.map;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.SharedPreferencesKeys;
import com.adfinbureau.wms.actionbar.ActionBarConfig;
import com.adfinbureau.wms.actionbar.ActionBarOwner;
import com.adfinbureau.wms.model.LocationData;
import com.adfinbureau.wms.repository.JsonSharedPreferencesRepository;
import com.adfinbureau.wms.screen.asbuild.AsBuildScreen;
import com.adfinbureau.wms.screen.picture.PictureScreen;
import com.adfinbureau.wms.screen.text.TextScreen;
import com.adfinbureau.wms.screen.video.VideoScreen;
import com.adfinbureau.wms.util.bluetooth.BlueTooth;
import com.adfinbureau.wms.util.bluetooth.OnLocationReceived;
import com.adfinbureau.wms.util.mortar.BaseViewPresenter;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.squareup.sqlbrite.BriteDatabase;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import flow.Flow;

class OfflineMapPresenter2 extends BaseViewPresenter<OfflineMapView>{

	@Inject
	public Flow flow;
	@Inject
	public ActionBarOwner actionBarOwner;
	@Inject
	public BriteDatabase db;

	@Inject
	public JsonSharedPreferencesRepository sharedPreferencesRepository;

	private static final String TAG = "OfflineMapPresenter";
	/**
	 * Keeps track of the last selected marker (though it may no longer be selected).  This is
	 * useful for refreshing the info window.
	 */
	private Marker mLastSelectedMarker;
	private LatLng mLastLocation;

	private BlueTooth locationThread2;

	@Inject
	public String survey;
	@Inject
	public Location location;

	Location mCurrentLocation;

	private void turnOnBluetoothReceiver(String address) {
		final Handler mHandler = new Handler();
		locationThread2 = new BlueTooth(address, getView().getContext(), new OnLocationReceived() {
			@Override
			public void sendLocation(final Location gps) {
				mCurrentLocation = gps;
				mLastLocation = new LatLng(gps.getLatitude(), gps.getLongitude());

				mHandler.post(new Runnable() {
					@Override
					public void run() {
						if (markerMyLocation.size() > 0) {
							markerMyLocation.removeAllItems();
						}
						GeoPoint current = new GeoPoint(gps);
						myMapController.setCenter(current);
						OverlayItem x = new OverlayItem("a", "a", current);

//						Drawable icon = getView().getContext().getResources().getDrawable(R.drawable.dot2);
//						icon.setBounds(0, 0, icon.getIntrinsicWidth(), icon.getIntrinsicHeight());
//						x.setMarker(icon);

						markerMyLocation.addItem(x);
						getView().invalidate();
					}
				});
//				onLocationChanged(gps);
			}
		}, new BlueTooth.BlueToothEvent() {
			@Override
			public void onBluetoothDisconnect() {

			}
		});
		if(locationThread2.isConnected) {
			Log.d(TAG, "Bluetooth started");
			locationThread2.start();
		}else{
			Toast.makeText(getView().getContext(), "INFO: Check bluetooth connection", Toast.LENGTH_LONG).show();
		}
	}

	MapController myMapController;
	private ItemizedIconOverlay<OverlayItem> marker, markerMyLocation;
	private List<OverlayItem> items;

	@Override
	protected void onLoad(Bundle savedInstanceState) {
		super.onLoad(savedInstanceState);

		Log.d(TAG, "ONLOAD MAP PRESENTER");

		turnOnBluetoothReceiver(sharedPreferencesRepository.getObject(SharedPreferencesKeys.BLUETOOTH_GPS, String.class));

		OfflineMapView view = getView();
//		view.maps.setTileSource(TileSourceFactory.MAPQUESTOSM);
//		view.maps.setBuiltInZoomControls(true);
//		view.maps.setUseDataConnection(true);
//		myMapController = view.maps.getController();
//		myMapController.setZoom(18);

		items = new ArrayList<OverlayItem>();
		marker = new ItemizedIconOverlay<OverlayItem>(getView().getContext(), items, new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
			@Override
			public boolean onItemLongPress(int position, OverlayItem item) {
				// TODO Auto-generated method stub
				return true;
			}
			@Override
			public boolean onItemSingleTapUp(int arg0, OverlayItem item) {
				// TODO Auto-generated method stub
				onMarkerClick(item);
				return true;
			}
		});

		DefaultResourceProxyImpl defaultResourceProxyImpl
				= new DefaultResourceProxyImpl(getView().getContext());
		markerMyLocation = new ItemizedIconOverlay<OverlayItem>(new ArrayList<OverlayItem>(), null, defaultResourceProxyImpl);
//		view.maps.getOverlays().add(marker);
//		view.maps.getOverlays().add(markerMyLocation);

		/*
		FloatingActionButton fab = view.fab;
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Snackbar.make(view, "Location Captured", Snackbar.LENGTH_LONG)
						.setAction("Action", null).show();

				if(mLastLocation != null) {
					GeoPoint myPoint1 = new GeoPoint(mLastLocation.latitude, mLastLocation.longitude);
					OverlayItem x = new OverlayItem("A", "A", myPoint1);
					//x.setMarker(getView().getContext().getResources().getDrawable(R.drawable.dot2));
					marker.addItem(x);
				}

				db.insert(LocationData.TABLE,
						new LocationData.Builder()
								.longitude(mLastLocation.longitude + "")
								.latitude(mLastLocation.latitude + "")
								.survey(survey)
								.type("NODE")
								.status("PENDING")
								.build());
			}
		});
*/
		if (view != null) {
			hideActionBar();
		}
	}

	public boolean onMarkerClick(final OverlayItem marker) {
//		mLastSelectedMarker = marker;
		new MaterialDialog.Builder(getView().getContext())
				.title(R.string.menu)
				.items(R.array.menu)
				.itemsCallbackSingleChoice(2, new MaterialDialog.ListCallbackSingleChoice() {
					@Override
					public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
//						LatLng loc = mLastSelectedMarker.getPosition();
						GeoPoint point = marker.getPoint();
						Location mCurrentLocation = new Location("");
						mCurrentLocation.setLongitude(point.getLongitudeE6());
						mCurrentLocation.setLatitude(point.getLatitudeE6());

						if (text.toString().contentEquals("Picture")) {
							flow.goTo(new PictureScreen(mCurrentLocation, Long.valueOf(survey)));
						}
						else if (text.toString().contentEquals("Video")) {
							flow.goTo(new VideoScreen(mCurrentLocation, Long.valueOf(survey)));
						}
						else if (text.toString().contentEquals("As Build")) {
							flow.goTo(new AsBuildScreen(mCurrentLocation, Long.valueOf(survey)));
						}
						else if (text.toString().contentEquals("Text")) {
							flow.goTo(new TextScreen(mCurrentLocation, Long.valueOf(survey)));
						}

						if(locationThread2!=null)
							locationThread2.terminate();
						//showToast(which + ": " + text);
						return true; // allow selection
					}
				})
				.positiveText(R.string.md_choose_label)
				.show();
		return false;
	}

	private void hideActionBar() {
		ActionBarConfig config = new ActionBarConfig.Builder()
				.title("Offline Map")
				.build();
		actionBarOwner.setConfig(config);
	}

	/*
	@Override
	public boolean onMarkerClick(Marker marker) {
		mLastSelectedMarker = marker;
		new MaterialDialog.Builder(getView().getContext())
				.title(R.string.menu)
				.items(R.array.menu)
				.itemsCallbackSingleChoice(2, new MaterialDialog.ListCallbackSingleChoice() {
					@Override
					public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
						LatLng loc = mLastSelectedMarker.getPosition();
						Location mCurrentLocation = new Location("");
						mCurrentLocation.setLongitude(loc.longitude);
						mCurrentLocation.setLatitude(loc.latitude);

						if (text.toString().contentEquals("Picture")) {
							flow.goTo(new PictureScreen(mCurrentLocation, Long.valueOf(survey)));
						}
						else if (text.toString().contentEquals("Video")) {
							flow.goTo(new VideoScreen(mCurrentLocation, Long.valueOf(survey)));
						}
						else if (text.toString().contentEquals("As Build")) {
							flow.goTo(new AsBuildScreen(mCurrentLocation, Long.valueOf(survey)));
						}
						else if (text.toString().contentEquals("Text")) {
							flow.goTo(new SettingScreen(mCurrentLocation, Long.valueOf(survey)));
						}

						//showToast(which + ": " + text);
						return true; // allow selection
					}
				})
				.positiveText(R.string.md_choose_label)
				.show();
		return false;
	}
*/
}
