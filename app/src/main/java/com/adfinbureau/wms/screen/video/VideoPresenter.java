package com.adfinbureau.wms.screen.video;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.View;

import com.adfinbureau.wms.SharedPreferencesKeys;
import com.adfinbureau.wms.actionbar.ActionBarConfig;
import com.adfinbureau.wms.actionbar.ActionBarOwner;
import com.adfinbureau.wms.activity.ActivityResultOwner;
import com.adfinbureau.wms.activity.ActivityResultRegistrar;
import com.adfinbureau.wms.model.LocationData;
import com.adfinbureau.wms.repository.JsonSharedPreferencesRepository;
import com.adfinbureau.wms.service.LocationService;
import com.adfinbureau.wms.service.UserService;
import com.adfinbureau.wms.util.adapter.VideoListAdapter;
import com.adfinbureau.wms.util.camera.AlbumStorageDirFactory;
import com.adfinbureau.wms.util.camera.BaseAlbumDirFactory;
import com.adfinbureau.wms.util.camera.FroyoAlbumDirFactory;
import com.adfinbureau.wms.util.helper.FileSplitter;
import com.adfinbureau.wms.util.mortar.BaseViewPresenter;
import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.sqlbrite.BriteDatabase;

import org.acra.ACRA;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Validator;

import flow.Flow;
import mortar.MortarScope;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

class VideoPresenter extends BaseViewPresenter<VideoView> implements VideoListAdapter.OnItemClickListener , ActivityResultOwner.ActivityResultListener{

	private static final int ACTION_TAKE_VIDEO = 3;

	private ActionBarOwner actionBarOwner;
	private final ActivityResultRegistrar activityResultRegistrar;

	private final Validator validator;
	private final UserService userService;
	private final LocationService locationService;
	private final Flow flow;
	private static List<LocationData> items = new ArrayList<>();
	private final BriteDatabase db;
	private Location location;
	private String survey;
	private final JsonSharedPreferencesRepository sharedPreferences;

	private static final String TAG = "VideoPresenter";


	@Inject
	VideoPresenter(ActionBarOwner actionBarOwner, Validator validator, UserService userService, LocationService locationService, Flow flow, ActivityResultRegistrar activityResultRegistrar, BriteDatabase db, Location location, String survey, JsonSharedPreferencesRepository sharedPreferences) {
		this.actionBarOwner = actionBarOwner;
		this.validator = validator;
		this.userService = userService;
		this.locationService = locationService;
		this.flow = flow;
		this.activityResultRegistrar = activityResultRegistrar;
		this.db = db;
		this.location = location;
		this.survey = survey;
		this.sharedPreferences = sharedPreferences;
	}

	private AlbumStorageDirFactory mAlbumStorageDirFactory = null;

	@Override
	protected void onLoad(Bundle savedInstanceState) {
		super.onLoad(savedInstanceState);

		final VideoView view = getView();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
			mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
		} else {
			mAlbumStorageDirFactory = new BaseAlbumDirFactory();
		}
		if (view != null) {
			configureActionBar();
			view.recyclerView.setLayoutManager(new GridLayoutManager(view.getContext(), 2));
			setRecyclerAdapter(view.recyclerView);
			view.recyclerView.scheduleLayoutAnimation();

			view.fab.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dispatchTakeVideoIntent();
				}
			});
			String x = "SELECT * from "+LocationData.TABLE+" where survey_id = '"+survey+"' and video IS NOT NULL "+
			"AND longitude = '"+location.getLongitude()+"' "+
			"AND latitude = '"+location.getLatitude()+"'";
			subscription = db.createQuery(LocationData.TABLE, x)
					.mapToList(LocationData.MAPPER)
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe(adapter);
		}
	}

	private Uri mVideoUri;

	private static final String VIDEO_FILE_PREFIX = "VIDEO_";
	private static final String VIDEO_FILE_SUFFIX = ".mp4";


	/* Photo album for this application */
	private String getAlbumName() {
		return "WMS-Video";
	}

	private File getAlbumDir() {
		File storageDir = null;

		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
			String videoLocation = sharedPreferences.getObject(SharedPreferencesKeys.SETTING_VIDEO_LOCATION, String.class);
			if(videoLocation != null){
				storageDir = new File(videoLocation);
			}else{
				storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(getAlbumName());

				if (storageDir != null) {
					if (! storageDir.mkdirs()) {
						if (! storageDir.exists()){
							Log.d(TAG, "failed to create directory");
							return null;
						}
					}
				}
			}
		} else {
			Log.v("WMS", "External storage is not mounted READ/WRITE.");
		}

		return storageDir;
	}


	private void dispatchTakeVideoIntent() {
		Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
		activityResultRegistrar.startActivityForResult(ACTION_TAKE_VIDEO, takeVideoIntent);
	}

	private VideoListAdapter adapter;
	private Subscription subscription;

	private void setRecyclerAdapter(RecyclerView recyclerView) {
		adapter = new VideoListAdapter(getView().getContext(), items);
		adapter.setOnItemClickListener(this);
		recyclerView.setAdapter(adapter);
	}

	private void configureActionBar() {
		ActionBarConfig config = new ActionBarConfig.Builder()
				.title("Video")
				.build();
		actionBarOwner.setConfig(config);
	}

	@Override
	public void onItemClick(View view, final LocationData viewModel) {
		new MaterialDialog.Builder(getView().getContext())
				.title("Video")
				.content("Description")
				.inputType(InputType.TYPE_CLASS_TEXT |
						InputType.TYPE_TEXT_VARIATION_PERSON_NAME |
						InputType.TYPE_TEXT_FLAG_CAP_WORDS)
						//.inputRange(2, 16)
				.positiveText("Update")
				.input("Edit description", viewModel.description(), false, new MaterialDialog.InputCallback() {
					@Override
					public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
						//showToast("Hello, " + input.toString() + "!");
						//getView().showToast(input.toString(), Toast.LENGTH_LONG);
						if (!viewModel.status().contentEquals("COMPLETE")) {
							db.update(LocationData.TABLE,
									new LocationData.Builder()
											.description(input.toString())
											.build(), LocationData.ID + " = ?", new String[]{String.valueOf(viewModel.id())});
						} else if (viewModel.status().contentEquals("COMPLETE")) {
							db.update(LocationData.TABLE,
									new LocationData.Builder()
											.status("UPDATE")
											.description(input.toString())
											.build(), LocationData.ID + " = ?", new String[]{String.valueOf(viewModel.id())});
						}
						Snackbar.make(getView(), "Description successful updated "+input.toString()+" "+LocationData.ID+" : "+viewModel.id(), Snackbar.LENGTH_LONG).show();
					}
				}).show();
	}

	@Override
	protected void onEnterScope(MortarScope scope) {
		super.onEnterScope(scope);
		activityResultRegistrar.register(scope, this);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == 0){
			Snackbar.make(getView(), "Cancel capture image", Snackbar.LENGTH_LONG).show();
		}else{
			mVideoUri = data.getData();
			String loc = getRealPathFromURI(getView().getContext(), mVideoUri );
			File f = new File(loc);

			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
			String videoFileName = VIDEO_FILE_PREFIX + timeStamp + "_";
			File albumF = getAlbumDir();

			String curFileName = albumF+"/"+videoFileName+"."+VIDEO_FILE_SUFFIX;
			File videoF = new File(curFileName);
//			f.renameTo(videoF);

//			moveFile(f, videoF);
			try {
				FileSplitter.copyFile(f, videoF);
			} catch (IOException e) {
				e.printStackTrace();
			}

			Snackbar.make(getView(), "Video captured "+curFileName, Snackbar.LENGTH_LONG).show();

			String x = "SELECT * from "+ LocationData.TABLE+" where survey_id = "+survey+" AND "+LocationData.TYPE+" = 'NODE' AND "+LocationData.LONGITUDE+" = '"+location.getLongitude()+"' AND "+LocationData.LATITUDE+" = '"+location.getLatitude()+"'";
			Cursor cursor = db.query(x);
			Log.d(TAG, "CEK NODE  : " + cursor.getCount());
			if(cursor.getCount() == 0) {
				db.insert(LocationData.TABLE,
						new LocationData.Builder()
								.longitude(location.getLongitude() + "")
								.latitude(location.getLatitude() + "")
								.altitude(location.getAltitude() + "")
								.survey(survey)
								.type("NODE")
								.status("PENDING")
								.build());
			}

			db.insert(LocationData.TABLE,
					new LocationData.Builder()
							.longitude(location.getLongitude() + "")
							.latitude(location.getLatitude() + "")
							.altitude(location.getAltitude() + "")
							.survey(survey)
							.type("VIDEO")
							.video(curFileName)
							.status("PENDING")
							.build());
		}
	}

	public void moveFile(File afile, File bfile){
		InputStream inStream = null;
		OutputStream outStream = null;

		try{
			inStream = new FileInputStream(afile);
			outStream = new FileOutputStream(bfile);

			byte[] buffer = new byte[1024];

			int length;
			//copy the file content in bytes
			while ((length = inStream.read(buffer)) > 0){

				outStream.write(buffer, 0, length);

			}

			inStream.close();
			outStream.close();

			//delete the original file
			afile.delete();

			System.out.println("File is copied successful!");

		}catch(IOException e){
			e.printStackTrace();
		}
	}


	public String getRealPathFromURI(Context context, Uri contentUri) {
		Cursor cursor = null;
		try {
			String[] proj = { MediaStore.Images.Media.DATA };
			cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
			int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}


}
