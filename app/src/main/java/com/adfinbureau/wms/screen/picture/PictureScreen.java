package com.adfinbureau.wms.screen.picture;

import android.location.Location;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.activity.ActivityResultModule;
import com.adfinbureau.wms.screen.main.MainScreen;
import com.adfinbureau.wms.screen.map.MapScreen;

import dagger.Provides;
import flow.HasParent;
import flow.Layout;
import mortar.Blueprint;

/**
 * This screen might be where you send user's once they're logged in/registered
 * as a starting point for your app.
 *
 * The implementation is incomplete and left to your imagination.
 */

@Layout(R.layout.view_picture)
public class PictureScreen implements Blueprint , HasParent<MapScreen> {

	private Location location;
	private long survey_id;
	public PictureScreen(Location location, long survey_id){
		this.location = location;
		this.survey_id = survey_id;
	}

	@Override
	public String getMortarScopeName() {
		return getClass().getName();
	}

	@Override
	public Object getDaggerModule() {
		return new Module();
	}

	@Override
	public MapScreen getParent() {
		return new MapScreen(location, survey_id);
	}

	@dagger.Module(
			includes = { ActivityResultModule.class },
			injects = PictureView.class,
			addsTo = MainScreen.Module.class
	)
 	class Module {
		@Provides
		Location provideLocation() {
			return location;
		}
		@Provides
		String survey() { return String.valueOf(survey_id); }
	}
}
