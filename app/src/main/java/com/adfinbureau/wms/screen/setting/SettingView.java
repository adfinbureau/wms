package com.adfinbureau.wms.screen.setting;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.Spinner;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.util.mortar.BaseView;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import javax.inject.Inject;

import butterknife.InjectView;
import mortar.Presenter;

public class SettingView extends BaseView {

	@Inject
	SettingPresenter presenter;

	@InjectView(R.id.quality)
	Spinner quality;

	@InjectView(R.id.bluetooth)
	Button bluetooth;

	@InjectView(R.id.image)
	Button image;

	@InjectView(R.id.video)
	Button video;

	@InjectView(R.id.asbuild)
	Button asbuild;

	public SettingView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected Presenter getPresenter() {
		return presenter;
	}


}
