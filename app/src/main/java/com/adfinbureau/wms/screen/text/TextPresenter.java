package com.adfinbureau.wms.screen.text;


import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.View;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.actionbar.ActionBarConfig;
import com.adfinbureau.wms.actionbar.ActionBarOwner;
import com.adfinbureau.wms.model.LocationData;
import com.adfinbureau.wms.service.LocationService;
import com.adfinbureau.wms.service.UserService;
import com.adfinbureau.wms.util.adapter.PictureListAdapter;
import com.adfinbureau.wms.util.adapter.RecyclerViewAdapter;
import com.adfinbureau.wms.util.adapter.TextListAdapter;
import com.adfinbureau.wms.util.adapter.ViewModel;
import com.adfinbureau.wms.util.mortar.BaseViewPresenter;
import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.sqlbrite.BriteDatabase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Validator;

import flow.Flow;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

class TextPresenter extends BaseViewPresenter<TextView> implements TextListAdapter.OnItemClickListener{

	private ActionBarOwner actionBarOwner;

	private final Validator validator;
	private final UserService userService;
	private final LocationService locationService;
	private final Flow flow;
	private final String survey;
	private final Location location;
	private final BriteDatabase db;

	private Subscription subscription;
	private TextListAdapter adapter;

	private static List<LocationData> items = new ArrayList<>();

	static {
//		for (int i = 1; i <= 10; i++) {
//			items.add(new ViewModel("Item " + i, R.drawable.arrow));
//		}
	}

	@Inject
	TextPresenter(ActionBarOwner actionBarOwner, Validator validator, UserService userService, LocationService locationService, Flow flow, String survey, Location location, BriteDatabase db) {
		this.actionBarOwner = actionBarOwner;
		this.validator = validator;
		this.userService = userService;
		this.locationService = locationService;
		this.flow = flow;
		this.survey = survey;
		this.location = location;
		this.db = db;
	}

	@Override
	protected void onLoad(Bundle savedInstanceState) {
		super.onLoad(savedInstanceState);

		TextView view = getView();
		if (view != null) {
			configureActionBar();
//			view.recyclerView.setLayoutManager(new GridLayoutManager(view.getContext(), 2));
			RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getView().getContext());
			view.recyclerView.setLayoutManager(mLayoutManager);

			setRecyclerAdapter(view.recyclerView);
			view.recyclerView.scheduleLayoutAnimation();

			view.fab.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					flow.goTo(new AddTextScreen(location, Long.valueOf(survey)));
//					new MaterialDialog.Builder(getView().getContext())
//							.title("Picture")
//							.content("Description")
//							.inputType(InputType.TYPE_CLASS_TEXT |
//									InputType.TYPE_TEXT_VARIATION_PERSON_NAME |
//									InputType.TYPE_TEXT_FLAG_CAP_WORDS)
//									//.inputRange(2, 16)
//							.positiveText("Update")
//							.input("Edit description", "", false, new MaterialDialog.InputCallback() {
//								@Override
//								public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
//									//showToast("Hello, " + input.toString() + "!");
//									//getView().showToast(input.toString(), Toast.LENGTH_LONG);
//									Snackbar.make(getView(), "Description successful updated " + input.toString(), Snackbar.LENGTH_LONG).show();
//								}
//							})
//							.show();
				}
			});

			String x = "SELECT * from "+LocationData.TABLE+" where survey_id = '"+survey+"' and masalah IS NOT NULL " +
					"AND longitude = '"+location.getLongitude()+"' "+
					"AND latitude = '"+location.getLatitude()+"'";
			subscription = db.createQuery(LocationData.TABLE,
//					LocationLists.QUERY)
					x)
					.mapToList(LocationData.MAPPER)
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe(adapter);

		}
	}

	private void setRecyclerAdapter(RecyclerView recyclerView) {
		adapter = new TextListAdapter(getView().getContext(), items);
		adapter.setOnItemClickListener(this);
		recyclerView.setAdapter(adapter);
	}

	private void configureActionBar() {
		ActionBarConfig config = new ActionBarConfig.Builder()
				.title("Text")
				.build();
		actionBarOwner.setConfig(config);
	}

	@Override
	public void onItemClick(View view, LocationData locationData) {
		android.location.Location location = new Location("");
		location.setLatitude(Double.valueOf(locationData.latitude()));
		location.setLongitude(Double.valueOf(locationData.longitude()));
		flow.goTo(new AddTextScreen(location, Long.valueOf(locationData.survey()), locationData));
	}
}
