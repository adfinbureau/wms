package com.adfinbureau.wms.screen.home;

import com.adfinbureau.wms.Application;
import com.adfinbureau.wms.R;
import com.adfinbureau.wms.actionbar.ActionBarModule;
import com.adfinbureau.wms.activity.ActivityResultModule;
import com.adfinbureau.wms.screen.login.LoginView;
import com.adfinbureau.wms.screen.main.MainScreen;

import flow.Layout;
import mortar.Blueprint;

/**
 * This screen might be where you send user's once they're logged in/registered
 * as a starting point for your app.
 *
 * The implementation is incomplete and left to your imagination.
 */

@Layout(R.layout.view_home)
public class HomeScreen implements Blueprint {
	@Override
	public String getMortarScopeName() {
		return getClass().getName();
	}

	@Override
	public Object getDaggerModule() {
		return new Module();
	}

	@dagger.Module(
			includes = { ActivityResultModule.class },
			injects = HomeView.class,
			addsTo = MainScreen.Module.class,
			library = true
	)
 	class Module {

	}
}
