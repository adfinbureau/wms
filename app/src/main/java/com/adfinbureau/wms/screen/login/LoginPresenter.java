package com.adfinbureau.wms.screen.login;


import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.adfinbureau.wms.SharedPreferencesKeys;
import com.adfinbureau.wms.actionbar.ActionBarConfig;
import com.adfinbureau.wms.actionbar.ActionBarOwner;
import com.adfinbureau.wms.event.RegisterGCMEvent;
import com.adfinbureau.wms.model.User;
import com.adfinbureau.wms.model.UserWMS;
import com.adfinbureau.wms.model.UserWithPassword;
import com.adfinbureau.wms.repository.JsonSharedPreferencesRepository;
import com.adfinbureau.wms.screen.home.HomeScreen;
import com.adfinbureau.wms.screen.map.MapView;
import com.adfinbureau.wms.service.LocationService;
//import com.adfinbureau.wms.service.RegistrationIntentService;
import com.adfinbureau.wms.service.UserService;
import com.adfinbureau.wms.util.mortar.BaseViewPresenter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.maps.MapsInitializer;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.squareup.otto.Subscribe;

import java.util.Set;

import flow.Flow;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class LoginPresenter extends BaseViewPresenter<LoginView> {

	private ActionBarOwner actionBarOwner;

	private final Validator validator;
	private final UserService userService;
	private final LocationService locationService;
	private final Flow flow;
	private final JsonSharedPreferencesRepository sharedPreferences;

	@Inject
	LoginPresenter(ActionBarOwner actionBarOwner, Validator validator, UserService userService, LocationService locationService, Flow flow, JsonSharedPreferencesRepository sharedPreferences) {
		this.actionBarOwner = actionBarOwner;
		this.validator = validator;
		this.userService = userService;
		this.locationService = locationService;
		this.flow = flow;
		this.sharedPreferences = sharedPreferences;
	}

	protected boolean userAccountExists() {
		return sharedPreferences.getObject(SharedPreferencesKeys.USER_ACCOUNT, User.class) != null;
	}

	@Override
	protected void onLoad(Bundle savedInstanceState) {
		super.onLoad(savedInstanceState);

		if(userAccountExists()){
			flow.goTo(new HomeScreen());
		}else {
			LoginView view = getView();

			if (view != null) {
				MapsInitializer.initialize(view.getContext());
				view.maps.onCreate(savedInstanceState);
				view.maps.onResume();
				configureActionBar();
			}
		}
	}

	private void configureActionBar() {
		ActionBarConfig config = new ActionBarConfig.Builder()
				.title("Login")
				.build();
		actionBarOwner.setConfig(config);
	}

	ProgressDialog d;

	public void login(String email, String password){
		final UserWMS u = new UserWMS(email, password, "");

		Set<ConstraintViolation<UserWMS>> errors = validator.validate(u);

		if(errors.size() > 0) {
			LoginView view = getView();
			view.onValidationError(errors);
		}else {
			d = getView().showProgressDialog(true, false, "Please Wait");

			userService.getUser(email, password).enqueue(new Callback<Void>() {
				@Override
				public void onResponse(Call<Void> call, Response<Void> response) {
					if (response.isSuccessful()) {
						getView().showToast("Login success", Toast.LENGTH_LONG);
						sharedPreferences.putObject(SharedPreferencesKeys.USER_ACCOUNT, u);
						flow.goTo(new HomeScreen());
					} else {
						getView().showToast("Username or password not match", Toast.LENGTH_LONG);
					}
					d.dismiss();
				}

				@Override
				public void onFailure(Call<Void> call, Throwable t) {
					getView().showToast("Login failure " + t.getMessage(), Toast.LENGTH_LONG);
					d.dismiss();
				}
			});
		}
	}


	@Subscribe
	public void onRegisterGCMComplete(final RegisterGCMEvent event) {
		Log.d(TAG, "OMREGISTERGCM COMPLETE");
		userService.getUser(event.getUserWMS().getUsername(), event.getUserWMS().getPassword()).enqueue(new Callback<Void>() {
			@Override
			public void onResponse(Call<Void> call, Response<Void> response) {
				if (response.isSuccessful()) {
					getView().showToast("Login success", Toast.LENGTH_LONG);
					sharedPreferences.putObject(SharedPreferencesKeys.USER_ACCOUNT, event.getUserWMS());
					flow.goTo(new HomeScreen());
				} else {
					getView().showToast("Username or password not match", Toast.LENGTH_LONG);

				}
				d.dismiss();
			}

			@Override
			public void onFailure(Call<Void> call, Throwable t) {
				getView().showToast("Login failure " + t.getMessage(), Toast.LENGTH_LONG);
				d.dismiss();
			}
		});
	}

	private static final String TAG = "LoginPresenter";

}
