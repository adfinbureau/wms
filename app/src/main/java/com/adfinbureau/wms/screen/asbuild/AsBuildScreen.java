package com.adfinbureau.wms.screen.asbuild;

import android.location.Location;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.activity.ActivityResultModule;
import com.adfinbureau.wms.screen.main.MainScreen;
import com.adfinbureau.wms.screen.map.MapScreen;

import dagger.Provides;
import flow.HasParent;
import flow.Layout;
import mortar.Blueprint;

/**
 * This screen might be where you send user's once they're logged in/registered
 * as a starting point for your app.
 *
 * The implementation is incomplete and left to your imagination.
 */

@Layout(R.layout.view_as_build)
public class AsBuildScreen implements Blueprint, HasParent<MapScreen> {

	private Location location;
	private long survey_id;

	public AsBuildScreen(Location location, long survey_id){
		this.survey_id = survey_id;
		this.location = location;
	}

	@Override
	public String getMortarScopeName() {
		return getClass().getName();
	}

	@Override
	public Object getDaggerModule() {
		return new Module();
	}

	@Override
	public MapScreen getParent() {
		return new MapScreen(location, survey_id);
	}

	@dagger.Module(
			includes = { ActivityResultModule.class },
			injects = AsBuildView.class,
			addsTo = MainScreen.Module.class,
			library = true
	)
 	class Module {
		@Provides
		Location provideLocation() {
			return location;
		}
		@Provides
		String survey() { return String.valueOf(survey_id); }
	}
}
