package com.adfinbureau.wms.screen.todo;


import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.actionbar.ActionBarConfig;
import com.adfinbureau.wms.actionbar.ActionBarOwner;
import com.adfinbureau.wms.activity.ActivityResultOwner;
import com.adfinbureau.wms.activity.ActivityResultRegistrar;
import com.adfinbureau.wms.service.LocationService;
import com.adfinbureau.wms.service.UserService;
import com.adfinbureau.wms.util.adapter.RecyclerViewAdapter;
import com.adfinbureau.wms.util.adapter.ViewModel;
import com.adfinbureau.wms.util.camera.AlbumStorageDirFactory;
import com.adfinbureau.wms.util.camera.BaseAlbumDirFactory;
import com.adfinbureau.wms.util.camera.FroyoAlbumDirFactory;
import com.adfinbureau.wms.util.mortar.BaseViewPresenter;
import com.squareup.sqlbrite.BriteDatabase;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Validator;

import flow.Flow;
import mortar.MortarScope;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

class TodoPresenter extends BaseViewPresenter<TodoView> implements RecyclerViewAdapter.OnItemClickListener {

	private ActionBarOwner actionBarOwner;

	private final Validator validator;
	private final UserService userService;
	private final LocationService locationService;
	private final Flow flow;
	private static List<ViewModel> items = new ArrayList<>();

	private BriteDatabase db;

	private ListsAdapter adapter;
	private Subscription subscription;

	@Inject
	TodoPresenter(ActionBarOwner actionBarOwner, Validator validator, UserService userService, LocationService locationService, Flow flow, BriteDatabase db) {
		this.actionBarOwner = actionBarOwner;
		this.validator = validator;
		this.userService = userService;
		this.locationService = locationService;
		this.flow = flow;
		this.db = db;
	}

	@Override
	protected void onLoad(Bundle savedInstanceState) {
		super.onLoad(savedInstanceState);

		final TodoView view = getView();
		if (view != null) {
			configureActionBar();

			//listener = (Listener) view;
			adapter = new ListsAdapter(view.getContext());

			view.getListView().setEmptyView(view.getEmptyView());
			view.getListView().setAdapter(adapter);

			subscription = db.createQuery(ListsItem.TABLES, ListsItem.QUERY)
					.mapToList(ListsItem.MAPPER)
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe(adapter);
		}
	}

	private void configureActionBar() {
		ActionBarConfig config = new ActionBarConfig.Builder()
				.title("To Do")
				.build();
		actionBarOwner.setConfig(config);
	}

	@Override
	public void onItemClick(View view, ViewModel viewModel) {
		if(viewModel.getText().contentEquals("Item 1")){
		}
	}

	@Override
	protected void onEnterScope(MortarScope scope) {
		super.onEnterScope(scope);
	}
}
