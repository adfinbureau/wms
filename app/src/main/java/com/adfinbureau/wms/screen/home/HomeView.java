package com.adfinbureau.wms.screen.home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.model.UserWithPassword;
import com.adfinbureau.wms.util.mortar.BaseView;

import java.util.Set;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;

import butterknife.InjectView;
import butterknife.OnClick;
import mortar.Presenter;

public class HomeView extends BaseView {

	@Inject
	HomePresenter presenter;

	@InjectView(R.id.recycler)
	RecyclerView recyclerView;

	@InjectView(R.id.switch1)
	Switch aSwitch;

	@InjectView(R.id.locationSource)
	Spinner spinnerLocationSource;

	@InjectView(R.id.txtLong)
	TextView lng;
	@InjectView(R.id.txtLat)
	TextView lat;
	@InjectView(R.id.txtAlt)
	TextView alt;

	public HomeView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected Presenter getPresenter() {
		return presenter;
	}


}
