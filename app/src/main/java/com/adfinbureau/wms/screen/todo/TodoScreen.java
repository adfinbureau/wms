package com.adfinbureau.wms.screen.todo;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.activity.ActivityResultModule;
import com.adfinbureau.wms.screen.home.HomeScreen;
import com.adfinbureau.wms.screen.main.MainScreen;

import flow.HasParent;
import flow.Layout;
import mortar.Blueprint;

/**
 * This screen might be where you send user's once they're logged in/registered
 * as a starting point for your app.
 *
 * The implementation is incomplete and left to your imagination.
 */

@Layout(R.layout.view_todo)
public class TodoScreen implements Blueprint, HasParent<HomeScreen> {
	@Override
	public String getMortarScopeName() {
		return getClass().getName();
	}

	@Override
	public Object getDaggerModule() {
		return new Module();
	}

	@Override
	public HomeScreen getParent() {
		return new HomeScreen();
	}

	@dagger.Module(
			includes = { ActivityResultModule.class },
			injects = TodoView.class,
			addsTo = MainScreen.Module.class,
			library = true
	)
 	class Module {

	}
}
