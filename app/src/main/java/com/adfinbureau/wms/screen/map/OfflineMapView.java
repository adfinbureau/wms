package com.adfinbureau.wms.screen.map;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.analytics.EventTracker;
import com.adfinbureau.wms.util.mortar.BaseView;

import javax.inject.Inject;

import butterknife.InjectView;
import mortar.Presenter;

public class OfflineMapView extends BaseView {

	@Inject
	OfflineMapPresenter presenter;

	@Inject
	EventTracker eventTracker;

	@InjectView(R.id.openmapview)
	com.mapquest.mapping.maps.MapView maps;

	@InjectView(R.id.fab)
	FloatingActionButton fab;

	public OfflineMapView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected Presenter getPresenter() {
		return presenter;
	}
}
