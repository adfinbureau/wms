package com.adfinbureau.wms.screen.test;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.activity.ActivityResultModule;
import com.adfinbureau.wms.screen.main.MainScreen;

import flow.Layout;
import mortar.Blueprint;

/**
 * This screen might be where you send user's once they're logged in/registered
 * as a starting point for your app.
 *
 * The implementation is incomplete and left to your imagination.
 */

@Layout(R.layout.view_test)
public class TestScreen implements Blueprint {
	@Override
	public String getMortarScopeName() {
		return getClass().getName();
	}

	@Override
	public Object getDaggerModule() {
		return new Module();
	}

	@dagger.Module(
			includes = { ActivityResultModule.class },
			injects = TestView.class,
			addsTo = MainScreen.Module.class,
			library = true
	)
 	class Module {

	}
}
