package com.adfinbureau.wms.screen.map;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.analytics.EventTracker;
import com.adfinbureau.wms.util.mortar.BaseView;
import com.google.android.gms.maps.SupportMapFragment;

import javax.inject.Inject;

import butterknife.InjectView;
import butterknife.OnClick;
import mortar.Presenter;

public class MapView extends BaseView {

	@Inject
	MapPresenter presenter;

	@Inject
	EventTracker eventTracker;

	@InjectView(R.id.mapview)
	com.google.android.gms.maps.MapView maps;

	@InjectView(R.id.fab)
	FloatingActionButton fab;

	@InjectView(R.id.txtLongMap)
	TextView lng;
	@InjectView(R.id.txtLatMap)
	TextView lat;
	@InjectView(R.id.txtAltMap)
	TextView alt;

	public MapView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected Presenter getPresenter() {
		return presenter;
	}
}
