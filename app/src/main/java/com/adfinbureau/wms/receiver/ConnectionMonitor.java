package com.adfinbureau.wms.receiver;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.adfinbureau.wms.Application;
import com.adfinbureau.wms.SharedPreferencesKeys;
import com.adfinbureau.wms.repository.JsonSharedPreferencesRepository;
import com.adfinbureau.wms.service.UpdateDataServiceNew;
import com.adfinbureau.wms.service.UploadAsbuildServiceListener;
import com.adfinbureau.wms.service.UploadDataService;
import com.adfinbureau.wms.service.UploadPictureServiceListener;
import com.adfinbureau.wms.service.UploadVideoServiceListener;

import javax.inject.Inject;

public class ConnectionMonitor extends BroadcastReceiver {

    @Inject
    JsonSharedPreferencesRepository sharedPreferencesRepository;

    @Override
    public void onReceive(Context context, Intent intent) {
        ((Application)context.getApplicationContext()).inject(this);

        String action = intent.getAction();
        if (!action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            return;
        }
        boolean noConnectivity = intent.getBooleanExtra(
                ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
        NetworkInfo aNetworkInfo = (NetworkInfo) intent
                .getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);

        if((sharedPreferencesRepository.getObject(SharedPreferencesKeys.AUTOUPLOAD, String.class) != null) &&
                !sharedPreferencesRepository.getObject(SharedPreferencesKeys.AUTOUPLOAD, String.class).contentEquals("")	) {
            if (!noConnectivity) {
                if ((aNetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE)
                        || (aNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI)) {
                    // Handle connected case
                    onConnected(context);
                }
            } else {
                if ((aNetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE)
                        || (aNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI)) {
                    // Handle disconnected case
                    onDisconnected(context);
                }
            }
        }
    }

    private void onDisconnected(Context context) {
        Log.d("ConnectionMonitor", "INTERNET DISCONNECTED, STOPPING SERVICE...");

        if(isMyServiceRunning(context, UploadDataService.class)){
            Intent msgIntent = new Intent(context, UploadDataService.class);
            context.stopService(msgIntent);
            Log.d("ConnectionMonitor", "STOP SERVICE UploadDataListener, Stoppping...");
        }else{
            Log.d("ConnectionMonitor", "UploadDataListener IS NOT RUNNING");
        }

        if(isMyServiceRunning(context, UploadPictureServiceListener.class)) {
            Intent msgIntent = new Intent(context, UploadPictureServiceListener.class);
            context.stopService(msgIntent);
            Log.d("ConnectionMonitor", "UploadPictureServiceListener, Stoppping...");
        }else{
            Log.d("ConnectionMonitor", "UploadPictureServiceListener IS NOT RUNNING");
        }

        if(isMyServiceRunning(context, UploadVideoServiceListener.class)) {
            Intent msgIntent = new Intent(context, UploadVideoServiceListener.class);
            context.stopService(msgIntent);
            Log.d("ConnectionMonitor", "UploadVideoServiceListener, Stoppping...");
        }else{
            Log.d("ConnectionMonitor", "UploadVideoServiceListener IS NOT RUNNING");
        }

        if(isMyServiceRunning(context, UploadAsbuildServiceListener.class)) {
            Intent msgIntent = new Intent(context, UploadAsbuildServiceListener.class);
            context.stopService(msgIntent);
            Log.d("ConnectionMonitor", "UploadAsbuildServiceListener, Stoppping...");
        }else{
            Log.d("ConnectionMonitor", "UploadAsbuildServiceListener IS NOT RUNNING");
        }

        /*
        if(isMyServiceRunning(context, UpdateDataServiceNew.class)) {
            Intent msgIntent = new Intent(context, UpdateDataServiceNew.class);
            context.stopService(msgIntent);
            Log.d("ConnectionMonitor", "STOP SERVICE UpdateDataServiceNew ALREADY RUNNING");
        }else{
            Log.d("ConnectionMonitor", "STOP SERVICE UpdateDataServiceNew NOT RUNNING");
        }
        */
    }

    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    
    private void onConnected(Context context){
        Log.d("ConnectionMonitor", "INTERNET CONNECTED, STARTING SERVICE...");

//        if(!isMyServiceRunning(context, SendLogService.class)) {
//            Intent msgIntent = new Intent(context, SendLogService.class);
//            context.startService(msgIntent);
//            Log.d("ConnectionMonitor", "START SERVICE SendLogService NOT RUNNING");
//        }else{
//            Log.d("ConnectionMonitor", "START SERVICE SendLogService ALREADY RUNNING");
//        }

        if(!isMyServiceRunning(context, UploadDataService.class)) {
            Intent msgIntent = new Intent(context, UploadDataService.class);
            context.startService(msgIntent);
            Log.d("ConnectionMonitor", "UploadDataService, Starting....");
        }else{
            Log.d("ConnectionMonitor", "UploadDataService ALREADY RUNNING");
        }

        if(!isMyServiceRunning(context, UploadPictureServiceListener.class)) {
            Intent msgIntent = new Intent(context, UploadPictureServiceListener.class);
            context.startService(msgIntent);
            Log.d("ConnectionMonitor", "UploadPictureServiceListener, Starting....");
        }else{
            Log.d("ConnectionMonitor", "UploadPictureServiceListener ALREADY RUNNING");
        }

        if(!isMyServiceRunning(context, UploadVideoServiceListener.class)) {
            Intent msgIntent = new Intent(context, UploadVideoServiceListener.class);
            context.startService(msgIntent);
            Log.d("ConnectionMonitor", "UploadVideoServiceListener, Starting...");
        }else{
            Log.d("ConnectionMonitor", "UploadVideoServiceListener ALREADY RUNNING");
        }

        if(!isMyServiceRunning(context, UploadAsbuildServiceListener.class)) {
            Intent msgIntent = new Intent(context, UploadAsbuildServiceListener.class);
            context.startService(msgIntent);
            Log.d("ConnectionMonitor", "UploadAsbuildServiceListener, Starting....");
        }else{
            Log.d("ConnectionMonitor", "UploadAsbuildServiceListener ALREADY RUNNING");
        }

        /*
        if(!isMyServiceRunning(context, UpdateDataServiceNew.class)) {
            Intent msgIntent = new Intent(context, UpdateDataServiceNew.class);
            context.startService(msgIntent);
            Log.d("ConnectionMonitor", "UpdateDataServiceNew, Starting....");
        }else{
            Log.d("ConnectionMonitor", "UpdateDataServiceNew ALREADY RUNNING");
        }
        */
    }
}