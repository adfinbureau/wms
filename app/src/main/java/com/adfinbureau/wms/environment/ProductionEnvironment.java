package com.adfinbureau.wms.environment;

public class ProductionEnvironment extends Environment {

	public ProductionEnvironment() {
		super("Production");
	}

	@Override
	public String getMixpanelToken() {
		return "replace with your token here";
	}

	@Override
	public String getApiHost() {
		return "http://192.168.43.217:8080/";
	}
}