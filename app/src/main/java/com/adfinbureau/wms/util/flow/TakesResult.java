package com.adfinbureau.wms.util.flow;

public interface TakesResult<T> {
	void receive(T result);
}
