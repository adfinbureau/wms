package com.adfinbureau.wms.util.socket;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidParameterException;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import android_serialport_api.SerialPort;
import android_serialport_api.SerialPortFinder;

public class LocationThread extends Thread
{
	private static final String TAG = "LocationThread";

	private OnLocationReceived handler;
	private Context context;

	public InputStream mInputStream;
	
	NMEAStringReader reader;

	public boolean requested = true;

	public LocationThread(Context context, OnLocationReceived handler) {
		this.handler = handler;
		this.context = context;
		try {
			this.mInputStream = getSerialPort().getInputStream();
		} catch (InvalidParameterException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			reader = new NMEAStringReader(handler);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void run()
	{
		// TODO: Implement this method
		super.run();
		Log.e(TAG, "THREAD STARTING");
		while(requested){
			Log.e(TAG, "TRY READ LONGLAT FROM GPS");

			int size;
			try {
				byte[] buffer = new byte[2000];
				if (mInputStream == null) {
					Log.e(TAG, "INPUT STREAM NULL");
					return;
				}else{
					Log.e(TAG, "INPUT STREAM ADA");
				}
				Log.e(TAG, "START READ FROM INPUT STREAM");
				size = mInputStream.read(buffer);
				Log.e(TAG, "END READ FROM INPUT STREAM");

				if (size > 0) {
					Log.e(TAG, "ADA INPUTAN");
					reader.read(buffer);
				}
				else{
					Log.e(TAG, "TIDAK ADA INPUTAN");

				}
			} catch (IOException e) {
				Log.e(TAG, "TRY READ LONGLAT FROM GPS IOEXCEPTION "+e.getMessage());
				e.printStackTrace();
				return;
			}
		}
	}
	
	//serial
	public SerialPortFinder mSerialPortFinder = new SerialPortFinder();
	private SerialPort mSerialPort = null;

	public SerialPort getSerialPort() throws SecurityException, IOException, InvalidParameterException {
		if (mSerialPort == null) {
			/* Read serial port parameters */
			SharedPreferences sp = context.getSharedPreferences("android_serialport_api.sample_preferences", context.MODE_PRIVATE);
			String path = "/dev/ttyMT3";
			int baudrate = 9600;

			/* Check parameters */
			if ( (path.length() == 0) || (baudrate == -1)) {
				throw new InvalidParameterException();
			}

			/* Open the serial port */
			mSerialPort = new SerialPort(new File(path), baudrate, 0);
		}
		return mSerialPort;
	}

	public void closeSerialPort() {
		if (mSerialPort != null) {
			mSerialPort.close();
			mSerialPort = null;
		}
	}
}
