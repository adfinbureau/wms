/*
 * Copyright (C) 2015 Antonio Leiva
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.adfinbureau.wms.util.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.model.LocationData;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import rx.functions.Action1;

public class AsBuildListAdapter extends RecyclerView.Adapter<AsBuildListAdapter.ViewHolder> implements View.OnClickListener, Action1<List<LocationData>> {

    private List<LocationData> items;
    private OnItemClickListener onItemClickListener;
    private Context context;

    public AsBuildListAdapter(Context context, List<LocationData> items) {
        this.items = items;
        this.context = context;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler, parent, false);
        v.setOnClickListener(this);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        LocationData item = items.get(position);
//        holder.text.setText(item.description());
        holder.text.setText(item.description()+" "+item.latitude()+" "+item.longitude()+" "+item.status());
        //Picasso picassoInstance = new Picasso.Builder(context).addRequestHandler(new VideoRequestHandler()).build();
        Picasso.with(holder.image.getContext()).load(new File(item.asbuild())).config(Bitmap.Config.RGB_565).fit().centerCrop().into(holder.image);
        //holder.image.setImageBitmap(decodeFromFile(item.picture()));
//        holder.image.setImageResource(R.drawable.arrow);


        //Picasso.with(holder.image.getContext()).load(item.getImage()).into(holder.image);
        holder.itemView.setTag(item);
    }

    @Override public int getItemCount() {
        return items.size();
    }

    @Override public void onClick(final View v) {
        onItemClickListener.onItemClick(v, (LocationData) v.getTag());
    }

    @Override
    public void call(List<LocationData> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView text;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
            text = (TextView) itemView.findViewById(R.id.text);
        }
    }

    public interface OnItemClickListener {

        void onItemClick(View view, final LocationData viewModel);

    }
}
