package com.adfinbureau.wms.util.bluetooth;

import android.location.Location;

public interface OnLocationReceived {
	public void sendLocation(Location location);
}
