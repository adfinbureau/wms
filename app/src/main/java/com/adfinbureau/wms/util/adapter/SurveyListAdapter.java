/*
 * Copyright (C) 2015 Antonio Leiva
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.adfinbureau.wms.util.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.model.Survey;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import rx.functions.Action1;

public class SurveyListAdapter extends RecyclerView.Adapter<SurveyListAdapter.ViewHolder> implements View.OnClickListener, Action1<List<Survey>> {

    private List<Survey> items;
    private OnItemClickListener onItemClickListener;
    public Context context;

    public SurveyListAdapter(Context context, List<Survey> items) {
        this.items = items;
        this.context = context;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.survey_list_row, parent, false);
        v.setOnClickListener(this);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        Survey item = items.get(position);
//        holder.longitude.setText(item.longitude());
//        holder.latitude.setText(item.latitude());
        holder.date.setText(item.date());
        holder.itemView.setTag(item);
//        holder.maps.setTag(item);
//        holder.initializeMapView();

    }

    @Override public int getItemCount() {
        return items.size();
    }

    @Override public void onClick(final View v) {
        onItemClickListener.onItemClick(v, (Survey) v.getTag());
    }

    @Override
    public void call(List<Survey> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder  implements OnMapReadyCallback {
        public TextView longitude;
        public TextView latitude;
        public TextView date;

//        public MapView maps;
//        GoogleMap map;

        public ViewHolder(View itemView) {
            super(itemView);

            longitude = (TextView) itemView.findViewById(R.id.longitude);
            latitude = (TextView) itemView.findViewById(R.id.latitude);
            date = (TextView) itemView.findViewById(R.id.date);

//            maps = (MapView) itemView.findViewById(R.id.lite_listrow_map);
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
//            MapsInitializer.initialize(context);
//            map = googleMap;
//            map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
//                @Override
//                public void onMapClick(LatLng latLng) {
//                    onItemClickListener.onItemClick(maps, (Survey) maps.getTag());
//                }
//            });
            /*
            maps.getMap().setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    onItemClickListener.onItemClick(maps, (LocationLists) maps.getTag());
                }
            });
            */

//            Survey data = (Survey) maps.getTag();
//            if (data != null) {
//                setMapLocation(map, data);
//            }
        }

        void setMapLocation(GoogleMap map, Survey data) {
            // Add a marker for this item and set the camera
//            LatLng loc = new LatLng(Double.valueOf(data.latitude()), Double.valueOf(data.longitude()));
//            map.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 13f));
//            map.addMarker(new MarkerOptions().position(loc));

            // Set the map type back to normal.
//            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        }

        /**
         * Initialises the MapView by calling its lifecycle methods.
         */
        public void initializeMapView() {
//            if (maps != null) {
//                // Initialise the MapView
//                maps.onCreate(null);
//                // Set the map ready callback to receive the GoogleMap object
//                maps.getMapAsync(this);
//            }
        }
    }

    public interface OnItemClickListener {

        void onItemClick(View view, Survey survey);

    }
}
