package com.adfinbureau.wms.util.helper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
  * A class to take a file and split it into smaller files 
  * (for example, for fitting a file on floppy disks).
  * <P>To execute the program to split files apart, pass the -split
  * option and specify a filename:<BR>
  * <I>java FileSplitter -split bigFile.zip</I>
  * <P>To join a file back together, specify the -join flag and
  * give the base filename:<BR>
  * <I>java FileSplitter -join bigFile.zip</I><BR>
  * @author Keith Trnka
  */
public class FileSplitter
	{
	/** 
	  * a constant representing the size of a chunk that would go on a floppy disk.
	  * 1.4 MB is used instead of 1.44 MB so that it isn't too close to the limit.
	  */
	public static final long floppySize = (long)(1.4 * 1024 * 1024);
	
	/** the maximum size of each file "chunk" generated, in bytes */
	public static long chunkSize = 100000;
	
	public static void main(String[] args) throws Exception
		{
		if (args.length != 2)
			{
			System.out.println("Must specify a flag -split or -join and a file argument. The file argument for splitting is the file to split and for joining is the base filename to join on.");
			System.exit(0);
			}
			
		try
			{
			if (args[0].equalsIgnoreCase("-split"))
				split(args[1], "image");
			else if (args[0].equalsIgnoreCase("-join"))
				join(args[1]);
			else
				{
				System.out.println("The first argument must be an option:");
				System.out.println("\t-split: split the specified file");
				System.out.println("\t-join: join all splitter outfiles with the specified base filename");
				System.exit(0);
				}
			}
		catch (FileNotFoundException e)
			{
			System.out.println("File not found: " + args[1]);
			}
		catch (IOException e)
			{
			System.out.println("IO Error");
			}
		}

		public static void resizeImage(String mCurrentPhotoPath){
			Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
			Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, 300, 200, false);

			ExifInterface exif;
			try {
				exif = new ExifInterface(mCurrentPhotoPath);

				int orientation = exif.getAttributeInt(
						ExifInterface.TAG_ORIENTATION, 0);
				Log.d("EXIF", "Exif: " + orientation);
				Matrix matrix = new Matrix();
				if (orientation == 6) {
					matrix.postRotate(90);
					Log.d("EXIF", "Exif: " + orientation);
				} else if (orientation == 3) {
					matrix.postRotate(180);
					Log.d("EXIF", "Exif: " + orientation);
				} else if (orientation == 8) {
					matrix.postRotate(270);
					Log.d("EXIF", "Exif: " + orientation);
				}
				scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
						scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
						true);
			} catch (IOException e) {
				e.printStackTrace();
			}
			new File(mCurrentPhotoPath).delete();

			FileOutputStream out = null;
			try {
				out = new FileOutputStream(mCurrentPhotoPath);
				scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out); // bmp is your Bitmap instance
				// PNG is a lossless format, the compression factor (100) is ignored
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (out != null) {
						out.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		public static void compressImage(String filePath) {
			Bitmap scaledBitmap = null;

			BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
			options.inJustDecodeBounds = true;
			Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

			int actualHeight = options.outHeight;
			int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

			float maxHeight = 816.0f;
			float maxWidth = 612.0f;
			float imgRatio = actualWidth / actualHeight;
			float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

			if (actualHeight > maxHeight || actualWidth > maxWidth) {
				if (imgRatio < maxRatio) {               imgRatio = maxHeight / actualHeight;                actualWidth = (int) (imgRatio * actualWidth);               actualHeight = (int) maxHeight;             } else if (imgRatio > maxRatio) {
					imgRatio = maxWidth / actualWidth;
					actualHeight = (int) (imgRatio * actualHeight);
					actualWidth = (int) maxWidth;
				} else {
					actualHeight = (int) maxHeight;
					actualWidth = (int) maxWidth;

				}
			}

//      setting inSampleSize value allows to load a scaled down version of the original image

			options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
			options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
			options.inPurgeable = true;
			options.inInputShareable = true;
			options.inTempStorage = new byte[16 * 1024];

			try {
//          load the bitmap from its path
				bmp = BitmapFactory.decodeFile(filePath, options);
			} catch (OutOfMemoryError exception) {
				exception.printStackTrace();

			}
			try {
				scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight,Bitmap.Config.ARGB_8888);
			} catch (OutOfMemoryError exception) {
				exception.printStackTrace();
			}

			float ratioX = actualWidth / (float) options.outWidth;
			float ratioY = actualHeight / (float) options.outHeight;
			float middleX = actualWidth / 2.0f;
			float middleY = actualHeight / 2.0f;

			Matrix scaleMatrix = new Matrix();
			scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

			Canvas canvas = new Canvas(scaledBitmap);
			canvas.setMatrix(scaleMatrix);
			canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
			ExifInterface exif;
			try {
				exif = new ExifInterface(filePath);

				int orientation = exif.getAttributeInt(
						ExifInterface.TAG_ORIENTATION, 0);
				Log.d("EXIF", "Exif: " + orientation);
				Matrix matrix = new Matrix();
				if (orientation == 6) {
					matrix.postRotate(90);
					Log.d("EXIF", "Exif: " + orientation);
				} else if (orientation == 3) {
					matrix.postRotate(180);
					Log.d("EXIF", "Exif: " + orientation);
				} else if (orientation == 8) {
					matrix.postRotate(270);
					Log.d("EXIF", "Exif: " + orientation);
				}
				scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
						scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
						true);
			} catch (IOException e) {
				e.printStackTrace();
			}

			new File(filePath).delete();

			FileOutputStream out = null;
			String filename = filePath;
			try {
				out = new FileOutputStream(filename);
				//write the compressed bitmap at the destination specified by filename.
				scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}

		public static void copyFile(File sourceFile, File destFile) throws IOException {
			if (!sourceFile.exists()) {
				return;
			}

			FileChannel source = null;
			FileChannel destination = null;
			source = new FileInputStream(sourceFile).getChannel();
			destination = new FileOutputStream(destFile).getChannel();
			if (destination != null && source != null) {
				destination.transferFrom(source, 0, source.size());
			}
			if (source != null) {
				source.close();
			}
			if (destination != null) {
				destination.close();
			}
		}

		public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
			final int height = options.outHeight;
			final int width = options.outWidth;
			int inSampleSize = 1;

			if (height > reqHeight || width > reqWidth) {
				final int heightRatio = Math.round((float) height/ (float) reqHeight);
				final int widthRatio = Math.round((float) width / (float) reqWidth);
				inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;      }       final float totalPixels = width * height;       final float totalReqPixelsCap = reqWidth * reqHeight * 2;       while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
				inSampleSize++;
			}

			return inSampleSize;
		}
	/**
	  * split the file specified by filename into pieces, each of size
	  * chunkSize except for the last one, which may be smaller
	  */
	public static int split(String filename, String type) throws FileNotFoundException, IOException
		{
//			if(type.contentEquals("image")) {
//				compressImage(filename);
//			}
			// open the file
		BufferedInputStream in = new BufferedInputStream(new FileInputStream(filename));
		
		// get the file length
		File f = new File(filename);
		long fileSize = f.length();
		
		// loop for each full chunk
		int subfile;
		for (subfile = 0; subfile < fileSize / chunkSize; subfile++)
			{
			// open the output file
			BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(filename + "." + subfile));
			
			// write the right amount of bytes
			for (int currentByte = 0; currentByte < chunkSize; currentByte++)
				{
				// load one byte from the input file and write it to the output file
				out.write(in.read());
				}
				
			// close the file
			out.close();
			}
		System.out.println("SUBFILE "+(subfile+1));
		
		// loop for the last chunk (which may be smaller than the chunk size)
		if (fileSize != chunkSize * (subfile - 1))
			{
			// open the output file
			BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(filename + "." + subfile));
			// write the rest of the file
			int b;
			while ((b = in.read()) != -1)
				out.write(b);
				
			// close the file
			out.close();			
			}
		
		// close the file
		in.close();
		
		return subfile+1;
	}
		
	/**
	  * list all files in the directory specified by the baseFilename
	  * , find out how many parts there are, and then concatenate them
	  * together to create a file with the filename <I>baseFilename</I>.
	  */
	public static void join(String baseFilename) throws IOException
		{
		int numberParts = getNumberParts(baseFilename);
		System.out.println(numberParts);
		// now, assume that the files are correctly numbered in order (that some joker didn't delete any part)
		BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(baseFilename));
		for (int part = 0; part < numberParts; part++)
			{
			BufferedInputStream in = new BufferedInputStream(new FileInputStream(baseFilename + "." + part));

			int b;
			while ( (b = in.read()) != -1 )
				out.write(b);

			in.close();
			}
		out.close();
		}
	
	/**
	  * find out how many chunks there are to the base filename
	  */
	public static int getNumberParts(String baseFilename) throws IOException
		{
		// list all files in the same directory
		File directory = new File(baseFilename).getAbsoluteFile().getParentFile();
		final String justFilename = new File(baseFilename).getName();
		String[] matchingFiles = directory.list(new FilenameFilter()
			{
			public boolean accept(File dir, String name)
				{
				return name.startsWith(justFilename) && name.substring(justFilename.length()).matches("^\\.\\d+$");
				}
			});
		return matchingFiles.length;
		}

		public static void deleteParts(String baseFilename) throws IOException{
			int numberParts = getNumberParts(baseFilename);
			for (int part = 0; part < numberParts; part++){
				File x = new File(baseFilename + "." + part);
				x.delete();
//			BufferedInputStream in = new BufferedInputStream(new FileInputStream(baseFilename + "." + part));
			}
		}
	}