/* 
 * FileExample.java
 * Copyright (C) 2011 Kimmo Tuukkanen
 * 
 * This file is part of Java Marine API.
 * <http://ktuukkan.github.io/marine-api/>
 * 
 * Java Marine API is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * Java Marine API is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java Marine API. If not, see <http://www.gnu.org/licenses/>.
 */
package com.adfinbureau.wms.util.bluetooth;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import android.location.Location;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.location.GpsStatus.NmeaListener;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.text.TextUtils.SimpleStringSplitter;
import android.util.Log;
import android.widget.Toast;
import net.sf.marineapi.nmea.event.SentenceEvent;
import net.sf.marineapi.nmea.event.SentenceListener;
import net.sf.marineapi.nmea.io.SentenceReader;
import net.sf.marineapi.nmea.parser.SentenceFactory;
import net.sf.marineapi.nmea.sentence.APBSentence;
import net.sf.marineapi.nmea.sentence.BODSentence;
import net.sf.marineapi.nmea.sentence.GGASentence;
import net.sf.marineapi.nmea.sentence.GLLSentence;
import net.sf.marineapi.nmea.sentence.GSASentence;
import net.sf.marineapi.nmea.sentence.GSVSentence;
import net.sf.marineapi.nmea.sentence.RMBSentence;
import net.sf.marineapi.nmea.sentence.RMCSentence;
import net.sf.marineapi.nmea.sentence.RTESentence;
import net.sf.marineapi.nmea.sentence.Sentence;
import net.sf.marineapi.nmea.sentence.SentenceId;
import net.sf.marineapi.nmea.sentence.WPLSentence;
import net.sf.marineapi.nmea.util.Position;

/**
 * Simple example application that takes a filename as command-line argument and
 * prints Position from received GGA sentences.
 * 
 * @author Kimmo Tuukkanen
 */
public class NMEAStringReader implements SentenceListener {
	//private LocationManager lm;
	private Location fix = null;
	private String fixTime = null;
	private long fixTimestamp;
	private boolean hasGGA = false;
	private boolean hasRMC = false;
	private static final String LOG_TAG = "BlueGPS";
	private float precision = 10f;

	private InputStream input;
	private SentenceReader reader;
	private OnLocationReceived locationReceived; 
	
	public SentenceReader getReader() {
		return reader;
	}


	public NMEAStringReader(String input, OnLocationReceived locationReceived) throws IOException {
		// create sentence reader and provide input stream
//		InputStream stream = new ByteArrayInputStream(input.getBytes());
//		reader = new SentenceReader(stream);
		this.locationReceived = locationReceived;
		
		// register self as a listener for GGA sentences
//		reader.addSentenceListener(this, SentenceId.GGA);
//		reader.addSentenceListener(this, SentenceId.RMC);
//		reader.addSentenceListener(this, SentenceId.GLL);
//		reader.start();
	}

	public NMEAStringReader(OnLocationReceived locationReceived) throws IOException {
		this.locationReceived = locationReceived;
	}
	

	public void read(byte[] buffer) {
		input = new ByteArrayInputStream(buffer);
		
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(input,"US-ASCII"));
			String s;
			long now = SystemClock.uptimeMillis();
			long lastRead = now;
			//while((enabled) && (now < lastRead+5000 )){
			while( (now < lastRead+1000 )){
				if (reader.ready()){
					s = reader.readLine();
					Log.v(LOG_TAG, "data: "+System.currentTimeMillis()+" "+s);
					notifyNmeaSentence(s+"\r\n");
					//ready = true;
					lastRead = SystemClock.uptimeMillis();
				} else {
					Log.d(LOG_TAG, "data: not ready "+System.currentTimeMillis());
					
					SystemClock.sleep(500);
				}
				now = SystemClock.uptimeMillis();
			}
		} catch (IOException e) {
			Log.e(LOG_TAG, "error while getting data", e);
			Log.i("ADFINERROR", "ERROR: " + e.getMessage());
			//setMockLocationProviderOutOfService();
		} 
	}
	private synchronized void disableIfNeeded(){
/*		if (enabled){
			if (nbRetriesRemaining > 0){
				// Unable to connect
				Log.e(LOG_TAG, "Unable to establish connection");
				connectionProblemNotification.when = System.currentTimeMillis();
				String pbMessage = appContext.getResources().getQuantityString(R.plurals.connection_problem_notification, nbRetriesRemaining, nbRetriesRemaining);
				connectionProblemNotification.setLatestEventInfo(appContext, 
						appContext.getString(R.string.connection_problem_notification_title), 
						pbMessage, 
						connectionProblemNotification.contentIntent);
				connectionProblemNotification.number = 1 + maxConnectionRetries - nbRetriesRemaining;
				notificationManager.notify(R.string.connection_problem_notification_title, connectionProblemNotification);
			} else {
//				notificationManager.cancel(R.string.connection_problem_notification_title);
//				serviceStoppedNotification.when = System.currentTimeMillis();
//				notificationManager.notify(R.string.service_closed_because_connection_problem_notification_title, serviceStoppedNotification);
				disable(R.string.msg_two_many_connection_problems);
			}
		}*/
	}
	private void notifyNmeaSentence(final String nmeaSentence){
		try {
			parseNmeaSentence(nmeaSentence);
		} catch (SecurityException e){}
	}	
	public String parseNmeaSentence(String gpsSentence) throws SecurityException {
		String nmeaSentence = null;
		//Log.v(LOG_TAG, "data: "+System.currentTimeMillis()+" "+gpsSentence);
		Pattern xx = Pattern.compile("\\$([^*$]*)\\*([0-9A-F][0-9A-F])?\r\n");
		Matcher m = xx.matcher(gpsSentence);
		if (m.matches()){
			nmeaSentence = m.group(0);
			String sentence = m.group(1);
			//String checkSum = m.group(2);
			//Log.v(LOG_TAG, "data: "+System.currentTimeMillis()+" "+sentence+" cheksum; "+checkSum +" control: "+String.format("%X",computeChecksum(sentence)));
			SimpleStringSplitter splitter = new SimpleStringSplitter(',');
			splitter.setString(sentence);
			String command = splitter.next();
			if (TextUtils.isEmpty(command)) {
				return nmeaSentence;
			}
			Log.i("ADFINNMEA", gpsSentence);
			Log.i("ADFIN",System.currentTimeMillis()+ ">>data: " + command);
		//$GNGGA
			if (command.length() > 3 && command.substring(command.length() - 3).equals("GGA")){
				// UTC time of fix HHmmss.S
				String time = splitter.next();
				if (TextUtils.isEmpty(time)) {
					return nmeaSentence;
				}
				// latitude ddmm.M
				String lat = splitter.next();
				if (TextUtils.isEmpty(lat)) {
					return nmeaSentence;
				}
				// direction (N/S)
				String latDir = splitter.next();
				if (TextUtils.isEmpty(latDir)) {
					return nmeaSentence;
				}
				// longitude dddmm.M
				String lon = splitter.next();
				if (TextUtils.isEmpty(lon)) {
					return nmeaSentence;
				}
				// direction (E/W)
				String lonDir = splitter.next();
				if (TextUtils.isEmpty(lonDir)) {
					return nmeaSentence;
				}
				 			 
				String quality = splitter.next();
				if (TextUtils.isEmpty(quality)) {
					return nmeaSentence;
				}
				String numsattelite = splitter.next();
				String hor = splitter.next();
				String altitude = splitter.next();

				if (quality != null && !quality.equals("") && !quality.equals("0") ){			
					if (! time.equals(fixTime)){

						fix = new Location("dummyprovider");
						fixTime = time;
						fixTimestamp = parseNmeaTime(time);
						fix.setTime(fixTimestamp);				
					}
					if (lat != null && !lat.equals("")){
						fix.setLatitude(parseNmeaLatitude(lat,latDir));
					}
					if (lon != null && !lon.equals("")){
						fix.setLongitude(parseNmeaLongitude(lon,lonDir));
					}
					if (altitude != null && !altitude.equals("")){
						fix.setAltitude(Double.parseDouble(altitude));
					}
					hasGGA = true;
					if(fix != null){
						try{
							Log.i("ADFIN", "data: " + "Received");
							locationReceived.sendLocation(fix);	
							Log.i("ADFINPOS",  lat + "|" + lon);
							Log.i("ADFIN", "data: " + "Finish");
						}
						catch(Exception err ){
							Log.i("ADFINERROR", "ERROR: " + err.getMessage());
						}
					}

				} else if(quality.equals("0")){}
			}
			else if (command.length() > 3 && command.substring(command.length() - 3).equals("*RMC*")){
		
				// UTC time of fix HHmmss.S
				String time = splitter.next();
				if (TextUtils.isEmpty(time)) {
					return nmeaSentence;
				}
				// fix status (A/V)
				String status = splitter.next();
				if (TextUtils.isEmpty(status)) {
					return nmeaSentence;
				}
				// latitude ddmm.M
				String lat = splitter.next();
				if (TextUtils.isEmpty(lat)) {
					return nmeaSentence;
				}
				// direction (N/S)
				String latDir = splitter.next();
				if (TextUtils.isEmpty(latDir)) {
					return nmeaSentence;
				}
				// longitude dddmm.M
				String lon = splitter.next();
				if (TextUtils.isEmpty(lon)) {
					return nmeaSentence;
				}
				// direction (E/W)
				String lonDir = splitter.next();
				if (TextUtils.isEmpty(lonDir)) {
					return nmeaSentence;
				}

				if (status != null && !status.equals("") && status.equals("A") ){
			
					if (! time.equals(fixTime)){
						//notifyFix(fix);
						fix = new Location("dummyprovider");
						fixTime = time;
						fixTimestamp = parseNmeaTime(time);

					} 
					if (lat != null && !lat.equals("")){
						fix.setLatitude(parseNmeaLatitude(lat,latDir));
					}
					if (lon != null && !lon.equals("")){
						fix.setLongitude(parseNmeaLongitude(lon,lonDir));
					}

					hasRMC = true;
					if(fix != null){
						try{
							Log.i("ADFIN", "data: " + "Received");
							locationReceived.sendLocation(fix);	
							Log.i("ADFINPOS",  lat + "|" + lon);
							Log.i("ADFIN", "data: " + "Finish");
							//fix=null;
						}
						catch(Exception err ){
							Log.i("ADFINERROR", "ERROR: " + err.getMessage());
						}
					}
//					if (hasGGA && hasRMC){
//						notifyFix(fix);
//					}
				} else if(status.equals("V")){}		
			} else if (command.length() > 3 && command.substring(command.length() - 3).equals("*GLL*")){

				String lat = splitter.next();
				if (TextUtils.isEmpty(lat)) {
					return nmeaSentence;
				}
				// direction (N/S)
				String latDir = splitter.next();
				if (TextUtils.isEmpty(latDir)) {
					return nmeaSentence;
				}
				// longitude dddmm.M
				String lon = splitter.next();
				if (TextUtils.isEmpty(lon)) {
					return nmeaSentence;
				}
				// direction (E/W)
				String lonDir = splitter.next();
				if (TextUtils.isEmpty(lonDir)) {
					return nmeaSentence;
				}
				// UTC time of fix HHmmss.S
				String time = splitter.next();
				if (TextUtils.isEmpty(time)) {
					return nmeaSentence;
				}
				
				if (! time.equals(fixTime)){
					//notifyFix(fix);
					fix = new Location("dummyprovider");
					fixTime = time;
					fixTimestamp = parseNmeaTime(time);
					//fix.setTime(fixTimestamp);					
					//Log.v(LOG_TAG, "Fix: "+fix);
				} 
				if (lat != null && !lat.equals("")){
					fix.setLatitude(parseNmeaLatitude(lat,latDir));
				}
				if (lon != null && !lon.equals("")){
					fix.setLongitude(parseNmeaLongitude(lon,lonDir));
				}
				if(fix != null){
					try{
						Log.i("ADFIN", "data: " + "Received");
						locationReceived.sendLocation(fix);	
						Log.i("ADFINPOS",  lat + "|" + lon);
						Log.i("ADFIN", "data: " + "Finish");
						//fix=null;
					}
					catch(Exception err ){
						Log.i("ADFINERROR", "ERROR: " + err.getMessage());
					}
				}
			}
			/*if (command.substring(command.length() - 3).equals("GGA")){
				 $GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47
					
					Where:
					     GGA          Global Positioning System Fix Data
					     123519       Fix taken at 12:35:19 UTC
					     4807.038,N   Latitude 48 deg 07.038' N
					     01131.000,E  Longitude 11 deg 31.000' E
					     1            Fix quality: 0 = invalid
					                               1 = GPS fix (SPS)
					                               2 = DGPS fix
					                               3 = PPS fix
											       4 = Real Time Kinematic
											       5 = Float RTK
					                               6 = estimated (dead reckoning) (2.3 feature)
											       7 = Manual input mode
											       8 = Simulation mode
					     08           Number of satellites being tracked
					     0.9          Horizontal dilution of position
					     545.4,M      Altitude, Meters, above mean sea level
					     46.9,M       Height of geoid (mean sea level) above WGS84
					                      ellipsoid
					     (empty field) time in seconds since last DGPS update
					     (empty field) DGPS station ID number
					     *47          the checksum data, always begins with *
				 
				// UTC time of fix HHmmss.S
				String time = splitter.next();
				// latitude ddmm.M
				String lat = splitter.next();
				// direction (N/S)
				String latDir = splitter.next();
				// longitude dddmm.M
				String lon = splitter.next();
				// direction (E/W)
				String lonDir = splitter.next();
				 fix quality: 
				  	0= invalid
					1 = GPS fix (SPS)
					2 = DGPS fix
					3 = PPS fix
					4 = Real Time Kinematic
					5 = Float RTK
					6 = estimated (dead reckoning) (2.3 feature)
					7 = Manual input mode
					8 = Simulation mode
				 			 
				String quality = splitter.next();
				// Number of satellites being tracked
				String nbSat = splitter.next();
				// Horizontal dilution of position (float)
				String hdop = splitter.next();
				// Altitude, Meters, above mean sea level
				String alt = splitter.next();
				// Height of geoid (mean sea level) above WGS84 ellipsoid
				String geoAlt = splitter.next();
				// time in seconds since last DGPS update
				// DGPS station ID number
				if (quality != null && !quality.equals("") && !quality.equals("0") ){
//					if (this.mockStatus != LocationProvider.AVAILABLE){
//						long updateTime = parseNmeaTime(time);
//						notifyStatusChanged(LocationProvider.AVAILABLE, null, updateTime);
//					}				
					if (! time.equals(fixTime)){
						notifyFix(fix);
						fix = new Location("dummyprovider");
						fixTime = time;
						fixTimestamp = parseNmeaTime(time);
						fix.setTime(fixTimestamp);				
						Log.v(LOG_TAG, "Fix: "+fix);
					}
					if (lat != null && !lat.equals("")){
						fix.setLatitude(parseNmeaLatitude(lat,latDir));
					}
					if (lon != null && !lon.equals("")){
						fix.setLongitude(parseNmeaLongitude(lon,lonDir));
					}
					if (hdop != null && !hdop.equals("")){
						fix.setAccuracy(Float.parseFloat(hdop)*precision);
					}
					if (alt != null && !alt.equals("")){
						fix.setAltitude(Double.parseDouble(alt));
					}
					if (nbSat != null && !nbSat.equals("")){
						Bundle extras = new Bundle();
						extras.putInt("satellites", Integer.parseInt(nbSat));
						fix.setExtras(extras);
					}
					Log.v(LOG_TAG, "Fix: "+System.currentTimeMillis()+" "+fix);
					hasGGA = true;
					if(fix != null){
						locationReceived.sendLocation(fix);
					}
					if (hasGGA && hasRMC){
						notifyFix(fix);
					}
				} else if(quality.equals("0")){
//					if (this.mockStatus != LocationProvider.TEMPORARILY_UNAVAILABLE){
//						long updateTime = parseNmeaTime(time);
//						notifyStatusChanged(LocationProvider.TEMPORARILY_UNAVAILABLE, null, updateTime);
//					}				
				}
			} else if (command.substring(command.length() - 3).equals("RMC")){
				 $GPRMC,123519,A,4807.038,N,01131.000,E,022.4,084.4,230394,003.1,W*6A
	
				   Where:
				     RMC          Recommended Minimum sentence C
				     123519       Fix taken at 12:35:19 UTC
				     A            Status A=active or V=Void.
				     4807.038,N   Latitude 48 deg 07.038' N
				     01131.000,E  Longitude 11 deg 31.000' E
				     022.4        Speed over the ground in knots
				     084.4        Track angle in degrees True
				     230394       Date - 23rd of March 1994
				     003.1,W      Magnetic Variation
				     *6A          The checksum data, always begins with *
				
				// UTC time of fix HHmmss.S
				String time = splitter.next();
				// fix status (A/V)
				String status = splitter.next();
				// latitude ddmm.M
				String lat = splitter.next();
				// direction (N/S)
				String latDir = splitter.next();
				// longitude dddmm.M
				String lon = splitter.next();
				// direction (E/W)
				String lonDir = splitter.next();
				// Speed over the ground in knots		 
				String speed = splitter.next();
				// Track angle in degrees True
				String bearing = splitter.next();
				// UTC date of fix DDMMYY
				String date = splitter.next();
				// Magnetic Variation ddd.D
				String magn = splitter.next();
				// Magnetic variation direction (E/W)
				String magnDir = splitter.next();
				// for NMEA 0183 version 3.00 active the Mode indicator field is added
				// Mode indicator, (A=autonomous, D=differential, E=Estimated, N=not valid, S=Simulator )
				if (status != null && !status.equals("") && status.equals("A") ){
//					if (this.mockStatus != LocationProvider.AVAILABLE){
//						long updateTime = parseNmeaTime(time);
//						notifyStatusChanged(LocationProvider.AVAILABLE, null, updateTime);
//					}				
					if (! time.equals(fixTime)){
						notifyFix(fix);
						fix = new Location("dummyprovider");
						fixTime = time;
						fixTimestamp = parseNmeaTime(time);
						fix.setTime(fixTimestamp);					
						Log.v(LOG_TAG, "Fix: "+fix);
					} 
					if (lat != null && !lat.equals("")){
						fix.setLatitude(parseNmeaLatitude(lat,latDir));
					}
					if (lon != null && !lon.equals("")){
						fix.setLongitude(parseNmeaLongitude(lon,lonDir));
					}
					if (speed != null && !speed.equals("")){
						fix.setSpeed(parseNmeaSpeed(speed, "N"));
					} 
					if (bearing != null && !bearing.equals("")){
						fix.setBearing(Float.parseFloat(bearing));
					}
					Log.v(LOG_TAG, "Fix: "+System.currentTimeMillis()+" "+fix);
					hasRMC = true;
					if(fix != null){
						locationReceived.sendLocation(fix);
					}
					if (hasGGA && hasRMC){
						notifyFix(fix);
					}
				} else if(status.equals("V")){
//					if (this.mockStatus != LocationProvider.TEMPORARILY_UNAVAILABLE){
//						long updateTime = parseNmeaTime(time);
//						notifyStatusChanged(LocationProvider.TEMPORARILY_UNAVAILABLE, null, updateTime);
//					}				
				}		
			} else if (command.substring(command.length() - 3).equals("GSA")){
	
			} else if (command.substring(command.length() - 3).equals("VTG")){
				
			} else if (command.substring(command.length() - 3).equals("GLL")){
				  $GPGLL,4916.45,N,12311.12,W,225444,A,*1D
					
					Where:
					     GLL          Geographic position, Latitude and Longitude
					     4916.46,N    Latitude 49 deg. 16.45 min. North
					     12311.12,W   Longitude 123 deg. 11.12 min. West
					     225444       Fix taken at 22:54:44 UTC
					     A            Data Active or V (void)
					     *iD          checksum data
				 
				// latitude ddmm.M
				String lat = splitter.next();
				// direction (N/S)
				String latDir = splitter.next();
				// longitude dddmm.M
				String lon = splitter.next();
				// direction (E/W)
				String lonDir = splitter.next();
				// UTC time of fix HHmmss.S
				String time = splitter.next();
				// fix status (A/V)
				String status = splitter.next();
				// for NMEA 0183 version 3.00 active the Mode indicator field is added
				// Mode indicator, (A=autonomous, D=differential, E=Estimated, N=not valid, S=Simulator )
			}*/
		}
		return nmeaSentence;
	}
	public float parseNmeaSpeed(String speed,String metric){
		float meterSpeed = 0.0f;
		if (speed != null && metric != null && !speed.equals("") && !metric.equals("")){
			float temp1 = Float.parseFloat(speed)/3.6f;
			if (metric.equals("K")){
				meterSpeed = temp1;
			} else if (metric.equals("N")){
				meterSpeed = temp1*1.852f;
			}
		}
		return meterSpeed;
	}
	public double parseNmeaLatitude(String lat,String orientation){
		double latitude = 0.0;
		if (lat != null && orientation != null && !lat.equals("") && !orientation.equals("")){
			double temp1 = Double.parseDouble(lat);
			double temp2 = Math.floor(temp1/100); 
			double temp3 = (temp1/100 - temp2)/0.6;
			if (orientation.equals("S")){
				latitude = -(temp2+temp3);
			} else if (orientation.equals("N")){
				latitude = (temp2+temp3);
			}
		}
		return latitude;
	}
	public double parseNmeaLongitude(String lon,String orientation){
		double longitude = 0.0;
		if (lon != null && orientation != null && !lon.equals("") && !orientation.equals("")){
			double temp1 = Double.parseDouble(lon);
			double temp2 = Math.floor(temp1/100); 
			double temp3 = (temp1/100 - temp2)/0.6;
			if (orientation.equals("W")){
				longitude = -(temp2+temp3);
			} else if (orientation.equals("E")){
				longitude = (temp2+temp3);
			}
		}
		return longitude;
	}
	private void notifyFix(Location fix) throws SecurityException {
		fixTime = null;
		hasGGA = false;
		hasRMC=false;
		if (fix != null){
			Log.v(LOG_TAG, "New Fix: "+System.currentTimeMillis()+" "+fix);
//			if (lm != null){
//				///lm.setTestProviderLocation(mockLocationProvider, fix);
//				///Log.v(LOG_TAG, "New Fix notified to Location Manager: "+mockLocationProvider);
//			}
			this.fix = null;
		}
	}
	private void notifyStatusChanged(int status, Bundle extras, long updateTime){
		fixTime = null;
		hasGGA = false;
		hasRMC=false;
		this.fix = null;
//		if (this.mockStatus != status){
//			Log.d(LOG_TAG, "New mockStatus: "+System.currentTimeMillis()+" "+status);
////			if (lm != null && mockGpsEnabled){
////				lm.setTestProviderStatus(mockLocationProvider, status, extras, updateTime);
////				// lm.setTestProviderStatus(mockLocationProvider, status, extras, SystemClock.elapsedRealtime());
////				// lm.setTestProviderStatus(mockLocationProvider, status, extras, 50);
////				Log.v(LOG_TAG, "New mockStatus notified to Location Manager: " + status + " "+mockLocationProvider);
////			}
//			this.fix = null;
//			this.mockStatus = status;
//		}
	}
	public long parseNmeaTime(String time){
		long timestamp = 0;
		SimpleDateFormat fmt = new SimpleDateFormat("HHmmss.SSS");
		fmt.setTimeZone(TimeZone.getTimeZone("GMT"));
		try {
			if (time != null && time != null){
				long now = System.currentTimeMillis();
				long today = now - (now %86400000L);
				long temp1;
				// sometime we don't have millisecond in the time string, so we have to reformat it 
				temp1 = fmt.parse(String.format((Locale)null,"%010.3f", Double.parseDouble(time))).getTime();
				long temp2 = today+temp1;
				// if we're around midnight we could have a problem...
				if (temp2 - now > 43200000L) {
					timestamp  = temp2 - 86400000L;
				} else if (now - temp2 > 43200000L){
					timestamp  = temp2 + 86400000L;
				} else {
					timestamp  = temp2;
				}
			}
		} catch (ParseException e) {
			Log.e(LOG_TAG, "Error while parsing NMEA time", e);
		}
		return timestamp;
	}
	public byte computeChecksum(String s){
		byte checksum = 0;
		for (char c : s.toCharArray()){
			checksum ^= (byte)c;			
		}
		return checksum;
	}
	/*
	 * (non-Javadoc)
	 * @see net.sf.marineapi.nmea.event.SentenceListener#readingPaused()
	 */
	public void readingPaused() {
		System.out.println("-- Paused --");
	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.marineapi.nmea.event.SentenceListener#readingStarted()
	 */
	public void readingStarted() {
		System.out.println("-- Started --");
	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.marineapi.nmea.event.SentenceListener#readingStopped()
	 */
	public void readingStopped() {
		System.out.println("-- Stopped --");
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * net.sf.marineapi.nmea.event.SentenceListener#sentenceRead(net.sf.marineapi
	 * .nmea.event.SentenceEvent)
	 */
	public void sentenceRead(SentenceEvent event) {
		// Safe to cast as we are registered only for GGA updates. Could
		// also cast to PositionSentence if interested only in position data.
		// When receiving all sentences without filtering, you should check the
		// sentence type before casting (e.g. with Sentence.getSentenceId()).
		System.out.println("BEGIN CHAR : "+event.getSentence().getSentenceId()+" | "+event.getSentence().toSentence());
		try{
			if(event.getSentence().getSentenceId().contentEquals("GGA")){
				GGASentence s = (GGASentence) event.getSentence();
				Location loc = new Location(LocationManager.GPS_PROVIDER);
				Position pos = s.getPosition();
				double latitude = 0;
				double longitude = 0;
				double altitude = 0;
				float accuracy = 0;
				latitude = pos.getLatitude();
				longitude = pos.getLongitude();
				altitude = pos.getAltitude();
				accuracy = (float) s.getHorizontalDOP();
				loc.setLatitude(latitude);
				loc.setLongitude(longitude);
				loc.setAltitude(altitude);
				loc.setAccuracy(accuracy);	
				if(altitude!=0){
					locationReceived.sendLocation(loc);
				}			
			}else if(event.getSentence().getSentenceId().contentEquals("RMC")){
				RMCSentence s = (RMCSentence) event.getSentence();
				Location loc = new Location(LocationManager.GPS_PROVIDER);
				Position pos = s.getPosition();
	
				double latitude = 0;
				double longitude = 0;
				double altitude = 0;
				latitude = pos.getLatitude();
				longitude = pos.getLongitude();
				altitude = pos.getAltitude();
				loc.setLatitude(latitude);
				loc.setLongitude(longitude);
				loc.setAltitude(altitude);
				
				if(altitude!=0){
					locationReceived.sendLocation(loc);
				}
			}else if(event.getSentence().getSentenceId().contentEquals("GLL")){
				GLLSentence s = (GLLSentence) event.getSentence();
				Location loc = new Location(LocationManager.GPS_PROVIDER);
				Position pos = s.getPosition();
	
				double latitude = 0;
				double longitude = 0;
				double altitude = 0;
				latitude = pos.getLatitude();
				longitude = pos.getLongitude();
				altitude = pos.getAltitude();
				
				loc.setLatitude(latitude);
				loc.setLongitude(longitude);
				loc.setAltitude(altitude);
				
	//			locationThreadHandler.onReceive();
				if(altitude!=0){
					locationReceived.sendLocation(loc);
				}
			}else if(event.getSentence().getSentenceId().contentEquals("APB")){ //buat pesawat
				APBSentence s = (APBSentence) event.getSentence();
			}else if(event.getSentence().getSentenceId().contentEquals("BOD")){
				BODSentence s = (BODSentence) event.getSentence();
			}else if(event.getSentence().getSentenceId().contentEquals("GSA")){
				GSASentence s = (GSASentence) event.getSentence();
			}else if(event.getSentence().getSentenceId().contentEquals("GSV")){
				GSVSentence s = (GSVSentence) event.getSentence();
			}else if(event.getSentence().getSentenceId().contentEquals("RMB")){
				RMBSentence s = (RMBSentence) event.getSentence();
			}else if(event.getSentence().getSentenceId().contentEquals("RTE")){
				RTESentence s = (RTESentence) event.getSentence();
			}else if(event.getSentence().getSentenceId().contentEquals("WPL")){
				WPLSentence s = (WPLSentence) event.getSentence();
			}
		}catch(Exception e){
			System.out.println("ERROR READING : "+e.getMessage());
		}
	}

}
