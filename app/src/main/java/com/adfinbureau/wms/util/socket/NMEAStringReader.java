/* 
 * FileExample.java
 * Copyright (C) 2011 Kimmo Tuukkanen
 * 
 * This file is part of Java Marine API.
 * <http://ktuukkan.github.io/marine-api/>
 * 
 * Java Marine API is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * Java Marine API is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Java Marine API. If not, see <http://www.gnu.org/licenses/>.
 */
package com.adfinbureau.wms.util.socket;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;


import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import android.widget.Toast;
import net.sf.marineapi.nmea.event.SentenceEvent;
import net.sf.marineapi.nmea.event.SentenceListener;
import net.sf.marineapi.nmea.io.SentenceReader;
import net.sf.marineapi.nmea.parser.SentenceFactory;
import net.sf.marineapi.nmea.sentence.APBSentence;
import net.sf.marineapi.nmea.sentence.BODSentence;
import net.sf.marineapi.nmea.sentence.GGASentence;
import net.sf.marineapi.nmea.sentence.GLLSentence;
import net.sf.marineapi.nmea.sentence.GSASentence;
import net.sf.marineapi.nmea.sentence.GSVSentence;
import net.sf.marineapi.nmea.sentence.RMBSentence;
import net.sf.marineapi.nmea.sentence.RMCSentence;
import net.sf.marineapi.nmea.sentence.RTESentence;
import net.sf.marineapi.nmea.sentence.Sentence;
import net.sf.marineapi.nmea.sentence.SentenceId;
import net.sf.marineapi.nmea.sentence.WPLSentence;
import net.sf.marineapi.nmea.util.Position;

/**
 * Simple example application that takes a filename as command-line argument and
 * prints Position from received GGA sentences.
 * 
 * @author Kimmo Tuukkanen
 */
public class NMEAStringReader implements SentenceListener {

	private InputStream input;
	private SentenceReader reader;
	private OnLocationReceived locationReceived;
	private static final String TAG = "NMEAStringReader";


	public SentenceReader getReader() {
		return reader;
	}
	/**
	 * Creates a new instance of FileExample
	 * @param locationThreadHandler 
	 * @param onLocationReceived 
	 * 
	 * @param file File containing NMEA data
	 * */
	public NMEAStringReader(String input, OnLocationReceived locationReceived) throws IOException {
		// create sentence reader and provide input stream
		InputStream stream = new ByteArrayInputStream(input.getBytes());
		reader = new SentenceReader(stream);
		this.locationReceived = locationReceived;
		
		// register self as a listener for GGA sentences
		reader.addSentenceListener(this, SentenceId.GGA);
		reader.addSentenceListener(this, SentenceId.RMC);
		reader.addSentenceListener(this, SentenceId.GLL);
		reader.start();
	}
	 
	
	/**
	 * Creates a new instance of FileExample
	 * @param locationThreadHandler 
	 * @param onLocationReceived 
	 * 
	 * @param file File containing NMEA data
	 */
	public NMEAStringReader(OnLocationReceived locationReceived) throws IOException {
		// create sentence reader and provide input stream
		this.locationReceived = locationReceived;

//		reader = new SentenceReader(input);
//		reader.addSentenceListener(this, SentenceId.GGA);
//		reader.addSentenceListener(this, SentenceId.RMC);
//		reader.addSentenceListener(this, SentenceId.GLL);
//		reader.start();
		/*
		byte[] data = new byte[1024];
		stream = new ByteArrayInputStream(data);
		reader = new SentenceReader(stream);
		this.locationReceived = locationReceived;
		
		// register self as a listener for GGA sentences
		reader.addSentenceListener(this, SentenceId.GGA);
		reader.addSentenceListener(this, SentenceId.RMC);
		reader.addSentenceListener(this, SentenceId.GLL);
		*/
//		reader.start();
	}
	

	public void read(byte[] buffer) {
		// TODO Auto-generated method stub
		input = new ByteArrayInputStream(buffer);
		reader = new SentenceReader(input);
		reader.addSentenceListener(this, SentenceId.GGA);
		reader.addSentenceListener(this, SentenceId.RMC);
		reader.addSentenceListener(this, SentenceId.GLL);
		reader.start();
	}
	/*
	 * (non-Javadoc)
	 * @see net.sf.marineapi.nmea.event.SentenceListener#readingPaused()
	 */
	public void readingPaused() {
		System.out.println("-- Paused --");
	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.marineapi.nmea.event.SentenceListener#readingStarted()
	 */
	public void readingStarted() {
		System.out.println("-- Started --");
	}

	/*
	 * (non-Javadoc)
	 * @see net.sf.marineapi.nmea.event.SentenceListener#readingStopped()
	 */
	public void readingStopped() {
		System.out.println("-- Stopped --");
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * net.sf.marineapi.nmea.event.SentenceListener#sentenceRead(net.sf.marineapi
	 * .nmea.event.SentenceEvent)
	 */
	public void sentenceRead(SentenceEvent event) {
		// Safe to cast as we are registered only for GGA updates. Could
		// also cast to PositionSentence if interested only in position data.
		// When receiving all sentences without filtering, you should check the
		// sentence type before casting (e.g. with Sentence.getSentenceId()).
		Log.d(TAG, "READ FROM SOCKET " + "BEGIN CHAR : " + event.getSentence().getSentenceId() + " | " + event.getSentence().toSentence());
		try{
			if(event.getSentence().getSentenceId().contentEquals("GGA")){
				GGASentence s = (GGASentence) event.getSentence();
				Location loc = new Location(LocationManager.GPS_PROVIDER);
				Position pos = s.getPosition();
				double latitude = 0;
				double longitude = 0;
				double altitude = 0;
				float accuracy = 0;
				latitude = pos.getLatitude();
				longitude = pos.getLongitude();
				altitude = pos.getAltitude();
				accuracy = (float) s.getHorizontalDOP();
				loc.setLatitude(latitude);
				loc.setLongitude(longitude);
				loc.setAltitude(altitude);
				loc.setAccuracy(accuracy);	
//				if(altitude!=0){
					locationReceived.sendLocation(loc);
//				}
			}else if(event.getSentence().getSentenceId().contentEquals("RMC")){
				RMCSentence s = (RMCSentence) event.getSentence();
				Location loc = new Location(LocationManager.GPS_PROVIDER);
				Position pos = s.getPosition();
	
				double latitude = 0;
				double longitude = 0;
				double altitude = 0;
				latitude = pos.getLatitude();
				longitude = pos.getLongitude();
				altitude = pos.getAltitude();
				loc.setLatitude(latitude);
				loc.setLongitude(longitude);
				loc.setAltitude(altitude);
				
				if(altitude!=0){
					locationReceived.sendLocation(loc);
				}
			}else if(event.getSentence().getSentenceId().contentEquals("GLL")){
				GLLSentence s = (GLLSentence) event.getSentence();
				Location loc = new Location(LocationManager.GPS_PROVIDER);
				Position pos = s.getPosition();
	
				double latitude = 0;
				double longitude = 0;
				double altitude = 0;
				latitude = pos.getLatitude();
				longitude = pos.getLongitude();
				altitude = pos.getAltitude();
				
				loc.setLatitude(latitude);
				loc.setLongitude(longitude);
				loc.setAltitude(altitude);
				
	//			locationThreadHandler.onReceive();
				if(altitude!=0){
					locationReceived.sendLocation(loc);
				}
			}else if(event.getSentence().getSentenceId().contentEquals("APB")){ //buat pesawat
				APBSentence s = (APBSentence) event.getSentence();
			}else if(event.getSentence().getSentenceId().contentEquals("BOD")){
				BODSentence s = (BODSentence) event.getSentence();
			}else if(event.getSentence().getSentenceId().contentEquals("GSA")){
				GSASentence s = (GSASentence) event.getSentence();
			}else if(event.getSentence().getSentenceId().contentEquals("GSV")){
				GSVSentence s = (GSVSentence) event.getSentence();
			}else if(event.getSentence().getSentenceId().contentEquals("RMB")){
				RMBSentence s = (RMBSentence) event.getSentence();
			}else if(event.getSentence().getSentenceId().contentEquals("RTE")){
				RTESentence s = (RTESentence) event.getSentence();
			}else if(event.getSentence().getSentenceId().contentEquals("WPL")){
				WPLSentence s = (WPLSentence) event.getSentence();
			}
		}catch(Exception e){
			Log.e(TAG, "ERROR READING : "+e.getMessage());
		}
	}

}
