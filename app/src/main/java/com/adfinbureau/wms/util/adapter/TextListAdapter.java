/*
 * Copyright (C) 2015 Antonio Leiva
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.adfinbureau.wms.util.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.model.LocationData;
import com.adfinbureau.wms.model.Survey;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import rx.functions.Action1;

public class TextListAdapter extends RecyclerView.Adapter<TextListAdapter.ViewHolder> implements View.OnClickListener, Action1<List<LocationData>> {

    private List<LocationData> items;
    private OnItemClickListener onItemClickListener;
    public Context context;

    public TextListAdapter(Context context, List<LocationData> items) {
        this.items = items;
        this.context = context;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.text_list_row, parent, false);
        v.setOnClickListener(this);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        LocationData item = items.get(position);
        holder.masalah.setText(item.masalah());
        holder.usulan.setText(item.usulan());
        holder.status.setText(item.status());

        holder.itemView.setTag(item);
    }

    @Override public int getItemCount() {
        return items.size();
    }

    @Override public void onClick(final View v) {
        onItemClickListener.onItemClick(v, (LocationData) v.getTag());
    }

    @Override
    public void call(List<LocationData> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView masalah;
        public TextView usulan;
        public TextView status;

        public ViewHolder(View itemView) {
            super(itemView);

            masalah = (TextView) itemView.findViewById(R.id.masalah);
            usulan = (TextView) itemView.findViewById(R.id.usulan);
            status = (TextView) itemView.findViewById(R.id.status);
        }
    }

    public interface OnItemClickListener {

        void onItemClick(View view, LocationData locationData);

    }
}
