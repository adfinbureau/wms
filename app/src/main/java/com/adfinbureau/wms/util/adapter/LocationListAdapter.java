/*
 * Copyright (C) 2015 Antonio Leiva
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.adfinbureau.wms.util.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adfinbureau.wms.R;
import com.adfinbureau.wms.model.Location;
import com.adfinbureau.wms.model.LocationData;
import com.adfinbureau.wms.model.LocationProgress;
import com.adfinbureau.wms.model.Survey;
import com.adfinbureau.wms.util.helper.FileSplitter;
import com.bignerdranch.android.multiselector.MultiSelector;
import com.bignerdranch.android.multiselector.SwappingHolder;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Request;
import com.squareup.picasso.RequestHandler;
import com.squareup.sqlbrite.BriteDatabase;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import rx.functions.Action1;

public class LocationListAdapter extends RecyclerView.Adapter<LocationListAdapter.ViewHolder> implements
        Action1<List<LocationData>> {

    private List<LocationData> items;
    public Context context;

    BriteDatabase db;

    private MultiSelector mMultiSelector = new MultiSelector();

    public MultiSelector getmMultiSelector() {
        return mMultiSelector;
    }

    public LocationListAdapter(Context context, List<LocationData> items, BriteDatabase db) {
        this.items = items;
        this.context = context;
        this.db = db;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.location_list_row, parent, false);
        return new ViewHolder(v);
    }

    VideoRequestHandler videoRequestHandler=new VideoRequestHandler();

    public LocationData getItem(int position){
        return items.get(position);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        LocationData item = items.get(position);
        holder.longitude.setText(item.longitude());
        holder.latitude.setText(item.latitude());
        holder.status.setText(item.status());
        holder.type.setText(item.type());

        if(item.type().contentEquals("PICTURE") || item.type().contentEquals("ASBUILD")){
            if(item.type().contentEquals("PICTURE")) {
                Picasso.with(holder.image.getContext()).load(new File(item.picture()))
//                        .config(Bitmap.Config.RGB_565)
//                        .fit().centerCrop()
                        .into(holder.image);
            }
            else if(item.type().contentEquals("ASBUILD")){
                Picasso.with(holder.image.getContext()).load(new File(item.asbuild()))
//                        .config(Bitmap.Config.RGB_565)
//                        .fit().centerCrop()
                        .into(holder.image);
            }

            try {
                if(!item.status().contentEquals("COMPLETE")) {
                    int numberParts = 0;
                    if(item.type().contentEquals("PICTURE")){
                        numberParts = FileSplitter.getNumberParts(item.picture());
                    }
                    else if(item.type().contentEquals("ASBUILD")){
                        numberParts = FileSplitter.getNumberParts(item.asbuild());
                    }
                    String check = "SELECT * FROM "+ LocationProgress.TABLE+" WHERE "+LocationProgress.SERVER_ID+" = '"+item.serverId()+"'";
                    int successUpload = db.query(check).getCount();
                    holder.progress.setText(successUpload + "/" + numberParts);
                }
            } catch (IOException e) {
                holder.progress.setText("PICTURE/ASBUILD ERROR");
                e.printStackTrace();
            } catch (NullPointerException e){
                holder.progress.setText("PICTURE/ASBUILD ERROR");
                e.printStackTrace();
            } catch (Exception e){
                holder.progress.setText("PICTURE/ASBUILD ERROR");
                e.printStackTrace();
            }
            holder.image.setVisibility(View.VISIBLE);
        }
        else if(item.type().contentEquals("VIDEO")){

            try {
                Picasso picassoInstance = new Picasso.Builder(holder.image.getContext())
                        .addRequestHandler(videoRequestHandler)
                        .build();
                picassoInstance.load(videoRequestHandler.SCHEME_VIEDEO+":"+item.video()).into(holder.image);

                if(!item.status().contentEquals("COMPLETE")) {
                    int numberParts = FileSplitter.getNumberParts(item.video());
                    String check = "SELECT * FROM " + LocationProgress.TABLE + " WHERE " + LocationProgress.SERVER_ID + " = '" + item.serverId() + "'";
                    int successUpload = db.query(check).getCount();
                    holder.progress.setText(successUpload + "/" + numberParts);
                }
            } catch (IOException e) {
                holder.progress.setText("VIDEO ERROR");
                e.printStackTrace();
            } catch (NullPointerException e){
                holder.progress.setText("VIDEO ERROR");
                e.printStackTrace();
            } catch (Exception e){
                holder.progress.setText("VIDEO ERROR");
                e.printStackTrace();
            }
            holder.image.setVisibility(View.VISIBLE);
        }
        else{
            holder.image.setVisibility(View.GONE);
            holder.image.setImageBitmap(null);
            holder.progress.setText("");
        }
        holder.itemView.setTag(item);
    }

    public class VideoRequestHandler extends RequestHandler{
        public String SCHEME_VIEDEO="video";
        @Override
        public boolean canHandleRequest(Request data)
        {
            String scheme = data.uri.getScheme();
            return (SCHEME_VIEDEO.equals(scheme));
        }

        @Override
        public Result load(Request data, int arg1) throws IOException
        {
            Bitmap bm = ThumbnailUtils.createVideoThumbnail(data.uri.getPath(), MediaStore.Images.Thumbnails.MINI_KIND);
            return new RequestHandler.Result(bm, Picasso.LoadedFrom.DISK);


        }
    }

    @Override public int getItemCount() {
        return items.size();
    }

    @Override
    public void call(List<LocationData> locationDatas) {
        this.items = locationDatas;
        notifyDataSetChanged();
    }

    public class ViewHolder extends SwappingHolder implements View.OnLongClickListener, View.OnClickListener {
        public TextView longitude;
        public TextView latitude;
        public TextView type;
        public TextView status;
        public ImageView image;
        public TextView progress;

        /*
        private ModalMultiSelectorCallback mActionModeCallback
                = new ModalMultiSelectorCallback(mMultiSelector) {

            @Override
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                super.onCreateActionMode(actionMode, menu);
//                getActivity().getMenuInflater().inflate(R.menu.list_context_menu, menu);
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                if (menuItem.getItemId() == R.id.menu_item_delete) {
                    actionMode.finish();

                    for (int i = mObjects.size(); i >= 0; i--) {
                        if (mMultiSelector.isSelected(i, 0)) { // (1)
                            // remove item from list
                            mRecyclerView.getAdapter().notifyItemRemoved(i);
                        }
                    }

                    mMultiSelector.clearSelections(); // (2)
                    return true;

                }
                return false;
            }
        };
        */

        public ViewHolder(View itemView) {
            super(itemView, mMultiSelector);
            itemView.setLongClickable(true);
            itemView.setOnLongClickListener(this);
            itemView.setOnClickListener(this);

            longitude = (TextView) itemView.findViewById(R.id.longitude);
            latitude = (TextView) itemView.findViewById(R.id.latitude);
            type = (TextView) itemView.findViewById(R.id.type);
            status = (TextView) itemView.findViewById(R.id.status);
            image =  (ImageView)itemView.findViewById(R.id.image);
            progress = (TextView) itemView.findViewById(R.id.progress);
        }

        @Override
        public boolean onLongClick(View v) {
            if (!mMultiSelector.isSelectable()) { // (3)
//                ((AppCompatActivity) getActivity()).startSupportActionMode(mActionModeCallback); // (2)
                mMultiSelector.setSelectable(true); // (4)
                mMultiSelector.setSelected(ViewHolder.this, true); // (5)
                return true;
            }
            return false;
        }

        @Override
        public void onClick(View v) {
            //is not in selection mode
            if (!mMultiSelector.isSelectable()) { // (3)
            }
            else{
                if(mMultiSelector.isSelected(getAdapterPosition(), getItemId())){
                    mMultiSelector.setSelected(ViewHolder.this, false);
                }
                else {
                    mMultiSelector.setSelected(ViewHolder.this, true);
                }
                if(mMultiSelector.getSelectedPositions().size() == 0)
                    mMultiSelector.setSelectable(false); // (4)
            }
//            if (!mMultiSelector.tapSelection(ViewHolder.this)){
                //is not in selection model
//                mMultiSelector.setSelected(ViewHolder.this, true); // (5)
//            }
//            else{
//            }
        }
    }
}
