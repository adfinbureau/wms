package com.adfinbureau.wms.util.bluetooth;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.Socket;
import java.security.InvalidParameterException;
import java.util.Set;
import java.util.UUID;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.Context;

import android.os.Build;
import android.util.Log;
import android.widget.Toast;


public class BlueTooth extends Thread{
	private BluetoothAdapter btAdapter = null;
	private BluetoothSocket btSocket = null;
	private static String address = "";
	private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	 
	private OnLocationReceived handler;
	private Context context;

	private static String LOG_TAG = "BlueTooth";

	public InputStream mInputStream;
	
	NMEAStringReader reader;

	public boolean requested = true;
	public boolean isConnected = true;

	public BlueTooth.BlueToothEvent event;

	public BlueTooth(String address_,Context context, OnLocationReceived handler, BlueToothEvent event) {
		this.address=address_;
		requested = true;
		this.handler = handler;
		this.context = context;
		this.event = event;
		btAdapter = BluetoothAdapter.getDefaultAdapter();

	    BluetoothDevice device = null;
		try {
			Log.i(LOG_TAG, "INFO: " + "CREATE BLUETOOTH SOCKET ADDRESS : "+this.address);
			device = btAdapter.getRemoteDevice(this.address);
			btSocket = createBluetoothSocket(device);
			this.mInputStream = btSocket.getInputStream();
			btAdapter.cancelDiscovery();
			btSocket.connect();
		} catch (IOException e) {
			Log.i(LOG_TAG, "INFO: " + "ERROR CREATE BLUETOOTH SOCKET : "+e.getMessage());
			terminate();
			isConnected = false;
		} catch (IllegalArgumentException ex){
			Log.i(LOG_TAG, "INFO: " + "ILLEGAL ARGUMENT SOCKET "+ex.getMessage());
			isConnected = false;
			terminate();
		}
	}

	public void terminate() {
		try {
			btSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			event.onBluetoothDisconnect();
		}
	}
	
	@Override
	public void run()
	{
		// TODO: Implement this method
		super.run();
		while(requested){
			int size;
			try {
				byte[] buffer = new byte[2000];
				if (mInputStream == null) return;
				size = mInputStream.read(buffer);
				Log.i(LOG_TAG, "READ : "+buffer.toString());
				if (size > 0) {
					Log.i(LOG_TAG, "SIZE > 0 : "+size);
					try {
						reader = new NMEAStringReader(handler);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						Log.i(LOG_TAG, "FAILED TO READ : "+e.getMessage());
						e.printStackTrace();
					}
					reader.read(buffer);
				}
			} catch (IOException e) {
				Log.i(LOG_TAG, "IOEXCEPTION ON BLUETOOTH THREAD : " + e.getMessage());
				e.printStackTrace();
				terminate();
				return;
			}catch (Exception e){
				Log.i(LOG_TAG, "EXCEPTION ON BLUETOOTH THREAD : " + e.getMessage());
				e.printStackTrace();
				terminate();
			}
		}
//		terminate();
	}
	
	public static interface BlueToothEvent{
		public void onBluetoothDisconnect();
	}
	
	private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {
		if(Build.VERSION.SDK_INT >= 10){
			try {
				final Method  m = device.getClass().getMethod("createInsecureRfcommSocketToServiceRecord", new Class[] { UUID.class });
	            return (BluetoothSocket) m.invoke(device, MY_UUID);
	       	} catch (Exception e) {}
		}
	    return  device.createRfcommSocketToServiceRecord(MY_UUID);
	}
}
