package com.adfinbureau.wms.util.socket;

import android.location.Location;

public interface OnLocationReceived {
	public void sendLocation(Location location);
}
