/*
 * Copyright (C) 2015 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.adfinbureau.wms.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.adfinbureau.wms.model.LocationData;
import com.adfinbureau.wms.model.LocationProgress;
import com.adfinbureau.wms.model.Survey;

public final class DbOpenHelper extends SQLiteOpenHelper {
  private static final int VERSION = 1;

  private static final String CREATE_LIST = ""
      + "CREATE TABLE " + TodoList.TABLE + "("
      + TodoList.ID + " INTEGER NOT NULL PRIMARY KEY,"
      + TodoList.NAME + " TEXT NOT NULL,"
      + TodoList.ARCHIVED + " INTEGER NOT NULL DEFAULT 0"
      + ")";

  private static final String CREATE_ITEM = ""
      + "CREATE TABLE " + TodoItem.TABLE + "("
      + TodoItem.ID + " INTEGER NOT NULL PRIMARY KEY,"
      + TodoItem.LIST_ID + " INTEGER NOT NULL REFERENCES " + TodoList.TABLE + "(" + TodoList.ID + "),"
      + TodoItem.DESCRIPTION + " TEXT NOT NULL,"
      + TodoItem.COMPLETE + " INTEGER NOT NULL DEFAULT 0"
      + ")";

    private static final String CREATE_LOCATION_PROGRESS = ""
            + "CREATE TABLE " + LocationProgress.TABLE + "("
            + LocationProgress.ID + " INTEGER NOT NULL PRIMARY KEY,"
            + LocationProgress.SERVER_ID + " TEXT,"
            + LocationProgress.INDEX + " TEXT"
            + ")";

    private static final String CREATE_LOCATION = ""
        + "CREATE TABLE " + LocationData.TABLE + "("
        + LocationData.ID + " INTEGER NOT NULL PRIMARY KEY,"
        + LocationData.SERVER_ID + " TEXT,"
        + LocationData.LATITUDE + " TEXT,"
        + LocationData.LONGITUDE + " TEXT,"
        + LocationData.ALTITUDE + " TEXT,"
        + LocationData.PICTURE + " TEXT,"
        + LocationData.STATUS + " TEXT,"
        + LocationData.VIDEO + " TEXT,"
        + LocationData.TEXT + " TEXT,"
        + LocationData.DESCRIPTION + " TEXT,"
        + LocationData.SURVEY_ID+ " TEXT, "
            + LocationData.MASALAH+ " TEXT, "
            + LocationData.USULAN+ " TEXT, "
            + LocationData.TYPE+ " TEXT, "
        + LocationData.AS_BUILD + " TEXT"
        + ")";

    private static final String CREATE_SURVEY = ""
            + "CREATE TABLE " + Survey.TABLE + "("
            + Survey.ID + " INTEGER NOT NULL PRIMARY KEY, "
            + Survey.DATA + " TEXT, "
            + Survey.CREATED_DATE + " DATETIME DEFAULT (DATETIME(CURRENT_TIMESTAMP, 'LOCALTIME'))"
            + ")";
    private static final String CREATE_ITEM_LIST_ID_INDEX =
      "CREATE INDEX item_list_id ON " + TodoItem.TABLE + " (" + TodoItem.LIST_ID + ")";

    private static final String CREATE_LOCATION_DATA_LIST_ID_INDEX =
            "CREATE INDEX location_data_list_id ON " + LocationData.TABLE + " (" + Survey.ID + ")";

    public DbOpenHelper(Context context) {
    super(context, "todo.db", null /* factory */, VERSION);
  }

  @Override public void onCreate(SQLiteDatabase db) {
    db.execSQL(CREATE_LIST);
    db.execSQL(CREATE_ITEM);
    db.execSQL(CREATE_LOCATION);
    db.execSQL(CREATE_LOCATION_PROGRESS);
    db.execSQL(CREATE_SURVEY);
    db.execSQL(CREATE_ITEM_LIST_ID_INDEX);
    db.execSQL(CREATE_LOCATION_DATA_LIST_ID_INDEX);

    long groceryListId = db.insert(TodoList.TABLE, null, new TodoList.Builder()
        .name("Grocery List")
        .build());
    db.insert(TodoItem.TABLE, null, new TodoItem.Builder()
        .listId(groceryListId)
        .description("Beer")
        .build());
    db.insert(TodoItem.TABLE, null, new TodoItem.Builder()
        .listId(groceryListId)
        .description("Point Break on DVD")
        .build());
    db.insert(TodoItem.TABLE, null, new TodoItem.Builder()
        .listId(groceryListId)
        .description("Bad Boys 2 on DVD")
        .build());

    long holidayPresentsListId = db.insert(TodoList.TABLE, null, new TodoList.Builder()
        .name("Holiday Presents")
        .build());
    db.insert(TodoItem.TABLE, null, new TodoItem.Builder()
        .listId(holidayPresentsListId)
        .description("Pogo Stick for Jake W.")
        .build());
    db.insert(TodoItem.TABLE, null, new TodoItem.Builder()
        .listId(holidayPresentsListId)
        .description("Jack-in-the-box for Alec S.")
        .build());
    db.insert(TodoItem.TABLE, null, new TodoItem.Builder()
        .listId(holidayPresentsListId)
        .description("Pogs for Matt P.")
        .build());
    db.insert(TodoItem.TABLE, null, new TodoItem.Builder()
        .listId(holidayPresentsListId)
        .description("Coal for Jesse W.")
        .build());

    long workListId = db.insert(TodoList.TABLE, null, new TodoList.Builder()
        .name("Work Items")
        .build());
    db.insert(TodoItem.TABLE, null, new TodoItem.Builder()
        .listId(workListId)
        .description("Finish SqlBrite library")
        .complete(true)
        .build());
    db.insert(TodoItem.TABLE, null, new TodoItem.Builder()
        .listId(workListId)
        .description("Finish SqlBrite sample app")
        .build());
    db.insert(TodoItem.TABLE, null, new TodoItem.Builder()
        .listId(workListId)
        .description("Publish SqlBrite to GitHub")
        .build());

    long birthdayPresentsListId = db.insert(TodoList.TABLE, null, new TodoList.Builder()
        .name("Birthday Presents")
        .archived(true)
        .build());
    db.insert(TodoItem.TABLE, null, new TodoItem.Builder().listId(birthdayPresentsListId)
        .description("New car")
        .complete(true)
        .build());
  }

  @Override public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
  }
}
