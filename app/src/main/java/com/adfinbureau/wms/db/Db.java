/*
 * Copyright (C) 2015 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.adfinbureau.wms.db;

import android.database.Cursor;
import android.text.format.DateFormat;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public final class Db {
  public static final int BOOLEAN_FALSE = 0;
  public static final int BOOLEAN_TRUE = 1;

  public static String getDate(Cursor cursor, String columnName) {
    if(cursor.getString(cursor.getColumnIndexOrThrow(columnName)) != null) {
      Calendar cal = Calendar.getInstance(Locale.ENGLISH);
      cal.setTimeInMillis(Long.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(columnName))));
      String date = DateFormat.format("dd-MM-yyyy", cal).toString();
      return date;
    }else{
      return "";
    }
  }

  public static String getString(Cursor cursor, String columnName) {
    if(cursor.getString(cursor.getColumnIndexOrThrow(columnName)) != null) {
      //Log.d("MAPPER DATA "+columnName, cursor.getString(cursor.getColumnIndexOrThrow(columnName)));
      return cursor.getString(cursor.getColumnIndexOrThrow(columnName));
    }else{
      //Log.d("MAPPER DATA "+columnName, "NULL ");
      return "";
    }
  }

  public static boolean getBoolean(Cursor cursor, String columnName) {
    return getInt(cursor, columnName) == BOOLEAN_TRUE;
  }

  public static long getLong(Cursor cursor, String columnName) {
    return cursor.getLong(cursor.getColumnIndexOrThrow(columnName));
  }

  public static int getInt(Cursor cursor, String columnName) {
    return cursor.getInt(cursor.getColumnIndexOrThrow(columnName));
  }

  private Db() {
    throw new AssertionError("No instances.");
  }

  public static Map<String, Object> getMap(Cursor cursor, String columnName) {
    Map<String, Object> retMap = new HashMap<String, Object>();
    if(cursor.getString(cursor.getColumnIndexOrThrow(columnName)) != null) {
      //Log.d("MAPPER DATA "+columnName, cursor.getString(cursor.getColumnIndexOrThrow(columnName)));
      retMap = new Gson().fromJson(cursor.getString(cursor.getColumnIndexOrThrow(columnName)), new TypeToken<HashMap<String, Object>>() {}.getType());
    }
    return retMap;
  }
}
