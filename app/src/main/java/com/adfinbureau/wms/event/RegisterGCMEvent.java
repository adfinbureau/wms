package com.adfinbureau.wms.event;

import com.adfinbureau.wms.model.UserWMS;

/**
 * Created by yudiharto sasongko on 6/14/2016.
 */
public class RegisterGCMEvent {
    public final UserWMS userWMS;

    public RegisterGCMEvent(UserWMS userWMS){
        this.userWMS = userWMS;
    }

    public UserWMS getUserWMS() {
        return userWMS;
    }
}
