package com.adfinbureau.wms.activity;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(injects = {}, library = true)
public class ActivityResultModule {
    @Provides
    @Singleton ActivityResultRegistrar provideIntentLauncher(ActivityResultOwner presenter) {
        return presenter;
    }
}
