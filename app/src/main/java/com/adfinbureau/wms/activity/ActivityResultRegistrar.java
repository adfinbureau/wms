package com.adfinbureau.wms.activity;

import android.content.Intent;

import mortar.MortarScope;

public interface ActivityResultRegistrar {

    void register(MortarScope scope, ActivityResultOwner.ActivityResultListener listener);

    void startActivityForResult(int requestCode, Intent intent);

    void startActivity(Intent intent);
}
