package com.adfinbureau.wms;

public final class SharedPreferencesKeys {

	public static final String ENVIRONMENT = "environment";
	public static final String USER_ACCOUNT = "userAccount";

	public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
	public static final String REGISTRATION_COMPLETE = "registrationComplete";

	public static final String BLUETOOTH_GPS = "bluetoothGps";
	public static final String BLUETOOTH_GPS_NAME = "bluetoothNameGps";
	public static final String LOCATION_SOURCE = "GPS";

	public static final String AUTOUPLOAD = "autoupload";

	public static final String SETTING_PICTURE_QUALITY = "pictureQuality";


	public static final String SETTING_IMAGE_LOCATION = "imageLocation";
	public static final String SETTING_ASBUILD_LOCATION = "asbuildLocation";
	public static final String SETTING_VIDEO_LOCATION = "videoLocation";
	private SharedPreferencesKeys() {
		throw new UnsupportedOperationException("not supported");
	}
}
