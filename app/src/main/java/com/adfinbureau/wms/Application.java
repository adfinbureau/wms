package com.adfinbureau.wms;

import android.content.Context;
import android.os.StrictMode;
import android.support.multidex.MultiDex;

import com.adfinbureau.wms.BuildConfig;

import dagger.ObjectGraph;
import mortar.Mortar;
import mortar.MortarScope;
import com.adfinbureau.wms.util.logging.Logger;
import com.orm.SugarContext;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.annotation.ReportsCrashes;

@ReportsCrashes(
		formUri = "http://103.4.164.221:8089/api/android/logs",
//		mailTo = "billyfebram@gmail.com",
		reportType = org.acra.sender.HttpSender.Type.JSON,
		customReportContent = {
				ReportField.APP_VERSION_CODE,
				ReportField.APP_VERSION_NAME,
				ReportField.ANDROID_VERSION,
				ReportField.PACKAGE_NAME,
				ReportField.REPORT_ID,
				ReportField.BUILD,
				ReportField.STACK_TRACE,
				ReportField.CUSTOM_DATA
		}
)
public class Application extends android.app.Application {
	private static final Logger LOG = Logger.getLogger(Application.class);

	protected MortarScope rootScope;
	ObjectGraph objectGraph;

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		MultiDex.install(this);

		// The following line triggers the initialization of ACRA
		ACRA.init(this);
	}

	@Override
	public void onCreate() {
		super.onCreate();

		LOG.info("Starting application");

		LOG.debug("Initialising application object graph...");
		objectGraph = ObjectGraph.create(new ApplicationModule(this));

		// Eagerly validate development builds (too slow for production).
		LOG.debug("Creating root Mortar scope...");
		rootScope = Mortar.createRootScope(BuildConfig.DEBUG, objectGraph);
		SugarContext.init(this);

	}

	public void inject(Object object){
		objectGraph.inject(object);
	}

	@Override
	public Object getSystemService(String name) {
		if (Mortar.isScopeSystemService(name)) {
			return rootScope;
		}
		return super.getSystemService(name);
	}
}
