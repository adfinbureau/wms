package com.adfinbureau.wms.service;

import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcel;
import android.support.annotation.Nullable;
import android.util.Log;

import com.adfinbureau.wms.Application;
import com.adfinbureau.wms.SharedPreferencesKeys;
import com.adfinbureau.wms.db.Db;
import com.adfinbureau.wms.model.LocationData;
import com.adfinbureau.wms.model.UserWMS;
import com.adfinbureau.wms.repository.JsonSharedPreferencesRepository;
import com.adfinbureau.wms.util.DeviceName;
import com.google.gson.JsonObject;
import com.squareup.sqlbrite.BriteDatabase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;
import rx.Subscription;

/**
 * Created by yudiharto sasongko on 5/10/2016.
 */
public class UpdateDataService extends Service {
    @Inject
    BriteDatabase db;
    @Inject
    LocationService locationService;
    private Subscription subscription;

    @Inject
    SQLiteOpenHelper sqLiteDatabase;

    Runnable mRunnable;

    @Inject
    JsonSharedPreferencesRepository sharedPreferencesRepository;

    private String username = "";

    @Override
    public void onCreate() {
        super.onCreate();
        ((Application)getApplication()).inject(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final Handler mHandler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {
                UserWMS userWMS = sharedPreferencesRepository.getObject(SharedPreferencesKeys.USER_ACCOUNT, UserWMS.class);
                if(userWMS != null) {
                    username = userWMS.getUsername();
                }

//                Log.i("SERVICE", "Every 10 seconds");
                Cursor cursor = db.query(LocationData.QUERY_UPDATE);
                Log.d("JUMLAH DATA", "DATA : " + cursor.getCount());
                List<LocationData> locations = new ArrayList<LocationData>();
                try {
                    while (cursor.moveToNext()) {
                        Log.d("READ CURSOR", "READ CURSOR DATA");

                        final long id = Db.getLong(cursor, LocationData.ID);
                        final String serverId = Db.getString(cursor, LocationData.SERVER_ID);
                        final String longitude = Db.getString(cursor, LocationData.LONGITUDE);
                        final String latitude = Db.getString(cursor, LocationData.LATITUDE);
                        final String altitude = Db.getString(cursor, LocationData.ALTITUDE);
                        final String picture = Db.getString(cursor, LocationData.PICTURE);
                        final String video = Db.getString(cursor, LocationData.VIDEO);
                        final String asbuild = Db.getString(cursor, LocationData.AS_BUILD);
                        final String text = Db.getString(cursor, LocationData.TEXT);
                        final String status = Db.getString(cursor, LocationData.STATUS);
                        final String description = Db.getString(cursor, LocationData.DESCRIPTION);
                        final String survey = Db.getString(cursor, LocationData.SURVEY_ID);
                        final String masalah = Db.getString(cursor, LocationData.MASALAH);
                        final String usulan = Db.getString(cursor, LocationData.USULAN);
                        final String type = Db.getString(cursor, LocationData.TYPE);

                        LocationData locationData = new LocationData() {
                            @Override
                            public long id() {
                                return id;
                            }

                            @Override
                            public String serverId() {
                                return serverId;
                            }

                            @Override
                            public String longitude() {
                                return longitude;
                            }

                            @Override
                            public String latitude() {
                                return latitude;
                            }

                            @Override
                            public String altitude(){ return altitude; }

                            @Override
                            public String picture() {
                                return picture;
                            }

                            @Override
                            public String video() {
                                return video;
                            }

                            @Override
                            public String asbuild() {
                                return asbuild;
                            }

                            @Override
                            public String text() {
                                return text;
                            }

                            @Override
                            public String status() {
                                return status;
                            }

                            @Override
                            public String description() {
                                return description;
                            }

                            @Override
                            public String survey() {
                                return survey;
                            }

                            @Override
                            public int describeContents() {
                                return 0;
                            }

                            @Override
                            public String masalah() {
                                return masalah;
                            }

                            @Override
                            public String usulan() {
                                return usulan;
                            }

                            @Override
                            public String type() {
                                return type;
                            }

                            @Override
                            public void writeToParcel(Parcel dest, int flags) {

                            }
                        };
                        locations.add(locationData);
                    }
                } finally {
                    cursor.close();
                }

                Log.d("RESPONSE", "SEND PICTURE " + locations.size());

                for (final LocationData x : locations) {
                    final com.adfinbureau.wms.model.Location location = new com.adfinbureau.wms.model.Location();
                    location.setDescription(x.description());
                    location.setId(x.serverId());
                    location.setUsername(username);
                    location.setMasalah(x.masalah());
                    location.setUsulan(x.usulan());
                    location.setDeviceName(DeviceName.getDeviceName());
                    location.setVersion("1");
                    Log.d("UPDATE DATA", "UPDATE DATA DESCRIPTION " + x.description());
                    Log.d("UPDATE DATA", "UPDATE DATA MASALAH USULAN " + x.masalah()+" | "+x.usulan());

                    new AsyncTask<String, String, Response<JsonObject>>() {
                        @Override
                        protected Response<JsonObject> doInBackground(String... params) {
                            try {
                                return locationService.editLocation(location).execute();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Response<JsonObject> jsonObjectResponse) {
                            if(jsonObjectResponse != null) {
                                if (jsonObjectResponse.isSuccessful()) {
                                    sqLiteDatabase.getWritableDatabase().update(LocationData.TABLE,
                                            new LocationData.Builder()
                                                    .status("COMPLETE")
                                                    .build(), LocationData.SERVER_ID + " = ?", new String[]{String.valueOf(x.serverId())});
                                } else {
                                    // error response, no access to resource?
                                    Log.d("RESPONSE", "ERROR");
                                }
                            }else{
                                //no internet connection
                                Log.d("RESPONSE", "ERROR INTERNET CONNECTION");
                            }
                        }
                    }.execute();
                }

                Log.d("RESPONSE", "POST DELAYED");
                mHandler.postDelayed(mRunnable, 60 * 1000);
            }
        };
        mHandler.postDelayed(mRunnable, 10 * 1000);
        return START_STICKY;
    }
}
