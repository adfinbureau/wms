package com.adfinbureau.wms.service;

import com.adfinbureau.wms.model.Location;
import com.adfinbureau.wms.model.PasswordAuthentication;
import com.adfinbureau.wms.model.User;
import com.adfinbureau.wms.model.UserToken;
import com.adfinbureau.wms.model.UserWithPassword;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * A sample interface to an API that might exist on your server.
 */
public interface UserService {

  /**
   * Registers a new user account.
   */
  @POST("/user")
  Observable<User> register(@Body UserWithPassword user);

  /**
   * Perform a user login.
   */
  @POST("/user/login")
  Observable<UserToken> login(@Body PasswordAuthentication authentication);

  /**
   * Perform a user logout.
   */
  @POST("user/logout")
  Observable<Void> logout(@Header("Authorization") UserToken token);

  @GET("users/oracle/{login}/{password}")
  Call<Void> getUser (@Path("login") String username, @Path("password") String password);

}
