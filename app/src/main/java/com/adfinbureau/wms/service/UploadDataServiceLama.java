package com.adfinbureau.wms.service;

import android.app.IntentService;
import android.content.Intent;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.adfinbureau.wms.Application;
import com.adfinbureau.wms.SharedPreferencesKeys;
import com.adfinbureau.wms.model.LocationData;
import com.adfinbureau.wms.model.UserWMS;
import com.adfinbureau.wms.repository.JsonSharedPreferencesRepository;
import com.adfinbureau.wms.util.DeviceName;
import com.adfinbureau.wms.util.helper.FileSplitter;
import com.adfinbureau.wms.util.requesthandler.CallUtils;
import com.google.gson.JsonObject;
import com.squareup.sqlbrite.BriteDatabase;

import org.acra.ACRA;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by yudiharto sasongko on 5/10/2016.
 */
public class UploadDataServiceLama extends IntentService {
    @Inject
    BriteDatabase db;
    @Inject
    SQLiteOpenHelper sqLiteDatabase;

    @Inject
    LocationService locationService;

    @Inject
    JsonSharedPreferencesRepository sharedPreferencesRepository;

    private Subscription subscription;
    private String username = "";

    private static final String TAG = "UploadData";

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public UploadDataServiceLama() {
        super("UploadDataService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ((Application)getApplication()).inject(this);

    }

    @Override
    protected void onHandleIntent(Intent intent) {
        subscription = db.createQuery(LocationData.TABLE,
        LocationData.QUERY_PENDING)
        .mapToList(LocationData.MAPPER)
        .subscribeOn(Schedulers.newThread())
//      .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Action1<List<LocationData>>() {
            @Override
            public void call(final List<LocationData> locationListses) {
                Log.d(TAG, "Upload LOCATION LIST " + locationListses.size());
                UserWMS userWMS = sharedPreferencesRepository.getObject(SharedPreferencesKeys.USER_ACCOUNT, UserWMS.class);
                if(userWMS != null) {
                    username = userWMS.getUsername();
                }
                for (final LocationData x : locationListses) {
                    final com.adfinbureau.wms.model.Location location = new com.adfinbureau.wms.model.Location();
                    location.setType(x.type());
                    location.setLongitude(x.longitude());
                    location.setLatitude(x.latitude());
                    location.setPictureContentType("image/jpeg");
                    location.setVideoContentType("video/mp4");
                    location.setAsbuildContentType("image/jpeg");
                    location.setText(x.text());
                    location.setStatus(x.status());
                    location.setDescription(x.description());
                    location.setUsername(username);
                    location.setDeviceName("Device : "+DeviceName.getDeviceName());
                    location.setVersion("1");

                    if(!x.masalah().contentEquals("")){
                        location.setMasalah(x.masalah());
                        location.setUsulan(x.usulan());
                    }

                    if (!x.picture().contentEquals("")) {
                        int size = 0;
                        try {
                            size = FileSplitter.split(x.picture(), "image");
                            location.setPictureSpliceCount(size);
                        } catch (IOException e) {
                            e.printStackTrace();
                            ACRA.getErrorReporter().handleException(e);
                        }finally {
                            if(size == 0){
                                return;
                            }
                        }
                        Log.d(TAG, "PICTURE NOT NULL SPLIT " + x.picture());
                    } else {
                        Log.d(TAG, "PICTURE NULL");
                    }

                    if (!x.video().contentEquals("")) {
                        int size = 0;
                        try {
                            size = FileSplitter.split(x.video(),"video");
                            location.setVideoSpliceCount(size);
                        } catch (IOException e) {
                            e.printStackTrace();
                            ACRA.getErrorReporter().handleException(e);
                        }finally {
                            if(size == 0){
                                return;
                            }
                        }
                        Log.d(TAG, "VIDEO NOT NULL SPLIT " + x.video());
                    } else {
                        Log.d(TAG, "VIDEO NULL");
                    }

                    if (!x.asbuild().contentEquals("")) {
                        int size = 0;
                        try {
                            size = FileSplitter.split(x.asbuild(), "image");
                            location.setAsbuildSpliceCount(size);
                        } catch (IOException e) {
                            e.printStackTrace();
                            ACRA.getErrorReporter().handleException(e);
                        }finally {
                            if(size == 0){
                                return;
                            }
                        }
                        Log.d(TAG, "ASBUILD NOT NULL SPLIT " + x.asbuild());
                    } else {
                        Log.d(TAG, "ASBUILD NULL");
                    }

                    CallUtils.enqueueWithRetry(
                            locationService.addLocation(location), new Callback<JsonObject>() {
                                @Override
                                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                    Log.d(TAG, "LOCATION ADDED");
                                    Log.d(TAG, "LOCATION PICTURE " + x.picture());

                                    if (!x.picture().contentEquals("") || !x.video().contentEquals("") || !x.asbuild().contentEquals("")) {
                                        sqLiteDatabase.getWritableDatabase().update(LocationData.TABLE,
                                                new LocationData.Builder()
                                                        .status("PROGRESS")
                                                        .serverId(response.body().get("id").getAsString())
                                                        .build(), LocationData.ID + " = ?", new String[]{String.valueOf(x.id())});
//                                        db.update(LocationData.TABLE,
//                                                new LocationData.Builder()
//                                                        .status("PROGRESS")
//                                                        .serverId(response.body().get("id").getAsString())
//                                                        .build(), LocationData.ID + " = ?", new String[]{String.valueOf(x.id())});
//                                        db.execute("UPDATE "+LocationData.TABLE+" SET "+LocationData.SERVER_ID+" = '"+response.body().get("id").getAsString()+"' AND "+LocationData.STATUS+" = 'PROGRESS' " +
//                                                "where "+LocationData.ID+" = '"+x.id()+"'");
                                    } else {
                                        sqLiteDatabase.getWritableDatabase().update(LocationData.TABLE,
                                                new LocationData.Builder()
                                                        .status("COMPLETE")
                                                        .serverId(response.body().get("id").getAsString())
                                                        .build(), LocationData.ID + " = ?", new String[]{String.valueOf(x.id())});
//                                        db.update(LocationData.TABLE,
//                                                new LocationData.Builder()
//                                                        .status("COMPLETE")
//                                                        .serverId(response.body().get("id").getAsString())
//                                                        .build(), LocationData.ID + " = ?", new String[]{String.valueOf(x.id())});
//                                        db.execute("UPDATE "+LocationData.TABLE+" SET "+LocationData.SERVER_ID+" = '"+response.body().get("id").getAsString()+"' AND "+LocationData.STATUS+" = 'COMPLETE' " +
//                                                "where "+LocationData.ID+" = '"+x.id()+"'");
                                    }
                                }

                                @Override
                                public void onFailure(Call<JsonObject> call, Throwable t) {
                                }
                            }
                    );
                }
            }
        });
    }
}
