package com.adfinbureau.wms.service;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * The dagger module associated with {@link InjectingService}.
 */
@Module(library = true)
public class InjectingServiceModule {
    private android.app.Service mService;

    InjectingServiceModule(){}
    /**
     * Class constructor.
     *
     * @param service the Service with which this module is associated.
     */
    public InjectingServiceModule(android.app.Service service) {
        mService = service;
    }

    @Provides
    @Singleton
    public android.app.Service provideService() {
        return mService;
    }

}

