package com.adfinbureau.wms.service;

import com.adfinbureau.wms.model.Location;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;

/**
 * A sample interface to an API that might exist on your server.
 */
public interface LocationService {

  @POST("locations")
  Call<JsonObject> addLocation (@Body Location location);

  @PUT("locations")
  Call<JsonObject> editLocation(@Body Location location);
}
