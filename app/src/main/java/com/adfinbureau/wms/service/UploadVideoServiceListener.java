package com.adfinbureau.wms.service;

import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcel;
import android.support.annotation.Nullable;
import android.util.Log;

import com.adfinbureau.wms.Application;
import com.adfinbureau.wms.db.Db;
import com.adfinbureau.wms.model.Location;
import com.adfinbureau.wms.model.LocationData;
import com.adfinbureau.wms.model.LocationProgress;
import com.adfinbureau.wms.util.helper.FileSplitter;
import com.google.gson.JsonObject;
import com.squareup.sqlbrite.BriteDatabase;

import org.acra.ACRA;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import retrofit2.Response;

/**
 * Created by yudiharto sasongko on 5/10/2016.
 */
public class UploadVideoServiceListener extends Service {
    @Inject
    BriteDatabase db;
    @Inject
    LocationService locationService;
    @Inject
    SQLiteOpenHelper sqLiteDatabase;

    private static final String TAG = "UploadVideo";
    @Override
    public void onCreate() {
        super.onCreate();
        ((Application)getApplication()).inject(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    Runnable mRunnable;
    Handler mHandler;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Received start id " + startId + ": " + intent);

        mHandler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {
                /*String queryCheckNode = "SELECT * FROM "+LocationData.TABLE+" WHERE "+LocationData.STATUS+" = 'PENDING' AND "+LocationData.SERVER_ID+" is null AND (("+LocationData.TYPE+" = 'NODE') OR ("+LocationData.TYPE+" = 'PICTURE') OR ("+LocationData.TYPE+" = 'ASBUILD'))";
                Cursor cursorNode = db.query(queryCheckNode);
                if(cursorNode.getCount() > 0){
//                    Log.d(TAG, "Masih ada node/gambar/asbuild yang belum terkirim, menunggu selesai terkirim terlebih dahulu");
                }
                else{

                }*/

                Cursor cursor = db.query(LocationData.QUERY_PROGRESS_VIDEO);
                Log.d(TAG, "JUMLAH DATA VIDEO : " + cursor.getCount());

                if(cursor.getCount() > 0) {
                    List<LocationData> locations = new ArrayList<LocationData>();
                    try {
                        while (cursor.moveToNext()) {
                            final long id = Db.getLong(cursor, LocationData.ID);
                            final String serverId = Db.getString(cursor, LocationData.SERVER_ID);
                            final String longitude = Db.getString(cursor, LocationData.LONGITUDE);
                            final String latitude = Db.getString(cursor, LocationData.LATITUDE);
                            final String altitude = Db.getString(cursor, LocationData.ALTITUDE);
                            final String picture = Db.getString(cursor, LocationData.VIDEO);
                            final String video = Db.getString(cursor, LocationData.VIDEO);
                            final String asbuild = Db.getString(cursor, LocationData.AS_BUILD);
                            final String text = Db.getString(cursor, LocationData.TEXT);
                            final String status = Db.getString(cursor, LocationData.STATUS);
                            final String description = Db.getString(cursor, LocationData.DESCRIPTION);
                            final String survey = Db.getString(cursor, LocationData.SURVEY_ID);

                            final String masalah = Db.getString(cursor, LocationData.MASALAH);
                            final String usulan = Db.getString(cursor, LocationData.USULAN);
                            final String type = Db.getString(cursor, LocationData.TYPE);

                            LocationData locationData = new LocationData() {
                                @Override
                                public long id() {
                                    return id;
                                }

                                @Override
                                public String serverId() {
                                    return serverId;
                                }

                                @Override
                                public String longitude() {
                                    return longitude;
                                }

                                @Override
                                public String latitude() {
                                    return latitude;
                                }

                                @Override
                                public String altitude(){ return altitude; }

                                @Override
                                public String picture() {
                                    return picture;
                                }

                                @Override
                                public String video() {
                                    return video;
                                }

                                @Override
                                public String asbuild() {
                                    return asbuild;
                                }

                                @Override
                                public String text() {
                                    return text;
                                }

                                @Override
                                public String status() {
                                    return status;
                                }

                                @Override
                                public String description() {
                                    return description;
                                }

                                @Override
                                public String survey() {
                                    return survey;
                                }

                                @Override
                                public String masalah() {
                                    return masalah;
                                }

                                @Override
                                public String usulan() {
                                    return usulan;
                                }

                                @Override
                                public String type() {
                                    return type;
                                }

                                @Override
                                public int describeContents() {
                                    return 0;
                                }

                                @Override
                                public void writeToParcel(Parcel dest, int flags) {

                                }
                            };
                            locations.add(locationData);
                        }
                    } finally {
                        cursor.close();
                    }

                    for (final LocationData x : locations) {
                        if (!x.video().contentEquals("")) {
                            Log.d(TAG, "VIDEO ADA DILOKASI (LONG/LAT): " + x.longitude() + " | " + x.latitude());
                            try {
                                Log.d(TAG, "MEMULAI MENGIRIM POTONGAN VIDEO " + x.video());
                                sendSplice(x.video(), x.serverId());
                            } catch (IOException e) {
                                Log.d(TAG, "GAGAL MENGIRIM POTONGAN VIDEO");
                                e.printStackTrace();
                                ACRA.getErrorReporter().handleSilentException(e);
                            }
                        } else {
                            Log.d(TAG, "VIDEO " + x.video() + " TIDAK DITEMUKAN, HARAP CEK KEMBALI VIDEO ANDA");
                        }
                    }
                }else{
                    Log.d(TAG, "TIDAK ADA DATA VIDEO UNTUK DIUPLOAD, MENUNGGU 1 MENIT KEMUDIAN");
                    sendDelayed(60);
//                    mHandler.postDelayed(mRunnable, 60 * 1000);
                }
            }
        };
        mHandler.post(mRunnable);
        return START_NOT_STICKY;
    }

    public byte[] extractBytes (String ImageName)throws IOException {
            Log.d(TAG, "MENGAMBIL BYTE DARI VIDEO " + ImageName);
        FileInputStream fileInputStream=null;

        File file = new File(ImageName);

        byte[] bFile = new byte[(int) file.length()];

        try {
            //convert file into array of bytes
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bFile);
            fileInputStream.close();
        }catch(Exception e){
            Log.d(TAG, "ERROR KETIKA MENGAMBIL BYTE DARI VIDEO "+ImageName+" | KARENA : "+e.getMessage());
            e.printStackTrace();
            ACRA.getErrorReporter().handleSilentException(e);
        }
        return bFile;
    }

    public void sendSplice(String baseFilename, String id) throws IOException {
        Log.d(TAG, "MENGIRIM POTONGAN VIDEO : "+baseFilename);
        int numberParts = FileSplitter.getNumberParts(baseFilename);
        try {
            String responseId = id;
            boolean isSending = false;
            for (int part = 0; part < numberParts; part++){
                String check = "SELECT * FROM "+LocationProgress.TABLE+" WHERE "+LocationProgress.SERVER_ID+" = '"+responseId+"' AND "+LocationProgress.INDEX+" = '"+part+"'";
                String check2 = "SELECT * FROM "+LocationData.TABLE+" WHERE "+LocationData.SERVER_ID+" = '"+responseId+"' AND "+LocationData.STATUS+" = 'COMPLETE'";
                if(db.query(check).getCount() == 0 && db.query(check2).getCount() == 0) {
                    byte[] splice = extractBytes(baseFilename + "." + part);
                    Log.d(TAG, "MENGIRIM PECAHAN VIDEO " + baseFilename + " BAGIAN KE - " + part);
                    isSending = true;
                    sendPOST(baseFilename, part, splice, responseId);
                    break;
                }
                else{
                    Log.d(TAG, "VIDEO " + baseFilename + " BAGIAN KE - " + part + "/" + numberParts + " SUDAH BERHASIL TERKIRIM");
//                    mHandler.post(mRunnable);
                }
            }

            if(!isSending){
                Log.d(TAG, "TIDAK ADA DATA PART VIDEO UNTUK DIUPLOAD, MENUNGGU 1 MENIT KEMUDIAN");
                sendDelayed(60);
//                mHandler.postDelayed(mRunnable, 60 * 1000);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.d(TAG, "VIDEO ERROR SEND POST");
            e.printStackTrace();
            ACRA.getErrorReporter().handleSilentException(e);
        }
    }

    private void sendPOST(final String basefilename, final int index, final byte[] splice, final String responseId) {
        final Location location = new Location();
        location.setId(responseId);
        location.setVideoSplice(splice);
        location.setVideoSpliceIndex(index);

        new AsyncTask<String, String, Response<JsonObject>>() {
            @Override
            protected Response<JsonObject> doInBackground(String... params) {
                Log.d(TAG, "VIDEO "+basefilename+" BAGIAN KE - "+index+" SEDANG DIKIRIM");

                try {
                    return locationService.editLocation(location).execute();
                }
                catch(OutOfMemoryError e){
                    Log.d(TAG, "MEMORY ERROR "+e.getMessage());
                    e.printStackTrace();
                    ACRA.getErrorReporter().handleSilentException(e);
                }
                catch (IOException e) {
                    Log.d(TAG, "VIDEO "+basefilename+" BAGIAN KE - "+index+" GAGAL TERKIRIM KARENA : "+e.getMessage()+" | "+e.getLocalizedMessage());
                    e.printStackTrace();
                    ACRA.getErrorReporter().handleSilentException(e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Response<JsonObject> jsonObjectResponse) {
                if(jsonObjectResponse != null) {
                    if (jsonObjectResponse.isSuccessful()) {
                        Log.d(TAG, "VIDEO "+basefilename+" BAGIAN KE - "+index+" SUKSES TERKIRIM");

                        sqLiteDatabase.getWritableDatabase().insert(LocationProgress.TABLE, "", new LocationProgress.Builder()
                                .index(index + "")
                                .serverId(responseId)
                                .build());

                        String checkAll = "SELECT * FROM " + LocationProgress.TABLE + " WHERE " + LocationProgress.SERVER_ID + " = '" + responseId + "'";
                        try {
                            int parts = db.query(checkAll).getCount();
                            int numberParts = FileSplitter.getNumberParts(basefilename);

                            if (parts == numberParts) {
                                Log.d(TAG, "SEMUA BAGIAN VIDEO SUDAH DIKIRIM");
                                sqLiteDatabase.getWritableDatabase().update(LocationData.TABLE,
                                        new LocationData.Builder()
                                                .status("COMPLETE")
                                                .build(), LocationData.SERVER_ID + " = ?", new String[]{String.valueOf(responseId)});
                                    sqLiteDatabase.getWritableDatabase().delete(LocationProgress.TABLE, LocationProgress.SERVER_ID + " = ?", new String[]{String.valueOf(responseId)});
                                FileSplitter.deleteParts(basefilename);
                            }
                            else{
                                Log.d(TAG, "VIDEO BELUM TERKIRIM SEMUA : "+parts+"/"+numberParts);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            ACRA.getErrorReporter().handleSilentException(e);
                            Log.d(TAG, "TERJADI IOEXCEPTION : " + e.getMessage());
//                            mHandler.postDelayed(mRunnable, 60 * 1000);
                        } finally{
                            send();
//                            mHandler.post(mRunnable);
                        }
                    } else {
                        // error response, no access to resource?
//                        Log.d(TAG, "ERROR : "+jsonObjectResponse.toString());
                        Log.d(TAG, "GAGAL UPLOAD DATA VIDEO KE SERVER DIKARENAKAN KONEKSI INTERNET TIDAK STABIL 1, MENCOBA UNTUK MENGIRIM DATA KEMBALI 1 MENIT KEDEPAN");
                        sendDelayed(60);
//                        mHandler.postDelayed(mRunnable, 60 * 1000);
//                        sqLiteDatabase.getWritableDatabase().delete(LocationProgress.TABLE, LocationProgress.SERVER_ID+" = ? AND "+LocationProgress.INDEX+" = ?", new String[]{String.valueOf(responseId), String.valueOf(index)});
                    }
                }else{
                    //no internet connection
                    Log.d(TAG, "GAGAL UPLOAD DATA VIDEO KE SERVER DIKARENAKAN KONEKSI INTERNET TIDAK STABIL 1, MENCOBA UNTUK MENGIRIM DATA KEMBALI 1 MENIT KEDEPAN");
                    sendDelayed(60);
//                    mHandler.postDelayed(mRunnable, 60 * 1000);
//                    Log.d(TAG, "ERROR INTERNET CONNECTION");
//                    sqLiteDatabase.getWritableDatabase().delete(LocationProgress.TABLE, LocationProgress.SERVER_ID+" = ? AND "+LocationProgress.INDEX+" = ?", new String[]{String.valueOf(responseId), String.valueOf(index)});
                }
            }
        }.execute();
    }

    private void send() {
        if(mHandler != null) {
            mHandler.post(mRunnable);
        }else{
            Log.d(TAG, "Service sudah dimatikan");
        }
    }

    private void sendDelayed(int i) {
        int delay = i * 1000;// in ms
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                if (mHandler != null) {
                    mHandler.post(mRunnable);
                } else {
                    Log.d(TAG, "Service sudah dimatikan");
                }
            }
        }, delay);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "ONDESTROY SERVICE");
        mHandler = null;
    }
}
