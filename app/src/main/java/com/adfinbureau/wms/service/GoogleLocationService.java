package com.adfinbureau.wms.service;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by yudiharto sasongko on 8/25/2016.
 */
public class GoogleLocationService extends Service implements OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMarkerClickListener
{
    private static String LOG_TAG = "GoogleLocationService";

    @Override
    public void onCreate() {
        super.onCreate();
        Log.v(LOG_TAG, "in onCreate");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.v(LOG_TAG, "in onDestroy");
    }

    public void onLocationChanged(Location location) {
        Log.d(LOG_TAG, "On location change : "+location.getLatitude()+" | "+location.getLongitude()+" | "+location.getAltitude());

        Intent in = new Intent(LOG_TAG);

        // Put extras into the intent as usual
        in.putExtra("longitude", location.getLongitude());
        in.putExtra("latitude", location.getLatitude());
        in.putExtra("altitude", location.getAltitude());
//        in.putExtra("accuracy", location.getAccuracy());


        LocalBroadcastManager.getInstance(this).sendBroadcast(in);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        MapsInitializer.initialize(this);

        com.google.android.gms.maps.MapView maps = new MapView(this);
        maps.onCreate(intent.getExtras());
        maps.onResume();
        maps.getMapAsync(this);

        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.v(LOG_TAG, "in onBind");
        return null;
    }

    GoogleMap googleMap;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.v(LOG_TAG, "MAP READY");

        this.googleMap = googleMap;

        enableMyLocation();
        GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {
                //Log.d(LOG_TAG, "onMyLocationChange "+location.getLongitude()+" | "+location.getLatitude());
                onLocationChanged(location);
            }
        };
        googleMap.setOnMyLocationChangeListener(myLocationChangeListener);

    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {
        Log.d(LOG_TAG, "Enable my location");

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            Log.d(LOG_TAG, "Map permission to access the location is missing.");
        } else if (googleMap != null) {
            // Access to the location has been granted to the app.
            Log.d(LOG_TAG, "Access to the location has been granted to the app.");
            googleMap.setMyLocationEnabled(true);
        }
    }
    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }
}
