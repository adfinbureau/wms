package com.adfinbureau.wms.service;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.adfinbureau.wms.Application;
import com.adfinbureau.wms.SharedPreferencesKeys;
import com.adfinbureau.wms.repository.JsonSharedPreferencesRepository;
import com.adfinbureau.wms.util.bluetooth.BlueTooth;
import com.adfinbureau.wms.util.bluetooth.OnLocationReceived;
import com.adfinbureau.wms.util.socket.LocationThread;

import javax.inject.Inject;

/**
 * Created by yudiharto sasongko on 8/25/2016.
 */
public class BluetoothLocationService extends Service implements OnLocationReceived
{
    private static String LOG_TAG = "BluetoothLocationService";

    BlueTooth bluetooth;

    @Inject
    JsonSharedPreferencesRepository sharedPreferencesRepository;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.v(LOG_TAG, "in onCreate");
        ((Application)getApplication()).inject(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(bluetooth!=null) {
            bluetooth.requested = false;
            bluetooth = null;
        }

//        Intent in = new Intent("ERROR");
//        in.putExtra("message", "Koneksi Bluetooth terputus");
//        LocalBroadcastManager.getInstance(this).sendBroadcast(in);

        Log.v(LOG_TAG, "in onDestroy");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        String address = intent.getStringExtra("address");
        String address = sharedPreferencesRepository.getObject(SharedPreferencesKeys.BLUETOOTH_GPS, String.class);

        if(address != null) {
            Log.d(LOG_TAG, "Start bluetooth Location Thread with address : " + address);
            bluetooth = new BlueTooth(address, this, this, new BlueTooth.BlueToothEvent() {
                @Override
                public void onBluetoothDisconnect() {
                Log.d(LOG_TAG, "Bluetooth Terminated");
                Intent in = new Intent("ERROR");
                in.putExtra("message", "Koneksi bluetooth terputus, mohon periksa koneksi bluetooth anda");
                LocalBroadcastManager.getInstance(BluetoothLocationService.this).sendBroadcast(in);
                stopSelf();
                }
            });
            bluetooth.start();
        }
        else{
            Intent in = new Intent("ERROR");
            in.putExtra("message", "Device bluetooth tidak ditemukan, harap setup bluetooth terlebih dahulu");
            LocalBroadcastManager.getInstance(this).sendBroadcast(in);
            stopSelf();
        }
        return START_NOT_STICKY;
    }

    @Override
    public void sendLocation(Location location) {
        Log.d(LOG_TAG, "On location change : "+location.getLatitude()+" | "+location.getLongitude()+" | "+location.getAltitude());

        Intent in = new Intent(LOG_TAG);

        // Put extras into the intent as usual
        in.putExtra("longitude", location.getLongitude());
        in.putExtra("latitude", location.getLatitude());
        in.putExtra("altitude", location.getAltitude());
//        in.putExtra("accuracy", location.getAccuracy());
        LocalBroadcastManager.getInstance(this).sendBroadcast(in);
    }
}
