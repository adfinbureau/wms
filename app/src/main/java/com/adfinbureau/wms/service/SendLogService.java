package com.adfinbureau.wms.service;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.adfinbureau.wms.Application;
import com.adfinbureau.wms.SharedPreferencesKeys;
import com.adfinbureau.wms.model.LocationData;
import com.adfinbureau.wms.model.UserWMS;
import com.adfinbureau.wms.repository.JsonSharedPreferencesRepository;
import com.adfinbureau.wms.util.DeviceName;
import com.adfinbureau.wms.util.helper.FileSplitter;
import com.adfinbureau.wms.util.requesthandler.CallUtils;
import com.google.gson.JsonObject;
import com.squareup.sqlbrite.BriteDatabase;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by yudiharto sasongko on 5/10/2016.
 */
public class SendLogService extends Service {
    @Inject
    BriteDatabase db;
    @Inject
    SQLiteOpenHelper sqLiteDatabase;

    @Inject
    LocationService locationService;

    @Inject
    JsonSharedPreferencesRepository sharedPreferencesRepository;

    private static final String TAG = SendLogService.class.getCanonicalName();
    private static final String processId = Integer.toString(android.os.Process
            .myPid());

    private Subscription subscription;
    private String username = "";


    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */

    @Override
    public void onCreate() {
        super.onCreate();
        ((Application)getApplication()).inject(this);

    }

    Runnable mRunnable;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Start SendLog");
        final Handler mHandler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {
                StringBuilder logs = getLog();
//                Log.d(TAG, logs.toString());
                mHandler.postDelayed(mRunnable, 10 * 1000);
            }
        };
        mHandler.postDelayed(mRunnable, 10 * 1000);
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static StringBuilder getLog() {

        StringBuilder builder = new StringBuilder();

        try {
            String[] command = new String[] { "logcat", "-d", "-v", "threadtime" };

            Process process = Runtime.getRuntime().exec(command);

            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if (line.contains(processId)) {
                    builder.append(line);
                    //Code here
                }
            }
        } catch (IOException ex) {
            Log.e(TAG, "getLog failed", ex);
        }

        return builder;
    }
}

