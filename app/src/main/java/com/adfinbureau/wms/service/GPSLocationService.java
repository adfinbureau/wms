package com.adfinbureau.wms.service;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.adfinbureau.wms.util.socket.LocationThread;
import com.adfinbureau.wms.util.socket.OnLocationReceived;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by yudiharto sasongko on 8/25/2016.
 */
public class GPSLocationService extends Service implements OnLocationReceived
{
    private static String LOG_TAG = "GPSLocationService";

    LocationThread gps;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.v(LOG_TAG, "in onCreate");

        gps = new LocationThread(this, this);
        gps.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        gps.requested = false;
        gps = null;

        Log.v(LOG_TAG, "in onDestroy");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }


    @Override
    public void sendLocation(Location location) {
        Log.d(LOG_TAG, "On location change : "+location.getLatitude()+" | "+location.getLongitude()+" | "+location.getAltitude());

        Intent in = new Intent(LOG_TAG);

        // Put extras into the intent as usual
        in.putExtra("longitude", location.getLongitude());
        in.putExtra("latitude", location.getLatitude());
        in.putExtra("altitude", location.getAltitude());
//        in.putExtra("accuracy", location.getAccuracy());
        LocalBroadcastManager.getInstance(this).sendBroadcast(in);
    }
}
