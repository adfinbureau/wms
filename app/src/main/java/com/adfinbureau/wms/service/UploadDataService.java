package com.adfinbureau.wms.service;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcel;
import android.support.annotation.Nullable;
import android.util.Log;

import com.adfinbureau.wms.Application;
import com.adfinbureau.wms.SharedPreferencesKeys;
import com.adfinbureau.wms.db.Db;
import com.adfinbureau.wms.model.LocationData;
import com.adfinbureau.wms.model.LocationProgress;
import com.adfinbureau.wms.model.UserWMS;
import com.adfinbureau.wms.repository.JsonSharedPreferencesRepository;
import com.adfinbureau.wms.util.DeviceName;
import com.adfinbureau.wms.util.helper.FileSplitter;
import com.adfinbureau.wms.util.requesthandler.CallUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.sqlbrite.BriteDatabase;

import org.acra.ACRA;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by yudiharto sasongko on 5/10/2016.
 */
public class UploadDataService extends Service {
    @Inject
    BriteDatabase db;
    @Inject
    SQLiteOpenHelper sqLiteDatabase;

    @Inject
    LocationService locationService;

    @Inject
    JsonSharedPreferencesRepository sharedPreferencesRepository;

    private Subscription subscription;
    private String username = "";

    private static final String TAG = "UploadData";

    @Override
    public void onCreate() {
        super.onCreate();
        ((Application)getApplication()).inject(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    Runnable mRunnable;
    Runnable mInternetRunnable;
    Handler mHandler;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "SERVICE UPLOAD DATA (NODE) SUDAH MENYALA " + startId + ": " + intent);
        mHandler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {
                Cursor cursor = db.query(LocationData.QUERY_PENDING);
                Log.d(TAG, "JUMLAH DATA NODE YANG AKAN DIUPLOAD : " + cursor.getCount());
                if(cursor.getCount() > 0) {
                    List<LocationData> locations = new ArrayList<LocationData>();
                    try {
                        while (cursor.moveToNext()) {
                            final long id = Db.getLong(cursor, LocationData.ID);
                            final String serverId = Db.getString(cursor, LocationData.SERVER_ID);
                            final String longitude = Db.getString(cursor, LocationData.LONGITUDE);
                            final String latitude = Db.getString(cursor, LocationData.LATITUDE);
                            final String altitude = Db.getString(cursor, LocationData.ALTITUDE);
                            final String picture = Db.getString(cursor, LocationData.PICTURE);
                            final String video = Db.getString(cursor, LocationData.VIDEO);
                            final String asbuild = Db.getString(cursor, LocationData.AS_BUILD);
                            final String text = Db.getString(cursor, LocationData.TEXT);
                            final String status = Db.getString(cursor, LocationData.STATUS);
                            final String description = Db.getString(cursor, LocationData.DESCRIPTION);
                            final String survey = Db.getString(cursor, LocationData.SURVEY_ID);

                            final String masalah = Db.getString(cursor, LocationData.MASALAH);
                            final String usulan = Db.getString(cursor, LocationData.USULAN);
                            final String type = Db.getString(cursor, LocationData.TYPE);

                            LocationData locationData = new LocationData() {
                                @Override
                                public long id() {
                                    return id;
                                }

                                @Override
                                public String serverId() {
                                    return serverId;
                                }

                                @Override
                                public String longitude() {
                                    return longitude;
                                }

                                @Override
                                public String latitude() {
                                    return latitude;
                                }

                                @Override
                                public String altitude(){ return altitude; }

                                @Override
                                public String picture() {
                                    return picture;
                                }

                                @Override
                                public String video() {
                                    return video;
                                }

                                @Override
                                public String asbuild() {
                                    return asbuild;
                                }

                                @Override
                                public String text() {
                                    return text;
                                }

                                @Override
                                public String status() {
                                    return status;
                                }

                                @Override
                                public String description() {
                                    return description;
                                }

                                @Override
                                public String survey() {
                                    return survey;
                                }

                                @Override
                                public int describeContents() {
                                    return 0;
                                }

                                @Override
                                public String masalah() {
                                    return masalah;
                                }

                                @Override
                                public String usulan() {
                                    return usulan;
                                }

                                @Override
                                public String type() {
                                    return type;
                                }

                                @Override
                                public void writeToParcel(Parcel dest, int flags) {

                                }
                            };
                            locations.add(locationData);
                        }
                    } finally {
                        cursor.close();
                    }

                    UserWMS userWMS = sharedPreferencesRepository.getObject(SharedPreferencesKeys.USER_ACCOUNT, UserWMS.class);
                    if (userWMS != null) {
                        username = userWMS.getUsername();
                        Log.d(TAG, "USERNAME : " + username + " akan melakukan upload node");
                        for (final LocationData x : locations) {
                            final com.adfinbureau.wms.model.Location location = new com.adfinbureau.wms.model.Location();
                            location.setType(x.type());
                            location.setAltitude(x.altitude());
                            location.setLongitude(x.longitude());
                            location.setLatitude(x.latitude());
                            location.setPictureContentType("image/jpeg");
                            location.setVideoContentType("video/mp4");
                            location.setAsbuildContentType("image/jpeg");
                            location.setText(x.text());
                            location.setStatus(x.status());
                            location.setDescription(x.description());
                            location.setUsername(username);
                            location.setDeviceName("Device : " + DeviceName.getDeviceName());
                            location.setVersion("1");

                            if (!x.masalah().contentEquals("")) {
                                Log.d(TAG, "NODE (LONG : " + x.longitude() + " | LAT : " + x.latitude() + ") INI BERJENIS MASALAH");

                                location.setMasalah(x.masalah());
                                location.setUsulan(x.usulan());
                            }

                            if (!x.picture().contentEquals("")) {
                                Log.d(TAG, "NODE (LONG : " + x.longitude() + " | LAT : " + x.latitude() + ") INI BERJENIS PICTURE");
                                int size = 0;
                                try {
                                    Log.d(TAG, "MEMULAI MEMECAH GAMBAR " + x.picture());
                                    size = FileSplitter.split(x.picture(), "image");
                                    location.setPictureSpliceCount(size);
                                    Log.d(TAG, "GAMBAR BERHASIL DIPECAH, JUMLAH PECAHAN GAMBAR " + x.picture() + " : " + size);
                                } catch (IOException e) {
                                    Log.d(TAG, "GAGAL MEMECAH GAMBAR KARENA : " + e.getMessage());
                                    e.printStackTrace();
                                    ACRA.getErrorReporter().handleException(e);
                                } finally {
                                    if (size == 0)
                                        return;
                                }
                                Log.d(TAG, "GAMBAR YANG AKAN DIPECAH " + x.picture());
                            } else {
                                //                            Log.d(TAG, "");
                            }

                            if (!x.video().contentEquals("")) {
                                Log.d(TAG, "NODE (LONG : " + x.longitude() + " | LAT : " + x.latitude() + ") INI BERJENIS VIDEO");
                                int size = 0;
                                try {
                                    Log.d(TAG, "MEMULAI MEMECAH VIDEO");
                                    size = FileSplitter.split(x.video(), "video");
                                    location.setVideoSpliceCount(size);
                                    Log.d(TAG, "VIDEO BERHASIL DIPECAH, JUMLAH PECAHAN  : " + size);
                                } catch (IOException e) {
                                    Log.d(TAG, "GAGAL MEMECAH VIDEO KARENA : " + e.getMessage());
                                    e.printStackTrace();
                                    ACRA.getErrorReporter().handleException(e);
                                } finally {
                                    if (size == 0)
                                        return;
                                }
                                Log.d(TAG, "VIDEO YANG AKAN DIPECAH " + x.video());
                            } else {
                                //                            Log.d(TAG, "VIDEO NULL");
                            }

                            if (!x.asbuild().contentEquals("")) {
                                Log.d(TAG, "NODE (LONG : " + x.longitude() + " | LAT : " + x.latitude() + ") INI BERJENIS ASBUILD");
                                int size = 0;
                                try {
                                    Log.d(TAG, "MEMULAI MEMECAH ASBUILD");
                                    size = FileSplitter.split(x.asbuild(), "image");
                                    location.setAsbuildSpliceCount(size);
                                    Log.d(TAG, "ASBUILD BERHASIL DIPECAH, JUMLAH PECAHAN  : " + size);
                                } catch (IOException e) {
                                    Log.d(TAG, "GAGAL MEMECAH ASBUILD KARENA : " + e.getMessage());
                                    e.printStackTrace();
                                    ACRA.getErrorReporter().handleException(e);
                                } finally {
                                    if (size == 0)
                                        return;
                                }
                                Log.d(TAG, "AS BUILD YANG AKAN DIPECAH " + x.asbuild());
                            } else {
                                //                            Log.d(TAG, "ASBUILD NULL");
                            }

                            Log.d(TAG, "READY TO SEND : "+location);

                            new AsyncTask<String, String, Response<JsonObject>>() {
                                @Override
                                protected Response<JsonObject> doInBackground(String... params) {
                                    try {
                                        Log.d(TAG, "MEMULAI MENGIRIM NODE (LONG : " + x.longitude() + " | LAT : " + x.latitude() + ") KE SERVER");
                                        return locationService.addLocation(location).execute();
                                    } catch (OutOfMemoryError e) {
                                        Log.d(TAG, "GAGAL UPLOAD DATA TITIK KE SERVER DIKARENAKAN KONEKSI INTERNET TIDAK STABIL DOINBACKGROUND EXCEPTION :  MEMORY ERROR " + e.getMessage());
                                        e.printStackTrace();
                                        ACRA.getErrorReporter().handleSilentException(e);
                                    } catch (IOException e) {
                                        Log.d(TAG, "GAGAL UPLOAD DATA TITIK KE SERVER DIKARENAKAN KONEKSI INTERNET TIDAK STABIL DOINBACKGROUND EXCEPTION :  IOException " + e.getMessage());
                                        e.printStackTrace();
                                    } catch (Exception e) {
                                        Log.d(TAG, "GAGAL UPLOAD DATA TITIK KE SERVER DIKARENAKAN KONEKSI INTERNET TIDAK STABIL DOINBACKGROUND EXCEPTION : Exception" + e.getMessage());
                                        e.printStackTrace();
                                    }
                                    return null;
                                }

                                @Override
                                protected void onPostExecute(Response<JsonObject> response) {
                                    super.onPostExecute(response);

                                    if (response != null) {
                                        if (response.isSuccessful()) {
                                            if (!x.picture().contentEquals("") || !x.video().contentEquals("") || !x.asbuild().contentEquals("")) {
                                                Log.d(TAG, "NODE (LONG : " + x.longitude() + " | LAT : " + x.latitude() + ") YANG BERISI " + x.type() + " BERHASIL TERKIRIM");
                                                sqLiteDatabase.getWritableDatabase().update(LocationData.TABLE,
                                                        new LocationData.Builder()
                                                                .status("PROGRESS")
                                                                .serverId(response.body().get("id").getAsString())
                                                                .build(), LocationData.ID + " = ?", new String[]{String.valueOf(x.id())});
                                            } else {
                                                Log.d(TAG, "NODE (LONG : " + x.longitude() + " | LAT : " + x.latitude() + ") BERHASIL DIKIRIM KE SERVER");
                                                sqLiteDatabase.getWritableDatabase().update(LocationData.TABLE,
                                                        new LocationData.Builder()
                                                                .status("COMPLETE")
                                                                .serverId(response.body().get("id").getAsString())
                                                                .build(), LocationData.ID + " = ?", new String[]{String.valueOf(x.id())});
                                            }
                                            send();
//                                            mHandler.postDelayed(mRunnable, 60 * 1000);
                                        } else {
                                            Log.d(TAG, "GAGAL UPLOAD DATA TITIK KE SERVER DIKARENAKAN KONEKSI INTERNET TIDAK STABIL 1");
//                                            mHandler.post(mRunnable);
//                                            mHandler.postDelayed(mRunnable, 60 * 1000);
                                            sendDelayed(60);
                                        }
                                    } else {
                                        Log.d(TAG, "GAGAL UPLOAD DATA TITIK KE SERVER DIKARENAKAN KONEKSI INTERNET TIDAK STABIL 2");
//                                        mHandler.post(mRunnable);
//                                        mHandler.postDelayed(mRunnable, 60 * 1000);
                                        sendDelayed(60);
                                    }
                                }
                            }.execute();
                        }
                    } else {
                        Log.d(TAG, "UPLOAD TITIK TIDAK DAPAT DILAKUKAN TANPA USER (HARUS LOGIN DULU)");
//                        mHandler.post(mRunnable);
//                        mHandler.postDelayed(mRunnable, 60 * 1000);
                        sendDelayed(60);
                    }
                }else{
                    Log.d(TAG, "TIDAK ADA TITIK UNTUK DIUPLOAD, MENUNGGU 1 MENIT KEMUDIAN");
//                     mHandler.post(mRunnable);
//                    mHandler.postDelayed(mRunnable, 60 * 1000);
                    sendDelayed(60);
                }
            }
        };
        mHandler.post(mRunnable);
        return START_NOT_STICKY;
    }

    private void send() {
        if(mHandler != null) {
            mHandler.post(mRunnable);
        }else{
            Log.d(TAG, "Service sudah dimatikan");
        }
    }

    private void sendDelayed(int i) {
            int delay = i * 1000;// in ms
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                public void run() {
                    if(mHandler != null) {
                        mHandler.post(mRunnable);
                    }else{
                        Log.d(TAG, "Service sudah dimatikan");
                    }
                }
            }, delay);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "ONDESTROY SERVICE");
        mHandler = null;
    }
}
