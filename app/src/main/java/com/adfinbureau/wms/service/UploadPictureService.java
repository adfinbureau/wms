package com.adfinbureau.wms.service;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

import com.adfinbureau.wms.Application;
import com.adfinbureau.wms.model.Location;
import com.adfinbureau.wms.model.LocationData;
import com.adfinbureau.wms.model.LocationProgress;
import com.adfinbureau.wms.util.helper.FileSplitter;
import com.adfinbureau.wms.util.requesthandler.CallUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.sqlbrite.BriteDatabase;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by yudiharto sasongko on 5/10/2016.
 */
public class UploadPictureService extends IntentService {
    @Inject
    BriteDatabase db;
    @Inject
    LocationService locationService;
    private Subscription subscription;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public UploadPictureService() {
        super("UploadPictureService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ((Application)getApplication()).inject(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        subscription = db.createQuery(LocationData.TABLE,
            LocationData.QUERY_PROGRESS_PICTURE)
            .mapToList(LocationData.MAPPER)
            .subscribeOn(Schedulers.newThread())
            .subscribe(new Action1<List<LocationData>>() {
                @Override
                public void call(final List<LocationData> locationListses) {
                    Log.d("RESPONSE", "MEMULAI MENGIRIM GAMBAR : " + locationListses.size());
                    for (final LocationData x : locationListses) {
                        if (!x.picture().contentEquals("")) {
                            Log.d("RESPONSE", "PICTURE NOT NULL");
                            try {
                                sendSplice(x.picture(), x.serverId());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Log.d("RESPONSE", "PICTURE NULL");
                        }
                    }
                }
            });
    }

    public byte[] extractBytes (String ImageName) throws IOException {
        FileInputStream fileInputStream=null;

        File file = new File(ImageName);

        byte[] bFile = new byte[(int) file.length()];

        try {
            //convert file into array of bytes
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bFile);
            fileInputStream.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return bFile;
    }

    public void sendSplice(String baseFilename, String id) throws IOException{
        Log.d("SEND SPLICE", baseFilename);
        int numberParts = FileSplitter.getNumberParts(baseFilename);
        try {
            String responseId = id;
            for (int part = 0; part < numberParts; part++){
                byte[] splice = extractBytes(baseFilename + "." + part);
                sendPOST(baseFilename, part, splice, responseId);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    private void sendPOST(final String basefilename, final int index, final byte[] splice, final String responseId) throws IOException {
        Log.d("SEND POST", "SEND IMAGE PART " + index);

        com.adfinbureau.wms.model.Location location = new com.adfinbureau.wms.model.Location();
        location.setId(responseId);
        location.setPictureSplice(splice);
        location.setPictureSpliceIndex(index);

        String check = "SELECT * FROM "+LocationProgress.TABLE+" WHERE "+LocationProgress.SERVER_ID+" = '"+responseId+"' AND "+LocationProgress.INDEX+" = '"+index+"'";
        if(db.query(check).getCount() == 0) {
            CallUtils.enqueueWithRetry(
                    locationService.editLocation(location), new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            db.insert(LocationProgress.TABLE, new LocationProgress.Builder()
                                    .index(index + "")
                                    .serverId(responseId)
                                    .build());

                            String checkAll = "SELECT * FROM "+LocationProgress.TABLE+" WHERE "+LocationProgress.SERVER_ID+" = '"+responseId+"'";
                            try {
                                int parts = db.query(checkAll).getCount();
                                int numberParts = FileSplitter.getNumberParts(basefilename);

                                if(parts == numberParts){
//                                    db.update(LocationData.TABLE,
//                                            new LocationData.Builder()
//                                                    .status("COMPLETE")
//                                                    .build(), LocationData.SERVER_ID + " = ?", new String[]{String.valueOf(responseId)});
//                                    db.execute("UPDATE "+LocationData.TABLE+" SET "+LocationData.STATUS+" = 'COMPLETE' WHERE "+LocationData.SERVER_ID+" = '"+responseId+"'");
                                    FileSplitter.deleteParts(basefilename);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            if (response.body() != null) {
                                Log.d("RESPONSE", "ID : " + response.body().get("id").getAsString());
                            }
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            Log.d("RESPONSE", "FAILURE : " + t.getMessage());
                        }
                    }
            );
        }else{
            Log.d("PART " + index, "ALREADY SENDED");
        }
    }
}
