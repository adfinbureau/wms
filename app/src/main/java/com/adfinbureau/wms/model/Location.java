package com.adfinbureau.wms.model;

/**
 * Created by yudiharto sasongko on 5/16/2016.
 */
public class Location {
    private String id;
    private String longitude;
    private String latitude;
    private String altitude;
    private String masalah;
    private String usulan;
    private String type;
    private byte[] picture;
    private String pictureContentType;
    private byte[] video;
    private String videoContentType;
    private byte[] asbuild;
    private String asbuildContentType;
    private String status;
    private String text;
    private String description;
    private int pictureSpliceCount;
    private int pictureSpliceIndex;
    private byte[] pictureSplice;
    private byte[] videoSplice;
    private int videoSpliceIndex;
    private int videoSpliceCount;
    private byte[] asbuildSplice;
    private int asbuildSpliceIndex;
    private int asbuildSpliceCount;

    private String username;
    private String deviceName;
    private String version;

    public int getPictureSpliceCount() {
        return pictureSpliceCount;
    }

    public void setPictureSpliceCount(int pictureSpliceCount) {
        this.pictureSpliceCount = pictureSpliceCount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAltitude() {
        return altitude;
    }

    public void setAltitude(String altitude) {
        this.altitude = altitude;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getPictureContentType() {
        return pictureContentType;
    }

    public void setPictureContentType(String pictureContentType) {
        this.pictureContentType = pictureContentType;
    }

    public byte[] getVideo() {
        return video;
    }

    public void setVideo(byte[] video) {
        this.video = video;
    }

    public String getVideoContentType() {
        return videoContentType;
    }

    public void setVideoContentType(String videoContentType) {
        this.videoContentType = videoContentType;
    }

    public byte[] getAsbuild() {
        return asbuild;
    }

    public void setAsbuild(byte[] asbuild) {
        this.asbuild = asbuild;
    }

    public String getAsbuildContentType() {
        return asbuildContentType;
    }

    public void setAsbuildContentType(String asbuildContentType) {
        this.asbuildContentType = asbuildContentType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPictureSpliceIndex() {
        return pictureSpliceIndex;
    }

    public void setPictureSpliceIndex(int pictureSpliceIndex) {
        this.pictureSpliceIndex = pictureSpliceIndex;
    }

    public byte[] getPictureSplice() {
        return pictureSplice;
    }

    public void setPictureSplice(byte[] pictureSplice) {
        this.pictureSplice = pictureSplice;
    }

    public void setVideoSplice(byte[] videoSplice) {
        this.videoSplice = videoSplice;
    }

    public byte[] getVideoSplice() {
        return videoSplice;
    }

    public void setVideoSpliceIndex(int videoSpliceIndex) {
        this.videoSpliceIndex = videoSpliceIndex;
    }

    public int getVideoSpliceIndex() {
        return videoSpliceIndex;
    }

    public void setVideoSpliceCount(int videoSpliceCount) {
        this.videoSpliceCount = videoSpliceCount;
    }

    public int getVideoSpliceCount() {
        return videoSpliceCount;
    }

    public byte[] getAsbuildSplice() {
        return asbuildSplice;
    }

    public void setAsbuildSplice(byte[] asbuildSplice) {
        this.asbuildSplice = asbuildSplice;
    }

    public int getAsbuildSpliceIndex() {
        return asbuildSpliceIndex;
    }

    public void setAsbuildSpliceIndex(int asbuildSpliceIndex) {
        this.asbuildSpliceIndex = asbuildSpliceIndex;
    }

    public int getAsbuildSpliceCount() {
        return asbuildSpliceCount;
    }

    public void setAsbuildSpliceCount(int asbuildSpliceCount) {
        this.asbuildSpliceCount = asbuildSpliceCount;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMasalah() {
        return masalah;
    }

    public void setMasalah(String masalah) {
        this.masalah = masalah;
    }

    public String getUsulan() {
        return usulan;
    }

    public void setUsulan(String usulan) {
        this.usulan = usulan;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Location{" +
                "longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", altitude='" + altitude + '\'' +
                '}';
    }
}
