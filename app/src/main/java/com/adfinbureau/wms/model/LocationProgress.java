/*
 * Copyright (C) 2015 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.adfinbureau.wms.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcelable;

import com.adfinbureau.wms.db.Db;
import com.google.auto.value.AutoValue;

import rx.functions.Func1;

@AutoValue
public abstract class LocationProgress implements Parcelable {
  public static final String TABLE = "location_progress";

  public static final String ID = "_id";
  public static final String SERVER_ID = "server_id";
  public static final String INDEX = "survey_id";

  public abstract long id();
  public abstract String serverId();
  public abstract String index();

  public static final String QUERY_PENDING = "SELECT * FROM "+TABLE+" WHERE status = 'PENDING' AND server_id is null";
  public static final String QUERY_PROGRESS = "SELECT * FROM "+TABLE+" WHERE status = 'PROGRESS' AND server_id != ''";

  public static final Func1<Cursor, LocationProgress> MAPPER = new Func1<Cursor, LocationProgress>() {
    @Override public LocationProgress call(Cursor cursor) {
      long id = Db.getLong(cursor, ID);
      String serverId = Db.getString(cursor, SERVER_ID);
      String index = Db.getString(cursor, INDEX);
      return new AutoValue_LocationProgress(id, serverId, index);
    }
  };

  public static final class Builder {
    private final ContentValues values = new ContentValues();

    public Builder id(long id) {
      values.put(ID, id);
      return this;
    }

    public Builder serverId(String serverId){
      values.put(SERVER_ID, serverId);
      return this;
    }

    public Builder index(String index) {
      values.put(INDEX, index);
      return this;
    }
    public ContentValues build() {
      return values; // TODO defensive copy?
    }
  }
}
