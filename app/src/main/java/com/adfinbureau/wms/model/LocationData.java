/*
 * Copyright (C) 2015 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.adfinbureau.wms.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcelable;

import com.adfinbureau.wms.db.Db;
import com.google.auto.value.AutoValue;

import java.util.HashMap;
import java.util.Map;

import rx.functions.Func1;

@AutoValue
public abstract class LocationData implements Parcelable {
  public static final String TABLE = "location_data";

  public static final String ID = "_id";
  public static final String SERVER_ID = "server_id";
  public static final String LONGITUDE = "longitude";
  public static final String LATITUDE = "latitude";
  public static final String ALTITUDE = "altitude";
  public static final String PICTURE = "picture";
  public static final String VIDEO = "video";
  public static final String AS_BUILD = "as_build";
  public static final String TEXT = "text";
  public static final String STATUS = "status";
  public static final String DESCRIPTION = "description";

  public static final String MASALAH = "masalah";
  public static final String USULAN = "usulan";
  public static final String SURVEY_ID = "survey_id";

  public static final String TYPE = "type";

  public abstract long id();
  public abstract String serverId();
  public abstract String longitude();
  public abstract String latitude();
  public abstract String altitude();
  public abstract String picture();
  public abstract String video();
  public abstract String asbuild();
  public abstract String text();
  public abstract String status();
  public abstract String description();
  public abstract String masalah();
  public abstract String usulan();
  public abstract String survey();
  public abstract String type();

  //PENDING
  //belum diupload ke server
  public static final String QUERY_PENDING = "SELECT * FROM "+TABLE+" WHERE status = 'PENDING' AND server_id is null LIMIT 1";

  //UPDATE
  //titik sudah diupload, hanya untuk proses update description
  public static final String QUERY_UPDATE = "SELECT * FROM "+TABLE+" WHERE status = 'UPDATE' AND server_id != ''";

  //PROGRESS
  //titik sudah diupload, menunggu upload gambar ke server
  public static final String QUERY_PROGRESS_PICTURE = "SELECT * FROM "+TABLE+" WHERE status = 'PROGRESS' AND picture != '' AND server_id != '' LIMIT 1";

  //PROGRESS
  //titik sudah diupload, menunggu upload video ke server
  public static final String QUERY_PROGRESS_VIDEO = "SELECT * FROM "+TABLE+" WHERE status = 'PROGRESS' AND video != '' AND server_id != '' LIMIT 1";

  //PROGRESS
  //titik sudah diupload, menunggu upload asbuild ke server
  public static final String QUERY_PROGRESS_ASBUILD = "SELECT * FROM "+TABLE+" WHERE status = 'PROGRESS' AND as_build != '' AND server_id != '' LIMIT 1";

  public static final Func1<Cursor, LocationData> MAPPER = new Func1<Cursor, LocationData>() {
    @Override public LocationData call(Cursor cursor) {
      long id = Db.getLong(cursor, ID);
      String serverId = Db.getString(cursor, SERVER_ID);
      String longitude = Db.getString(cursor, LONGITUDE);
      String latitude = Db.getString(cursor, LATITUDE);
      String altitude = Db.getString(cursor, ALTITUDE);
      String picture = Db.getString(cursor, PICTURE);
      String video = Db.getString(cursor, VIDEO);
      String asbuild = Db.getString(cursor, AS_BUILD);
      String text = Db.getString(cursor, TEXT);
      String status = Db.getString(cursor, STATUS);
      String description = Db.getString(cursor, DESCRIPTION);
  //    Map<String, Object> pictureProgress = Db.getMap(cursor, PICTURE_PROGRESS);
      String masalah = Db.getString(cursor, MASALAH);
      String usulan = Db.getString(cursor, USULAN);
      String survey = Db.getString(cursor, SURVEY_ID);
      String type = Db.getString(cursor, TYPE);
      return new AutoValue_LocationData(id, serverId, longitude, latitude, altitude, picture, video, asbuild, text, status, description, masalah, usulan, survey, type);
    }
  };

  public static final class Builder {
    private final ContentValues values = new ContentValues();

    public Builder id(long id) {
      values.put(ID, id);
      return this;
    }

    public Builder serverId(String serverId){
      values.put(SERVER_ID, serverId);
      return this;
    }

    public Builder longitude(String longitude) {
      values.put(LONGITUDE, longitude);
      return this;
    }

    public Builder latitude(String latitude) {
      values.put(LATITUDE, latitude);
      return this;
    }

    public Builder altitude(String altitude) {
      values.put(ALTITUDE, altitude);
      return this;
    }

    public Builder status(String status) {
      values.put(STATUS, status);
      return this;
    }

    public Builder picture(String status) {
      values.put(PICTURE, status);
      return this;
    }
    public Builder video(String status) {
      values.put(VIDEO, status);
      return this;
    }
    public Builder asbuild(String status) {
      values.put(AS_BUILD, status);
      return this;
    }
    public Builder text(String status) {
      values.put(TEXT, status);
      return this;
    }
    public Builder description(String description) {
      values.put(DESCRIPTION, description);
      return this;
    }

    public Builder survey(String id){
      values.put(SURVEY_ID, id);
      return this;
    }

    public Builder masalah(String masalah){
      values.put(MASALAH, masalah);
      return this;
    }

    public Builder usulan(String id){
      values.put(USULAN, id);
      return this;
    }

    public Builder type(String type){
      values.put(TYPE, type);
      return this;
    }
    public ContentValues build() {
      return values; // TODO defensive copy?
    }
  }
}
