/*
 * Copyright (C) 2015 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.adfinbureau.wms.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcelable;

import com.adfinbureau.wms.db.Db;
import com.google.auto.value.AutoValue;

import rx.functions.Func1;

@AutoValue
public abstract class Survey implements Parcelable {
    public static final String TABLE = "survey";

    public static final String ID = "_id";
    public static final String DATA = "data";
    public static final String CREATED_DATE = "created_date";

    public abstract long id();
    public abstract String data();
    public abstract String date();

    public static final Func1<Cursor, Survey> MAPPER = new Func1<Cursor, Survey>() {
        @Override public Survey call(Cursor cursor) {
            long id = Db.getLong(cursor, ID);
            String data = Db.getString(cursor, DATA);
            String date = Db.getString(cursor, CREATED_DATE);
            return new AutoValue_Survey(id, data, date);
        }
    };

    public static final class Builder {
        private final ContentValues values = new ContentValues();

        public Builder id(long id) {
            values.put(ID, id);
            return this;
        }
        public Builder data(String data) {
            values.put(DATA, data);
            return this;
        }
        public Builder date(String date) {
            values.put(CREATED_DATE, date);
            return this;
        }
        public ContentValues build() {
            return values; // TODO defensive copy?
        }
    }
}
