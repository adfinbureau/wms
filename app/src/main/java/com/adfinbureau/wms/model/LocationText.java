package com.adfinbureau.wms.model;

import com.orm.SugarRecord;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by SE-Billy on 4/11/2016.
 */
public class LocationText extends SugarRecord{
    @NotBlank(message = "Isi masalah")
    private String masalah;
    @NotBlank(message = "Isi usulan")
    private String usulan;

    public LocationText(String masalah, String usulan) {
        this.masalah = masalah;
        this.usulan = usulan;
    }

    public String getMasalah() {
        return masalah;
    }

    public String getUsulan() {
        return usulan;
    }

    public void setMasalah(String masalah) {
        this.masalah = masalah;
    }

    public void setUsulan(String usulan) {
        this.usulan = usulan;
    }
}
