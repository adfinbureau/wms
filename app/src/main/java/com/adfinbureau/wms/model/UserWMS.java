package com.adfinbureau.wms.model;

import com.orm.SugarRecord;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by SE-Billy on 4/11/2016.
 */
public class UserWMS extends SugarRecord{
    @NotBlank(message = "Please provide your username")
    private String username;
    @NotBlank(message = "Please provide your password")
    private String password;
    private String token;

    UserWMS() {
    }

    public UserWMS(String username, String password, String token) {
        this.setUsername(username);
        this.setPassword(password);
        this.setToken(token);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("username", getUsername())
                .append("password", getPassword())
                .append("token", getToken())
                .toString();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
