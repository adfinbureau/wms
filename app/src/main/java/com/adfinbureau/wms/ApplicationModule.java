package com.adfinbureau.wms;

import android.content.SharedPreferences;

import com.adfinbureau.wms.analytics.AnalyticsModule;
import com.adfinbureau.wms.android.AndroidModule;
import com.adfinbureau.wms.db.DbModule;
import com.adfinbureau.wms.environment.Environment;
import com.adfinbureau.wms.environment.EnvironmentModule;
import com.adfinbureau.wms.receiver.ConnectionMonitor;
import com.adfinbureau.wms.repository.JsonSharedPreferencesRepository;
import com.adfinbureau.wms.service.ApiService;
import com.adfinbureau.wms.service.BluetoothLocationService;
import com.adfinbureau.wms.service.InjectingServiceModule;
import com.adfinbureau.wms.service.LocationService;
//import com.adfinbureau.wms.service.RegistrationIntentService;
import com.adfinbureau.wms.service.SendLogService;
import com.adfinbureau.wms.service.StubApiService;
import com.adfinbureau.wms.service.TestService;
import com.adfinbureau.wms.service.UpdateDataServiceNew;
import com.adfinbureau.wms.service.UploadAsbuildServiceListener;
import com.adfinbureau.wms.service.UploadDataService;
import com.adfinbureau.wms.service.UploadPictureServiceListener;
import com.adfinbureau.wms.service.UploadVideoServiceListener;
import com.adfinbureau.wms.service.UserService;
import com.adfinbureau.wms.util.gson.GsonModule;
import com.google.gson.GsonBuilder;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;
import javax.validation.Validation;
import javax.validation.Validator;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(
  includes = {
    DbModule.class,
    AndroidModule.class,
    AnalyticsModule.class,
    EnvironmentModule.class,
    GsonModule.class,
    InjectingServiceModule.class
  },
  injects = {
  //        RegistrationIntentService.class,
          SendLogService.class,
          UploadDataService.class,
          UpdateDataServiceNew.class,
          UploadPictureServiceListener.class,
          UploadAsbuildServiceListener.class,
          UploadVideoServiceListener.class,
          ConnectionMonitor.class,
          BluetoothLocationService.class
  },
  library = true
)
public class ApplicationModule {
  private Application application;

  ApplicationModule(Application application) {
    this.application = application;
  }

  @Provides
  @Singleton
  android.app.Application provideApplication() {
    return application;
  }

  @Provides
  @Singleton
  Bus providesBus() {
    return new Bus(ThreadEnforcer.MAIN); // my name is Otto and I love to get blotto
  }

  @Provides
  @Singleton
  JsonSharedPreferencesRepository provideSharedPreferenceRepository(GsonBuilder gsonBuilder, SharedPreferences sharedPreferences) {
    return new JsonSharedPreferencesRepository(gsonBuilder, sharedPreferences);
  }

  /*
  @Provides
  @Singleton
  ApiService provideApiService(Environment environment) {
    if (environment.getName().equals("Local")) {
      return new StubApiService();
    } else {
      RestAdapter restAdapter = new RestAdapter.Builder()
        .setLogLevel(RestAdapter.LogLevel.FULL)
        .setEndpoint(environment.getApiHost() + environment.getApiBasePath())
        .build();
      return restAdapter.create(ApiService.class);
    }
  }
  */


  @Provides
  @Singleton
  ApiService provideApiService(Environment environment) {
    if (environment.getName().equals("Local")) {
      return new StubApiService();
    } else {
      /*RestAdapter restAdapter = new RestAdapter.Builder()
              .setLogLevel(RestAdapter.LogLevel.FULL)
              .setEndpoint(environment.getApiHost() + environment.getApiBasePath())
              .build();
      return restAdapter.create(ApiService.class);
      */

      final OkHttpClient httpClient = new OkHttpClient.Builder()
              .writeTimeout(60, TimeUnit.MINUTES)
              .readTimeout(60, TimeUnit.MINUTES)
              .connectTimeout(60, TimeUnit.MINUTES)
              .build();
//      OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
      Retrofit.Builder builder =
              new Retrofit.Builder()
                      .baseUrl(environment.getApiHost() + environment.getApiBasePath())
                      .addConverterFactory(GsonConverterFactory.create());
      Retrofit retrofit = builder.client(httpClient).build();
      return retrofit.create(ApiService.class);
    }
  }

  @Provides
  @Singleton
  UserService provideUserService(Environment environment) {
    Retrofit retrofit = buildRetrofit(environment);
    return retrofit.create(UserService.class);
  }

  @Provides
  @Singleton
  LocationService provideLocationService(Environment environment) {
    Retrofit retrofit = buildRetrofit(environment);
    return retrofit.create(LocationService.class);
  }

  @Provides
  @Singleton
  Retrofit buildRetrofit(Environment environment){
//    OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//    httpClient.addInterceptor(new Interceptor() {
//      @Override
//      public Response intercept(Interceptor.Chain chain) throws IOException {
//        Request original = chain.request();
//
//        Request.Builder requestBuilder = original.newBuilder()
//                .header("Accept", "application/json")
////                .header("Authorization",
////                        "Bearer" + " " + "a1c069ec-7b30-4435-8731-180e92ec2b27")
//                .method(original.method(), original.body());
//
//        Request request = requestBuilder.build();
//        return chain.proceed(request);
//      }
//    });
    final OkHttpClient httpClient = new OkHttpClient.Builder()
//            .writeTimeout(60, TimeUnit.MINUTES)
//            .readTimeout(60, TimeUnit.MINUTES)
//            .connectTimeout(60, TimeUnit.MINUTES)
            .addInterceptor(new Interceptor() {
              @Override
              public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                Request.Builder requestBuilder = original.newBuilder()
                        .header("Accept", "application/json")
//                .header("Authorization",
//                        "Bearer" + " " + "a1c069ec-7b30-4435-8731-180e92ec2b27")
                        .method(original.method(), original.body());

                Request request = requestBuilder.build();
                return chain.proceed(request);
              }
            })
            .build();

    //environment.getApiHost() + environment.getApiBasePath()
    Retrofit.Builder builder =
            new Retrofit.Builder()
//                    .baseUrl("http://192.168.43.19:8080/api/")
                    .baseUrl("http://103.4.164.221:8089/api/")
                    .addConverterFactory(GsonConverterFactory.create());
    Retrofit retrofit = builder.client(httpClient).build();
    return retrofit;
  }

  @Provides
  @Singleton
  TestService provideTestService(Environment environment) {
    OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(environment.getApiHost() + environment.getApiBasePath())
                    .addConverterFactory(GsonConverterFactory.create());
    Retrofit retrofit = builder.client(httpClient.build()).build();
    return retrofit.create(TestService.class);
  }


  @Provides
  @Singleton
  Validator provideValidator() {
    return Validation.buildDefaultValidatorFactory().getValidator();
  }

  /*
  public static <S> S createService(Class<S> serviceClass) {
    return createService(serviceClass, null);
  }

  public static <S> S createService(Class<S> serviceClass, String username, String password) {
    // we shortened this part, because it’s covered in
    // the previous post on basic authentication with Retrofit
  }

  public static <S> S createService(Class<S> serviceClass, AccessToken token) {
    if (token != null) {
      httpClient.addInterceptor(new Interceptor() {
        @Override
        public Response intercept(Interceptor.Chain chain) throws IOException {
          Request original = chain.request();

          Request.Builder requestBuilder = original.newBuilder()
                  .header("Accept", "application/json")
                  .header("Authorization",
                          token.getTokenType() + " " + token.getAccessToken())
                  .method(original.method(), original.body());

          Request request = requestBuilder.build();
          return chain.proceed(request);
        }
      });
    }

    OkHttpClient client = httpClient.build();
    Retrofit retrofit = builder.client(client).build();
    return retrofit.create(serviceClass);
  }
  */
}
